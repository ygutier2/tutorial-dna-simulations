#!/bin/bash
  #remove directory if exist
  if [ -d "vmd_trajectory" ]; then 
      rm -r vmd_trajectory
  fi

# Main path
mainpath="../REP1/"
          
# Directory where the data is
datapath=${mainpath}"data/"

# Directory where the output will be stored
mkdir -p "vmd_trajectory"
outputpath="vmd_trajectory/"

# Total number of molecules:
Mtot=1

# Number of ssDNA oligonucleotides per molecule
nss=3

# Number of nucleotides per ssDNA (in dsDNA this is also equal to the number of base-pairs)
N=49

# Starting timestep
start=0

# The dump frequency
dumpfreq=10000

# Last timestep of the simulation
last=1000000

# oxDNA version (either 1 or 2)
version=2


#####################
#compile the program#
#####################
#Compilation of the programms (Ubuntu)
c++ -std=c++11 initial_ovito.cpp -o initial_ovito.out -lm
c++ -std=c++11 trajectory.cpp -o trajectory.out -lm


################################################
### Creates topology configuration for ovito ###
################################################
flag1=1
if [ $flag1 -eq 1 ]
then
  echo "Creating OVITO initial topology"
  
  #Timeste from which we extract the init conf
  timestep=0
  
  # Topology (0) linear, (1) ring
  flagtopo=0
  
  ./initial_ovito.out ${datapath} ${outputpath} ${Mtot} ${nss} ${N} ${timestep} ${version} ${flagtopo}
fi


####################################
### Creates trajectory for ovito ###
####################################
./trajectory.out ${datapath} ${outputpath} ${start} ${dumpfreq} ${last} ${N} ${version}

rm initial_ovito.out
rm trajectory.out
