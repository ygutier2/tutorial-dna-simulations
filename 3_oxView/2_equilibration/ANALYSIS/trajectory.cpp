#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision

using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375

#define SMALL 1e-20

// function declaration
vector<double> quatproduct(vector<double> P, vector<double> Q);           
vector<double> q_to_ex(vector<double> Q);
vector<double> q_to_ey(vector<double> Q);
vector<double> q_to_ez(vector<double> Q);
double sign(double a);
double length(vector<double> a);
double dotProduct (vector<double> a, vector<double> b);
vector<double> crossProduct(vector<double> a, vector<double> b);
void fix_roundoff(double &a);
vector<vector <double> > matrixOrthogonal(vector<double> P, vector<double> Q, vector<double> R);
vector<vector <double> > matrixTranspose(vector<vector <double> > M);
vector<vector <double> > matrixProduct(vector<vector <double> > P, vector<vector <double> > Q);
double matrixTrace(vector<vector <double> > M);

int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<8)
    {
        cout << "Eleven arguments are required by the programm:"                      << endl;
        cout << "1.- Directory where the data is"                          << endl;
        cout << "2.- Directory where the output will be stored"            << endl;
        cout << "3.- Starting timestep" << endl; 
        cout << "4.- The dump frequency" <<endl;
        cout << "5.- Last timestep of the simulation" << endl;
        cout << "6.- number of base-pairs" << endl;       
        cout << "7.- oxDNA version" << endl;
    }


if(argc==8)
{   
    cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    
    /*Start the calculation from this timestep*/
    int equiltime;
    equiltime = atoi(argv[3]);
    
    /*Dump frequency*/
    int dumpfreq;
    dumpfreq = atoi(argv[4]);
    
    /*Last timestep*/
    int totrun;
    totrun = atoi(argv[5]);
    
    /*number of base-pairs*/  
    int nbp;
    nbp = atoi(argv[6]);

    /*Choose the oxDNA version you are using to compute the hb and bb location*/
    int version;         
    version = atoi(argv[7]);                        
                                                    
    int frames = 1 + (totrun-equiltime)/dumpfreq;
    int num1, num2, num3;

    /*Number of nucleotides (M*(nss))*/
    int N = 3*(nbp);
    

    /*the following variables will be useful to read the data file*/
    int id, mol, type, nx, ny, nz, tt,nn;
    double x, y, z, q0, q1, q2, q3;
 
    double Lx, Ly, Lz;
    double Lmaxx,Lminx,
           Lmaxy,Lminy,
           Lmaxz,Lminz;

    vector< vector< vector<double> > > quat(frames, vector<vector<double> >(4, vector<double>(N)));
    vector< vector< vector<double> > > com (frames, vector<vector<double> >(3, vector<double>(N)));
    vector< vector< vector<double> > > hb  (frames, vector<vector<double> >(3, vector<double>(N)));
    vector< vector< vector<double> > > bb  (frames, vector<vector<double> >(3, vector<double>(N)));
    vector<vector <int> > idvec(frames, vector<int>(N));
    vector<vector <int> > typevec(frames, vector<int>(N));
      
    /*Usefull when reading input files*/
    int  timestep;
    char name1[10]    = "out";
    char readFile[250] = {'\0'};
    string dummy; 


    /*Usefull when writing output files*/
    string line;
    
    /*********************/
    /**Reads input files**/
    /*********************/    
    //Inside the file, the number of rows assigned per time-frame is: 9, 2*nbp and 1 (bigbead);
    int nlinespf=9+(2*nbp);

    for (int t=0; t<frames; t++)
    {  
        timestep = equiltime+dumpfreq*t;
        sprintf(readFile, "%s%s%d.data",argv[1], name1,timestep);
        ifstream read(readFile);
        if(!read){cout << "Error while opening data file!"<<endl;}

        if (read.is_open())    
        {
            //Read the first 9 lines
            for(int i=0; i<9; i++)   
            {
                if(i==1)
                {
                    read >> tt;
                    //cout << "read timestep: " << tt << endl;
                }
                
                if(i==3)
                {
                    read >> N;
                    //cout << "read number of total nucleotides in the system: " << N << endl;
                }
                
                //Read The box size
                if(i==5)
                {
                    read >> Lminx >> Lmaxx;
                    //cout << "i=" << i << "  Lx " << Lminx << " " << Lmaxx <<endl;
                    Lx = Lmaxx-Lminx;
                }

                if(i==6)
                {
                    read >> Lminy >> Lmaxy;
                    //cout << "i=" << i << "  Ly " << Lminy << " " << Lmaxy <<endl;
                    Ly = Lmaxy-Lminy;
                }

                if(i==7)
                {
                    read >> Lminz >> Lmaxz;
                    //cout << "i=" << i << "  Lz " << Lminz << " " << Lmaxz <<endl;
                    Lz = Lmaxz-Lminz;
                    getline(read,dummy);
                }
                    
                else{ getline(read,line);}
            }                
                    
            // Continue reading the particles
            for(int i=0; i<N; i++)   
            {
                read >> id >> mol >> type >> x >> y >> z >> nx >> ny >> nz >> q0 >> q1 >> q2 >> q3;

                idvec[t][id-1] = id-1;
                typevec[t][id-1] = type;
                com[t][0][id-1] = x+(Lx*nx);
                com[t][1][id-1] = y+(Ly*ny);
                com[t][2][id-1] = z+(Lz*nz);

                quat[t][0][id-1] = q0;
                quat[t][1][id-1] = q1;
                quat[t][2][id-1] = q2;
                quat[t][3][id-1] = q3;
             }  
         }
         read.close();
     }



    /**************************************/
    /**Compute HB and phosphate positions**/
    /**************************************/
    double x1, y1, z1, q01, q11, q21, q31;
    double x2, y2, z2, q02, q12, q22, q32;

    for (int t=0; t<frames; t++)
    {
        for(int i=0; i<N; i++ )
        {
            x1  =  com[t][0][i]; y1  =  com[t][1][i]; z1  =  com[t][2][i];
            q01 = quat[t][0][i]; q11 = quat[t][1][i]; q21 = quat[t][2][i]; q31 = quat[t][3][i];

            vector<double> qs1 {q01, q11, q21, q31};
            vector<double> ex1(3), ey1(3), ez1(3);
            ex1 = q_to_ex(qs1); ey1 = q_to_ey(qs1); ez1 = q_to_ez(qs1);


            //oxdna1 backbone
            if(version==1)
            {
                bb[t][0][i] = x1 - 0.40*ex1[0];
                bb[t][1][i] = y1 - 0.40*ex1[1];
                bb[t][2][i] = z1 - 0.40*ex1[2];                
            }

            //oxdna2 backbone
            if(version==2)
            {
                bb[t][0][i] = x1 - 0.34*ex1[0] + 0.3408*ey1[0];
                bb[t][1][i] = y1 - 0.34*ex1[1] + 0.3408*ey1[1];
                bb[t][2][i] = z1 - 0.34*ex1[2] + 0.3408*ey1[2];
            }


            //The hydrogen bond sites
            hb[t][0][i] = x1 + 0.40*ex1[0];
            hb[t][1][i] = y1 + 0.40*ex1[1];
            hb[t][2][i] = z1 + 0.40*ex1[2];            
        }
    }






    //PRINT THE VISUALIZATION
    // for ovito we use oblate particles for the bases
    //Shape sphere
    double spx = 0.2; double spy = 0.2; double spz = 0.2;

    //Shape ellipsoid
    double elx = 0.2; double ely = 0.17; double elz = 0.1;
    
    char writeFile1 [250] = {'\0'};
    char name3[100] ="traj.ovito.txt";
    sprintf(writeFile1, "%sN%d.oxDNA%d.%s", argv[2], nbp, version, name3);
    ofstream write1(writeFile1);
    cout << "writing on .... " << writeFile1 <<endl;
        
    //set precision and the number of decimals to be printed always
    write1.precision(5);
    write1.setf(ios::fixed);
    write1.setf(ios::showpoint);

    for (int t=0; t<frames; t++) 
    { 
        //Properties=pos:R:3:orientation:R:4:aspherical_shape:R:3
        timestep = equiltime+dumpfreq*t;

        write1 << "ITEM: TIMESTEP"            << endl;
        write1 << timestep                    << endl;
        write1 << "ITEM: NUMBER OF ATOMS"     << endl;
        write1 << 2*N                   << endl;
        write1 << "ITEM: BOX BOUNDS pp pp pp" << endl;
        write1 << Lminx << " " << Lmaxx       << endl;
        write1 << Lminy << " " << Lmaxy       << endl;
        write1 << Lminz << " " << Lmaxz       << endl;
        write1 << "ITEM: ATOMS id mol type x y z q0 q1 q2 q3 sx sy sz" << endl;
        //Ovito reads the quaternions as (w, x, y, z) which means from lammps dump (quat[3], quat[0], quat[1], quat[2] )

        //Please note that the molecule-id refers to the strand (either 1 or 2) at which the nucleotide is part of.

        //First strand: 
       for(int i=0; i<N; i++ )
       {  
           //Backbone (change the type of molecule)
           int newtype;
           if(typevec[t][i] == 1){newtype=11;}
           if(typevec[t][i] == 2){newtype=12;}
           if(typevec[t][i] == 3){newtype=13;}
           if(typevec[t][i] == 4){newtype=14;}
           write1 << 2*i+1 << " " << 1 << " " << newtype      << " " << bb[t][0][i]  << " " << bb[t][1][i] << " " << bb[t][2][i] << " " << quat[t][0][i] << " " << quat[t][1][i] << " " << quat[t][2][i] << " " << quat[t][3][i] << " " <<  spx << " " << spy << " " << spz << endl;

           //Hydrogen-Bond (is the same type as the original molecule)
           write1 << 2*i+2 << " " << 1 << " " << typevec[t][i] << " " << hb[t][0][i]  << " " << hb[t][1][i] << " " << hb[t][2][i] << " " << quat[t][0][i] << " " << quat[t][1][i] << " " << quat[t][2][i] << " " << quat[t][3][i] << " " <<  elx << " " << ely << " " << elz << endl;

       }  
    }   
    write1.close();
    write1.clear();
    




}








return 0;
}



//Quaternion product
vector<double> quatproduct(vector<double> P, vector<double> Q)
{
    vector<double> R(4);
    R[0] = P[0]*Q[0] - P[1]*Q[1] - P[2]*Q[2] - P[3]*Q[3];
    R[1] = P[0]*Q[1] + Q[0]*P[1] + P[2]*Q[3] - P[3]*Q[2];
    R[2] = P[0]*Q[2] + Q[0]*P[2] - P[1]*Q[3] + P[3]*Q[1];
    R[3] = P[0]*Q[3] + Q[0]*P[3] + P[1]*Q[2] - P[2]*Q[1];

    return R;
}

// converts quaternion DOF into local body reference frame
vector<double> q_to_ex(vector<double> Q)
{
    vector<double> ex(3);

    ex[0]=Q[0]*Q[0]+Q[1]*Q[1]-Q[2]*Q[2]-Q[3]*Q[3];
    ex[1]=2*(Q[1]*Q[2]+Q[0]*Q[3]);
    ex[2]=2*(Q[1]*Q[3]-Q[0]*Q[2]);
    return ex;
}


vector<double> q_to_ey(vector<double> Q)
{
    vector<double> ey(3);

    ey[0]=2*(Q[1]*Q[2]-Q[0]*Q[3]);
    ey[1]=Q[0]*Q[0]-Q[1]*Q[1]+Q[2]*Q[2]-Q[3]*Q[3];
    ey[2]=2*(Q[2]*Q[3]+Q[0]*Q[1]);
    return ey;
}


vector<double> q_to_ez(vector<double> Q)
{
    vector<double> ez(3);

    ez[0]=2*(Q[1]*Q[3]+Q[0]*Q[2]);
    ez[1]=2*(Q[2]*Q[3]-Q[0]*Q[1]);
    ez[2]=Q[0]*Q[0]-Q[1]*Q[1]-Q[2]*Q[2]+Q[3]*Q[3];
    return ez;
}


//Sign function
double sign(double a) { if (a>0) {return 1.0;} else {return -1.0;} } 


//length
double length(vector<double> a)
{
  double r;
  r = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
  return r;
}

//Dot product
double dotProduct (vector<double> a, vector<double> b)
{
  double r;
  r = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
  return r;
}

//Cross product
vector<double> crossProduct(vector<double> a, vector<double> b)
{
  vector<double> r(3);
  r[0] = a[1]*b[2]-a[2]*b[1];
  r[1] = a[2]*b[0]-a[0]*b[2];
  r[2] = a[0]*b[1]-a[1]*b[0];
  
  return r;
}


// checks for round off in a number in the interval [-1,1] before using asin or acos
void fix_roundoff(double &a)
{
  if (abs(a)>1.001) 
  {
    std::cout<<"Error - number should be in interval [-1,1] but is "<< a << endl;
    exit(0);
  }
  if (a>1){a=1.0;}
  if (a<-1){a=-1.0;}
}


//The orthogonal matrix. This is the rotation matrix transforming the canonical frame into the frame of the respective triad
vector<vector <double> > matrixOrthogonal(vector<double> a, vector<double> b, vector<double> c)
{
  vector<vector <double> > T(3, vector<double>(3));

  T[0][0] = a[0];    T[0][1] = b[0];    T[0][2] = c[0];
  T[1][0] = a[1];    T[1][1] = b[1];    T[1][2] = c[1];
  T[2][0] = a[2];    T[2][1] = b[2];    T[2][2] = c[2];

  return T;
}


vector<vector <double> > matrixTranspose(vector<vector <double> > a)
{
  vector<vector <double> > M(3, vector<double>(3));

  for(int i=0; i<3; i++)
  {
      for(int j=0; j<3; j++)
      {
          M[i][j] = a[j][i];
      }
  }

  return M;
}


vector<vector <double> > matrixProduct(vector<vector <double> > a, vector<vector <double> > b)
{
  vector<vector <double> > M(3, vector<double>(3));

  for(int i=0; i<3; i++)
  {
      for(int j=0; j<3; j++)
      {
          double s=0;
          
          for(int k=0; k<3; k++)
          {
              s = s + a[i][k]*b[k][j];

              M[i][j] = s;
          }
      }
  }

  return M;
}
     

double matrixTrace(vector<vector <double> > a)
{
  double r;
  r = a[0][0] + a[1][1] + a[2][2];
  return r;
}









