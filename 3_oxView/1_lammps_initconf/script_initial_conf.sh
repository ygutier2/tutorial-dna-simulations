#!/bin/bash
#Executable 
tacoxConverter="../../2_Melting/tacoxDNA-master/src/oxDNA_LAMMPS.py"


folder0="0_oxview"
folder1="1_lammps_initconf"

#Number of nanostars
Nstars=1

#Number of bases per ssDNA oligonucleotide
nbase=49

#Number of bases per nanostar
Nbns=$(( 3*nbase ))

#Total number of bases in the system
N=$(( Nbns*Nstars ))

#Do you want to create initial configuration?
flag1=1


#############################
### Initial configuration ###
#############################
if [ $flag1 -eq 1 ]
then 
  echo "Creating LAMMPS initial configuration"
  
  cd ../${folder0}

  #The files created with oxview
  name="output"
  datfile="$name.dat"
  topfile="$name.top"

  #File generated by tacoxDNA
  tacoxfile="$datfile.lammps"

  #Name of the lammps initial configuration
  lammpsfile="initconf_N${N}"

  #Error if topology and dat files are not found
  if [ ! -s $datfile ]  || [ ! -s $topfile ]
  then
  	echo "Can't find input files. Are you sure you are in the right folder?"
	exit 1
  fi

  #Run tacoxDNA to convert from oxDNA format to LAMMPS format. This creates a file called ${tacoxfile}
  python3 ${tacoxConverter} $topfile $datfile
  
  #Replace the lines related to the box size by: 
  sed -i 's/0.000000 34.000000 xlo xhi/-30 30 xlo xhi/g' $tacoxfile
  sed -i 's/0.000000 34.000000 ylo yhi/-30 30 ylo yhi/g' $tacoxfile
  sed -i 's/0.000000 34.000000 zlo zhi/-30 30 zlo zhi/g' $tacoxfile

  
  mv ${tacoxfile} ../${folder1}/${lammpsfile}
  
  
fi
