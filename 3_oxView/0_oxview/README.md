# Sequence

The sequence of the three ssDNA oligonucleotides is given below starting from the 5' to 3' direction:

**Strand1**: 5'-  CTGGATCCGCGGAAGCTTAAAACGGAATTCGCATGGATCCCCACGATCG  -3'

**Strand2**: 5'-  GGGGATCCATGCGAATTCCGAACTGAATTCCCTGGGATCCCGACGATCG  -3' 

**Strand3**: 5'-  CGGGATCCCAGGGAATTCAGAATTAAGCTTCCGCGGATCCAGACGATCG  -3' 



# Design

The design parts of the nanostars from the previous sequences are shown in the following table:

![](table.png)

# oxView

So the directionality of the chain is the correct one (as shown in the previous sections) we need to insert the sequence in Edit-Seq in the 5' to 3' direction. In the oxview the backbone bonds have conical shape. The thickest part of the cone represents the 5' site and the pick of the cone represents the 3' site.

The way to build the nanostar:

> I.- To build the first arm:
>
> ​	1.- Create a dsDNA given the sequence from the two unpaired base-pairs at the core and forward: AACGGAATTCGCATGGATCCCCACGATCG
>
> ​	2.- Erase the nucleotides in the complementary strand corresponding to the first two (AA) and last seven (ACGATCG).
>
> II.- To build the second arm:
>
> ​	1.- Create a dsDNA given the sequence from the two unpaired base-pairs at the core and forward: AACTGAATTCCCTGGGATCCCGACGATCG
>
> ​	2.- Erase the nucleotides in the complementary strand corresponding to the first two (AA) and last seven (ACGATCG).
>
> ​	3.- Create a bond between AA and the first arm.
>
> III.- To build the third arm:
>
> ​	1.- Create a dsDNA given the sequence from the two unpaired base-pairs at the core and forward: AATTAAGCTTCCGCGGATCCAGACGATCG
>
> ​	2.- Erase the nucleotides in the complementary strand corresponding to the first two (AA) and last seven (ACGATCG).
>
> ​	3.- Create a bond between AA and the second arm.
>
> ​	4.- Run rigid body dynamics with the following parameters: cluster rep constant (1000), Connection spring constant=170, connection relaxed length (0), friction (0.1) and dt (0.1)



Finally check that the sequence on the oxView file correspond to the ones expected here. To this end first dump the sequences of the three strands from oxView (remember that this is given in the 5' to 3' direction). This is saved automatically in the file **sequences.csv**. Then I use the following formulas to compare strings. They return 1 if the strings in the cell and the sequences expected are the same and 0 if not.

=IF(EXACT(C1,"CTGGATCCGCGGAAGCTTAAAACGGAATTCGCATGGATCCCCACGATCG"),1,0)

=IF(EXACT(C2,"GGGGATCCATGCGAATTCCGAACTGAATTCCCTGGGATCCCGACGATCG"),1,0)

=IF(EXACT(C3,"CGGGATCCCAGGGAATTCAGAATTAAGCTTCCGCGGATCCAGACGATCG"),1,0)



# TacoxDNA and LAMMPS

We use tacoxDNA to convert from oxDNA format (configuration + topology file) to LAMMPS format (initial configuration file). For LAMMPS the directionality of the strands enters in the bond topology of the initial configuration: The first (second) atom in a bond definition is understood to point towards the 3’-end (5’-end) of the strand. Therefore, if the bond topology looks like this in the lammps file:

> 1 1 1 2
>
> 2 1 2 3
>
> 3 1 3 4
>
> 4 1 4 5 ...

This will imply the atom ids  1, 2, 3, 4, ...  in the lammps file are given progressively according to the 3' to 5' direction of the chains. Meaning that in the LAMMSP initial configuration the sequences are given in the following order:



**Strand1**: From atom 1   to atom   49:    3'-  *GCTAGCA*CCCCTAGGTACGCTTAAGGCAAAATTCGAAGGCGCCTAGGTC  -5'

​                                                                            324132122224 133 412  3244113 3 2111144 23 11 3323 2241 3342

**Strand2**: From atom 50 to atom   98:    3'-  *GCTAGCA*GCCCTAGGGTCCCTTAAGTCAAGCCTTAAGCGTACCTAGGGG  -5'

​                                                                             3241321322 24133  3422244 1134211 32244 11 3234 1224 1 3333

**Strand3**: From atom 99 to atom 147:    3'-  *GCTAGCA*GACCTAGGCGCCTTCGAATTAAGACTTAAGGGACCCTAGGGC  -5' 

​                                                                             32413213 12241 3323 2244 2311 44113 124411 333  122241 3332

The mapping between particle type and the nucleotide is the following: **A=1, C=2, G=3, T=4**



