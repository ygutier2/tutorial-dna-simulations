#!/bin/bash

c++ -std=c++11 ringdsDNA_setup_for_lammps.c++ -o ringdsDNA_setup_for_lammps

#Number of base-pairs
nbp=1000

#Number of helical turns (nbp/10 in the relaxed state)
turns="100"

./ringdsDNA_setup_for_lammps $nbp $turns

rm ringdsDNA_setup_for_lammps
 
