#!/bin/bash
  #Remove previous results if they exist
  if [ -f "Tw_Wr_Lk_vs_t.dat" ]; then
    rm Tw_Wr_Lk_vs_t.dat
  fi
  
  if [ -f "localTw.dat" ]; then
    rm localTw.dat
  fi
  
  if [ -f "fit.log" ]; then
    rm fit.log
  fi
  
  if [ -f "plot_localTw.pdf" ]; then
    rm plot_localTw.pdf
  fi

  
  #Compile the program:
  c++ -std=c++11 topology.cpp -o topology

  #System details (The same as initial configuration)
  nbp=1000
  
  #Start the calculation from this timestep
  ts="0"
  #Dump frequency
  dfreq="5000"
  #Last timestep
  last="20000"
  
  #Arguments passed to the c++ programs:
  #1.- Path to the input data
  datapath="../REP1/ring_DNA_N${nbp}/"
      
  #2.- Output path (here)
  outputpath="./"
      
  #3.- Name of files to read (without timestep)
  rfile="IN"
  
  #4.-Flag (0) linear; (1) ring
  flag="1"
    
  echo "Computing Tw, Wr and Lk"
  ./topology ${datapath} ${outputpath} ${rfile} ${ts} ${dfreq} ${last} ${nbp} ${flag}

  rm topology
