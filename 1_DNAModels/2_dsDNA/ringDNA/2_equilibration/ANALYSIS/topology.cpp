///////////////////////////////////////////////////////////////////////
// Compute the Twist following the paper Clauvelin2012.              //
// Compute the writhe following  method-1a of the Kelin and Langowski//
// paper: "Computation of Writhe in Modeling of supercoiled DNA"     //
///////////////////////////////////////////////////////////////////////
#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision

using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375

//The timestep set in lammps
double dt=0.005;

// function declaration
double sign(double a);
double length(vector<double> a);
double dotProduct (vector<double> a, vector<double> b);
vector<double> crossProduct(vector<double> a, vector<double> b);
void fix_roundoff(double &a);

int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<9)
    {
 
        cout << "Nine arguments are required by the programm:"             << endl;
        cout << "1.- Directory where the data is"                          << endl;
        cout << "2.- Directory where the output will be stored"            << endl;
        cout << "3.- Name of the input file withou timestep"               << endl;
        cout << "4.- Starting timestep"                                    << endl; 
        cout << "5.- The dump frequency"                                   <<endl;
        cout << "6.- Last timestep of the simulation"                      << endl;        
        cout << "7.- number of base-pairs"                                 << endl;
        cout << "8.- Flag linear or ring"                                 << endl;
    }

if(argc==9)
{   
    //cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    /*argv[3] Name of the inputfile*/
    
    /*Start the calculation from this timestep*/
    int equiltime;
    equiltime = atoi(argv[4]);
    
    /*Dump frequency*/
    int dumpfreq;
    dumpfreq = atoi(argv[5]);
    
    /*Last timestep*/
    int totrun;
    totrun = atoi(argv[6]);
    
    /*The number of frames to read*/
    int frames = 1 + (totrun-equiltime)/dumpfreq;

    /*Number of base-pairs*/
    int nbp;
    nbp = atoi(argv[7]);
    
    //Flag for: linear (0) or circular (1) polymers
    int flag = atoi(argv[8]);
    
    int Ntot=4*nbp;
          
    //position of beads and patches in the two strands
    vector< vector< vector<double> > > position  (frames, vector<vector<double> >(3, vector<double>(Ntot)));
    
    int idbb1, idbb2;
    int idhb1, idhb2;
    vector< vector< vector<double> > > hbS1  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > bbS1  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > hbS2  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > bbS2  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    
    vector< vector< vector<double> > > middle (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > S2toS1 (frames, vector<vector<double> >(3, vector<double>(nbp)));
    
    //Number of tangents
    int Ntan;
    if(flag==0){Ntan=nbp-1;}
    if(flag==1){Ntan=nbp;}
    vector< vector< vector<double> > > tangent  (frames, vector<vector<double> >(3, vector<double>(Ntan)));
    vector< vector< vector<double> > > normal  (frames, vector<vector<double> >(3, vector<double>(Ntan)));
    vector< vector< vector<double> > > binormal  (frames, vector<vector<double> >(3, vector<double>(Ntan)));
    
    
    //Vector obtained as Ti-1 x Ti
    vector< vector< vector<double> > > shiftbinormal  (frames, vector<vector<double> >(3, vector<double>(Ntan)));
    vector< vector< vector<double> > > vecB(frames, vector<vector<double> >(3, vector<double>(Ntan)));
    vector< vector<double> > alpha(frames, vector<double> (Ntan));
    
    vector< vector<double> > localTw(frames, vector<double> (Ntan));
    vector<double>avelocalTw(Ntan);
    vector<double> totalTw(frames);
    vector<double>totalWr(frames);
    
              
    /*the following variables will be useful to read the data file*/
    ifstream indata;
    char readFile[400] = {'\0'};
    string dummy;
    string line;
    
    int timestep;
    int tbrownian; 
    long int id,type,mol;
    double   num1,num2,num3;
    double   x,y,z;
    double   l1,l2;
    double   Lx,Ly,Lz;    
    
    
    for (int t=0; t<frames; t++)
    {
        timestep = equiltime + t*dumpfreq;
        
        sprintf(readFile, "%s%s%d.data",argv[1],argv[3],timestep);        
        indata.open(readFile);
        
        if (indata.is_open())
        {
            //cout << "reading from timestep = " << timestep << endl;
            
            //Read headers
            for(int i=0;i<10;i++){
                if(i==1) {
                    long long int time;
                    indata >> time;
                    tbrownian = t*dumpfreq*dt;
                }

                if(i==3) {
                    indata >> Ntot;
                }

                if(i==5) {
                    indata >> l1 >> l2;
                    Lx = l2-l1;
                }

                if(i==6) {
                    indata >> l1 >> l2;
                    Ly = l2-l1;
                }

                if(i==7) {
                    indata >> l1 >> l2;
                    Lz = l2-l1;
                }

                else getline(indata,dummy);
            }
            
            
            //READ FILES
            for(int n=0; n<Ntot; n++){
                indata >> id >> mol >> type >> x >> y >> z >> num1 >> num2 >> num3;
                position[t][0][(id-1)] = (x + Lx*num1);
                position[t][1][(id-1)] = (y + Ly*num2);
                position[t][2][(id-1)] = (z + Lz*num3);
                
                //cout << n+1 << " " << x << " " << y << " " << z << endl;
                //cout << n+1 << " " << position[t][0][n] << " " << position[t][1][n] << " " << position[t][2][n] << endl;
            }
            
            //Split positions into backbone and HB sites
            for(int n=0; n<nbp; n++){
                //Ids of beads and patches in the first strand
                idbb1 = 2*(n+1) - 1;   
                idhb1 = 2*(n+1);
                
                //Ids of beads and patches in the second strand
                idbb2 = idbb1 + Ntot/2;
                idhb2 = idhb1 + Ntot/2;
                
                //Beads in first strand
                bbS1[t][0][n] = position[t][0][idbb1-1];
                bbS1[t][1][n] = position[t][1][idbb1-1];
                bbS1[t][2][n] = position[t][2][idbb1-1];
                
                //Patches in the first strand
                hbS1[t][0][n] = position[t][0][idhb1-1];
                hbS1[t][1][n] = position[t][1][idhb1-1];
                hbS1[t][2][n] = position[t][2][idhb1-1];
                
                //Beads in second strand
                bbS2[t][0][n] = position[t][0][idbb2-1];
                bbS2[t][1][n] = position[t][1][idbb2-1];
                bbS2[t][2][n] = position[t][2][idbb2-1];
                
                //Patches in the second strand strand
                hbS2[t][0][n] = position[t][0][idhb2-1];
                hbS2[t][1][n] = position[t][1][idhb2-1];
                hbS2[t][2][n] = position[t][2][idhb2-1];
                
                //DNA axis
                middle[t][0][n] = (bbS1[t][0][n]+bbS2[t][0][n])/2.0;
                middle[t][1][n] = (bbS1[t][1][n]+bbS2[t][1][n])/2.0;
                middle[t][2][n] = (bbS1[t][2][n]+bbS2[t][2][n])/2.0;
                
                S2toS1[t][0][n] = bbS1[t][0][n]-bbS2[t][0][n]; 
                S2toS1[t][1][n] = bbS1[t][1][n]-bbS2[t][1][n];     
                S2toS1[t][2][n] = bbS1[t][2][n]-bbS2[t][2][n];
                
                //cout << n << " " << bbS1[t][0][n] << " " << bbS1[t][1][n] << " " << bbS1[t][2][n] << endl;
                //cout << n << " " << middle[t][0][n] << " " << middle[t][1][n] << " " << middle[t][2][n] << endl;
            }
            
            
            
            /******************/
            /*REFERENCE FRAME**/
            /******************/
            //Unitary tangent vector (Tn)
            double tx,ty,tz,normt;
            
            for(int n=0; n<Ntan; n++){
                if(n<nbp-1){
                    tx = middle[t][0][n+1]-middle[t][0][n];
                    ty = middle[t][1][n+1]-middle[t][1][n];
                    tz = middle[t][2][n+1]-middle[t][2][n];
                }
                
                //Only valid for ring
                if(n==nbp-1){
                    tx = middle[t][0][0]-middle[t][0][n];
                    ty = middle[t][1][0]-middle[t][1][n];
                    tz = middle[t][2][0]-middle[t][2][n];
                }
           
                normt = sqrt( pow(tx,2) + pow(ty,2) + pow(tz,2) );

                tangent[t][0][n] = tx/normt;
                tangent[t][1][n] = ty/normt;
                tangent[t][2][n] = tz/normt;
                
                //cout << n << " " << tangent[t][0][n] << " " << tangent[t][1][n] << " " << tangent[t][2][n] << endl;
            }
            
            /*******************/
            /*Normal vector (F)*/
            /*******************/
            //If we define the vector (F) as the linking vector between the center of the beads in a base pair (with this
            //direction: from strand2 to strand1), the normal vector will not always be perpendicular to the tangent vector defined 
            //above. That's why it is necessary to find the ortogonal proyection of the normal
            //vector, in the plane perpendicular to the tangent vector. So basically we need to solve a linear algebra problem.

            //Step 1: The equation of the plane which is perpendicular to the unitary tangent vector T=(Tx, Ty, Tz) is
            //Ax + By +Cz = 0, where A=Tx, B=Ty and C=Tz. So every vector on this plane has coordinates: V = ((-By-Cz)/A, y, z)

            //Step 2: We define two different vectors in this plane by giving differen values to y and z. For example if y=1
            //and z=0, --> v1 = (-B/A, 1, 0). And if y=0, z=1, --> v2=(-C/A, 0, 1)

            //Step 3: With the two last vectors we can build a new orthonormal basis (in the plane which is perpendicular to the
            //tangent vector). The first vector in this basis is simple the unitary u1 = v1/ ||v1||

            //Step 4: The other vector in the basis should be orthonormal to u1. So first we find the directon of this vector
            //y2 = v2 - (v2 * u1)u1. And then the unitary: u2 = y2 / ||y2||

            //Step 5: now that we have the basis in the plane (u1 and u2) we only find the ortogonal proyection of the linking
            //vector between beads in a base pair, onto the plane spaned for the vectors u1 and u2

            double v1x, v1y, v1z, v2x, v2y, v2z;
            double dv1,dv2;
            double u1x,u1y,u1z;
            double y2x, y2y, y2z, dotproductv2u1, dy2;
            double u2x,u2y,u2z;
            double dotproductS2toS1_u1, dotproductS2toS1_u2;
            double Fx,Fy,Fz,dF;
            
            for(int n=0; n<Ntan; n++)
            {
                double A=tangent[t][0][n];
                double B=tangent[t][1][n];
                double C=tangent[t][2][n];

                //STEP 2
                if(A!=0)
                {
                    //components of the vector embedding in the perpendicular plane to the tangent vector
                    v1x = -B/A; v1y = 1; v1z = 0;                                       
                    v2x = -C/A; v2y = 0; v2z = 1;
                }
 
                if(A==0)
                {
                    //Then V = (x, -(C/B) z, z).

                    if(B!=0)
                    {
                        //We define two different vectors in this plane by giving differen values to x and z. 
                        //For example if x=0 and z=1, --> v1 = (0, -C/B, 1). And if x=1, z=0, --> v2=(1, 0, 0)
                        v1x = 0; v1y = -C/B; v1z = 1;
                        v2x = 1; v2y = 0;    v2z = 0;
                    }

                    if(B==0)
                    {
                        //This means that the tangent points in the z direction (because Tx=0 and Ty=0). 
                        //Therefore the plane perpendicular is the x-y plane
                        v1x = 1; v1y = 0; v1z = 0;
                        v2x = 0; v2y = 1; v2z = 0;
                    }
                }
                
                
                //STEP 3
                //magnitude of the vector v1
                dv1 = sqrt( pow(v1x,2) + pow(v1y,2) + pow(v1z,2) );
                // components of the unitary vector: first vector of the basis in the plane.
                u1x = v1x/dv1; u1y = v1y/dv1; u1z = v1z/dv1;

                //STEP 4
                dotproductv2u1 = v2x*u1x + v2y*u1y + v2z*u1z;
                //components of the orthogonal vector to u1
                y2x = v2x - dotproductv2u1* u1x;
                y2y = v2y - dotproductv2u1* u1y;  
                y2z = v2z - dotproductv2u1* u1z;

                //magnitude of the vector y2
                dy2 = sqrt( pow(y2x,2) + pow(y2y,2) + pow(y2z,2) );  
                // components of the unitary vector: second vector in the basis of the plane
                u2x = y2x/dy2; 
                u2y = y2y/dy2;
                u2z = y2z/dy2;
                
                
                //STEP 5
                //Vector pointing from beads in the second strand to beads in the first strand, per base-pair is S2toS1
                //projection of the previous vector onto the plane (S2toS1*u1) u1 + (S2toS1*u2) u2 
                dotproductS2toS1_u1 = S2toS1[t][0][n]*u1x + S2toS1[t][1][n]*u1y + S2toS1[t][2][n]*u1z;
                dotproductS2toS1_u2 = S2toS1[t][0][n]*u2x + S2toS1[t][1][n]*u2y + S2toS1[t][2][n]*u2z;

                Fx = dotproductS2toS1_u1*u1x + dotproductS2toS1_u2*u2x;
                Fy = dotproductS2toS1_u1*u1y + dotproductS2toS1_u2*u2y;
                Fz = dotproductS2toS1_u1*u1z + dotproductS2toS1_u2*u2z;
                
                //magnitude of normal vectors
                dF = sqrt( Fx*Fx + Fy*Fy + Fz*Fz );
                
                normal[t][0][n] = Fx/dF;
                normal[t][1][n] = Fy/dF;
                normal[t][2][n] = Fz/dF;
                
                //cout << n << " " << normal[t][0][n] << " " << normal[t][1][n] << " " << normal[t][2][n] << endl;
            }
            
            
            /*****************/
            /*Binormal vector*/
            /*****************/
            double bnx,bny,bnz,dB;
            for(int n=0; n<Ntan; n++)
            {
               tx = tangent[t][0][n];
               ty = tangent[t][1][n];
               tz = tangent[t][2][n];
               
               Fx = normal[t][0][n];
               Fy = normal[t][1][n];
               Fz = normal[t][2][n];
               
               bnx =   ty*Fz - tz*Fy;
               bny = -(tx*Fz - tz*Fx);
               bnz =   tx*Fy - ty*Fx;  
               
               dB = sqrt( bnx*bnx + bny*bny + bnz*bnz );
               
               binormal[t][0][n] = bnx/dB;
               binormal[t][1][n] = bny/dB;
               binormal[t][2][n] = bnz/dB;
               
               //cout << n << " " << binormal[t][0][n] << " " << binormal[t][1][n] << " " << binormal[t][2][n] << endl;

            }
            
            
            /**************************************/
            /*Find the local twist and total twist*/
            /**************************************/
            //initalize total twist
            double totales = 0.0; 
             
            for(int n=0;n<Ntan;n++){
                /**A set of vectors vecBn=Tn-1 x Tn**/
                /************************************/
                double tnm1x,tnm1y,tnm1z;
                double tnx,tny,tnz;
                if(n==0){
                    //Tangent at n-1
                    tnm1x = tangent[t][0][Ntan-1];
                    tnm1y = tangent[t][1][Ntan-1];
                    tnm1z = tangent[t][2][Ntan-1];
                    
                    //Tangent at n
                    tnx = tangent[t][0][0];
                    tny = tangent[t][1][0];
                    tnz = tangent[t][2][0];
                }
                
                if(n>0){
                    //Tangent at n-1
                    tnm1x = tangent[t][0][n-1];
                    tnm1y = tangent[t][1][n-1];
                    tnm1z = tangent[t][2][n-1];
                    
                    //Tangent at n
                    tnx = tangent[t][0][n];
                    tny = tangent[t][1][n];
                    tnz = tangent[t][2][n];                
                }
                
                //The cross product
                vector<double> Tnm1 {tnm1x, tnm1y, tnm1z};
                vector<double> Tn   {tnx, tny, tnz};
                
                //cout << n << " " <<Tnm1[0] << " " << Tnm1[1] << " " << Tnm1[2] << endl;
                //cout << n << " " <<Tn[0]   << " " << Tn[1]   << " " << Tn[2]   << endl;
                
                vector<double> cross(3);
                cross=crossProduct(Tnm1,Tn);
                
                //cout << n << " " <<cross[0]   << " " << cross[1]   << " " << cross[2]   << endl;
                
                //Normalized
                double db= length(cross);
                
                vecB[t][0][n] =cross[0]/db;
                vecB[t][1][n] =cross[1]/db;
                vecB[t][2][n] =cross[2]/db;
                
                //cout << vecB[t][0][n] << " " << vecB[t][1][n] << " " << vecB[t][2][n] << endl;
                
                
                /*Angle alpha between Tn-1 and Tn*/
                /*********************************/
                double dotp = dotProduct(Tnm1,Tn);
                alpha[t][n] = acos(dotp);
                
                
                /*Shifted Binormal vector*/
                /*************************/
                //Find the rotation matrix associated with a counter-clockwise rotation about a given axis by theta radians.
                //In this case our axis is given by the unitary vecBn vectors and theta is the alpha_n.
                vector< vector<double> > rotM(3, vector<double> (3));
                double a = cos(alpha[t][n]/2.0);
                double b = -vecB[t][0][n] * sin(alpha[t][n]/2.0);
                double c = -vecB[t][1][n] * sin(alpha[t][n]/2.0);
                double d = -vecB[t][2][n] * sin(alpha[t][n]/2.0);
                
                rotM[0][0]=a*a+b*b-c*c-d*d;  rotM[0][1]=2*(b*c+a*d);      rotM[0][2]=2*(b*d-a*c);
                rotM[1][0]=2*(b*c-a*d);      rotM[1][1]=a*a+c*c-b*b-d*d;  rotM[1][2]=2*(c*d+a*b);
                rotM[2][0]=2*(b*d+a*c);      rotM[2][1]=2*(c*d-a*b);      rotM[2][2]=a*a+d*d-b*b-c*c;
                
                //Apply the previous matrix to binormal(n-1) to find shifted_binormal(n).
                double bnm1x,bnm1y,bnm1z;
                if(n==0){
                    bnm1x=binormal[t][0][Ntan-1];
                    bnm1y=binormal[t][1][Ntan-1];
                    bnm1z=binormal[t][2][Ntan-1];
                }
                
                if(n>0){
                    bnm1x=binormal[t][0][n-1];
                    bnm1y=binormal[t][1][n-1];
                    bnm1z=binormal[t][2][n-1];
                }
                
                shiftbinormal[t][0][n] =  (rotM[0][0]*bnm1x) + (rotM[0][1]*bnm1y) + (rotM[0][2]*bnm1z);
                shiftbinormal[t][1][n] =  (rotM[1][0]*bnm1x) + (rotM[1][1]*bnm1y) + (rotM[1][2]*bnm1z);
                shiftbinormal[t][2][n] =  (rotM[2][0]*bnm1x) + (rotM[2][1]*bnm1y) + (rotM[2][2]*bnm1z);
                
                /*Find the local and total twist*/
                /********************************/
                //We compute shiftbinormal(n) x binormal(n)
                double sbnx,sbny,sbnz;
                sbnx=shiftbinormal[t][0][n];
                sbny=shiftbinormal[t][1][n];
                sbnz=shiftbinormal[t][2][n];
                vector<double> vecSBnorm{sbnx, sbny, sbnz};
                
                bnx=binormal[t][0][n];
                bny=binormal[t][1][n];
                bnz=binormal[t][2][n];
                vector<double> vecBnorm{bnx, bny, bnz};
                
                tx = tangent[t][0][n];
                ty = tangent[t][1][n];
                tz = tangent[t][2][n];
                vector<double> vecT{tx, ty, tz}; 
                
                vector<double> crossSBB(3);
                crossSBB=crossProduct(vecSBnorm,vecBnorm);
                
                //Tn*(shiftbinormal(n) x binormal_(n)
                double sintheta = dotProduct(vecT,crossSBB);
                localTw[t][n] = asin(sintheta)*180.0/M_PI;
                totales = totales + asin(sintheta)*180.0/M_PI;
            }
            totalTw[t] = totales/360.0;

            
            
            /********************/
            /** Compute the Wr **/
            /********************/
            vector<double> pointone(3);
            vector<double> pointtwo(3);
            vector<double> pointthree(3);
            vector<double> pointfour(3);

            vector<double> r12(3);
            vector<double> r34(3);
            vector<double> r23(3);
            vector<double> r13(3);
            vector<double> r14(3);
            vector<double> r24(3);

            vector<double>n1(3);
            vector<double>n2(3);
            vector<double>n3(3);
            vector<double>n4(3);

            double SMALL=1e-10;
            vector<double>cvec(3);

            double Wr, omega, n1n2, n2n3, n3n4, n4n1;

            
            Wr=0.0;
            for (int i=1;i<Ntan;i++)
            {
                for (int j=0;j<i;j++)
                {
                    if (i==j||j==i-1||(i==Ntan-1&&j==0)){omega=0.0;}

                    else
                    { 
                        if (j==0){
                            pointthree[0] = middle[t][0][Ntan-1];
                            pointthree[1] = middle[t][1][Ntan-1];
                            pointthree[2] = middle[t][2][Ntan-1];
                        } 
            
                        else
                        {
                            pointthree[0] = middle[t][0][j-1];
                            pointthree[1] = middle[t][1][j-1];
                            pointthree[2] = middle[t][2][j-1];
                        }

                        pointone[0] = middle[t][0][i-1];
                        pointone[1] = middle[t][1][i-1];
                        pointone[2] = middle[t][2][i-1];
            
                        pointtwo[0] = middle[t][0][i];
                        pointtwo[1] = middle[t][1][i];
                        pointtwo[2] = middle[t][2][i];

                        pointfour[0] = middle[t][0][j];
                        pointfour[1] = middle[t][1][j];
                        pointfour[2] = middle[t][2][j];

                        r12[0]=pointtwo[0]-pointone[0];
                        r12[1]=pointtwo[1]-pointone[1];
                        r12[2]=pointtwo[2]-pointone[2];

                        r34[0]=pointfour[0]-pointthree[0];
                        r34[1]=pointfour[1]-pointthree[1];
                        r34[2]=pointfour[2]-pointthree[2];

                        r23[0]=pointthree[0]-pointtwo[0];
                        r23[1]=pointthree[1]-pointtwo[1];
                        r23[2]=pointthree[2]-pointtwo[2];

                        r13[0]=pointthree[0]-pointone[0];
                        r13[1]=pointthree[1]-pointone[1];
                        r13[2]=pointthree[2]-pointone[2];

                        r14[0]=pointfour[0]-pointone[0];
                        r14[1]=pointfour[1]-pointone[1];
                        r14[2]=pointfour[2]-pointone[2];

                        r24[0]=pointfour[0]-pointtwo[0];
                        r24[1]=pointfour[1]-pointtwo[1];
                        r24[2]=pointfour[2]-pointtwo[2];


                        n1 = crossProduct(r13,r14);
                        double ln1 = length(n1);
                        if (length(n1)<SMALL){
                            cout<<"error in writhe 1 "<<length(n1)<<endl;
                        }           
                        n1[0]=n1[0]/ln1;
                        n1[1]=n1[1]/ln1;
                        n1[2]=n1[2]/ln1;


                        n2 = crossProduct(r14,r24);
                        double ln2 = length(n2);
                        if (length(n2)<SMALL){
                            cout<<"error in writhe 1 "<<length(n2)<<endl;
                        }
                        n2[0]=n2[0]/ln2;
                        n2[1]=n2[1]/ln2;
                        n2[2]=n2[2]/ln2;

                        n3 = crossProduct(r24,r23);
                        double ln3 = length(n3);
                        if (length(n3)<SMALL){
                            cout<<"error in writhe 1 "<<length(n3)<<endl;
                        }
                        n3[0]=n3[0]/ln3;
                        n3[1]=n3[1]/ln3;
                        n3[2]=n3[2]/ln3;

                        n4 = crossProduct(r23,r13);
                        double ln4 = length(n4);
                        if (length(n4)<SMALL){
                            cout<<"error in writhe 1 "<<length(n4)<<endl;
                        }
                        n4[0]=n4[0]/ln4;
                        n4[1]=n4[1]/ln4;
                        n4[2]=n4[2]/ln4;

                        n1n2 = dotProduct(n1,n2); 
                        fix_roundoff(n1n2);

                        n2n3 = dotProduct(n2,n3);
                        fix_roundoff(n2n3);

                        n3n4 = dotProduct(n3,n4);
                        fix_roundoff(n3n4);

                        n4n1 = dotProduct(n4,n1);
                        fix_roundoff(n4n1);

                        cvec = crossProduct(r34,r12);

                        double punto = dotProduct(cvec,r13);
                        omega = ( asin(n1n2) + asin(n2n3) + asin(n3n4) + asin(n4n1) )*sign(punto);
                    }
                    Wr = Wr + omega/(4.0*M_PI);
                }
            }
            totalWr[t] = Wr*2.0;
    
            cout << "t=" << timestep << "    Tw=" << totalTw[t] << "    Wr=" << totalWr[t] << "    Lk=" << totalTw[t]+totalWr[t] << endl;
            
                
        }
        indata.close();
        indata.clear();
        
        //else{cout <<"file "<< readFile << " is not there"<<endl; return 0;}
    }//This closes the frame loop
    
    
    //Local twist average ofver time
    for(int n=0;n<Ntan;n++){
        double avv=0.0;
        for(int t=0; t<frames; t++){
            avv+=localTw[t][n];
        }
        avv/=(double)frames;
        avelocalTw[n]=avv;
    }
    
    
        
    ///////////////////////////////////////
    // PRINT Tw,Wr,Lk as function of time
    ///////////////////////////////////////
    stringstream writeFile1;
    ofstream write1;
    writeFile1 <<"Tw_Wr_Lk_vs_t.dat";
    write1.open(writeFile1.str().c_str());
    
    for(int t=0; t<frames; t++)
    {   
        timestep = equiltime + t*dumpfreq;
        write1 << timestep << " " << totalTw[t] << " " << totalWr[t] << " " << totalTw[t]+totalWr[t] << endl;
    }
    
    //PRINT <localtwist>
    stringstream writeFile2;
    ofstream write2;
    writeFile2 <<"localTw.dat";
    write2.open(writeFile2.str().c_str());
    
    for(int n=0; n<Ntan; n++)
    {   
        write2 << n+1 << " " << avelocalTw[n] << endl;
    }
    
        
}


return 0;
}



//Sign function
double sign(double a) { if (a>0) {return 1.0;} else {return -1.0;} } 

//length
double length(vector<double> a)
{
  double r;
  r = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
  return r;
}

//Dot product
double dotProduct (vector<double> a, vector<double> b)
{
  double r;
  r = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
  return r;
}

//Cross product
vector<double> crossProduct(vector<double> a, vector<double> b)
{
  vector<double> r(3);
  r[0] = a[1]*b[2]-a[2]*b[1];
  r[1] = a[2]*b[0]-a[0]*b[2];
  r[2] = a[0]*b[1]-a[1]*b[0];
  
  return r;
}


// checks for round off in a number in the interval [-1,1] before using asin or acos
void fix_roundoff(double &a)
{
  if (abs(a)>1.001) 
  {
    std::cout<<"Error - number should be in interval [-1,1] but is "<< a << endl;
    exit(0);
  }
  if (a>1){a=1.0;}
  if (a<-1){a=-1.0;}
}





