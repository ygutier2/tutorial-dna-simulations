You can run simulations here by running in a terminal the command

    ./bash.run.sh

Remember to change the variables LAMMPS and LAMMPSparallel (in this bash file) with the location of your executables. By deffault, the script run a short simulatioun using an artificial initial configuration of a dsDNA 1000 bp long that lies in the x-y plane. In order to use pre-equilibrated configurations of a ring made of 1000 bp, you just need to set the number of helical turns to (94, 100, 106) in line 16 of the bash, to run either: negative, relaxed or positive supercoiled configurations, respectively. You must also uncomment line 20 to run from any of these pre-equilibrated configurations.

Once the simulation finishes you can move the terminal inside the folder "ANALYSIS" and run the bash.analysis.sh file. With this, the total twist (Tw), writhe (Wr) and linking number (Lk=Tw+Wr) will be computed as function of time using the output from the simulation.
