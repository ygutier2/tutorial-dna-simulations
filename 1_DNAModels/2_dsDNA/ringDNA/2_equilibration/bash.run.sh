#!/bin/bash
#Current working directory variable 
cwd=`pwd`

#Location of the LAMMPS executables
LAMMPS="${cwd}/../../../../lmp_serial_29Sept2021"
LAMMPSparallel="${cwd}/../../../../lmp_mpi_29Sept2021"

#Name of the lammps script to run
lscript="commands_lammps_dsDNA"

#Number of base-pairs
nbp=1000

#Number of helical turns
T=100

#Initial configurations
intl="initial_configuration_ringdsDNA_N${nbp}_T${T}"
#intl="equilibrated_configuration_ringdsDNA_N${nbp}_T${T}"

#Number of cpu's to use in the simulation
ncpu=1
  
  mkdir -p REP1
  cd    REP1

  cp ${cwd}/MASTERFILES/${lscript} .
  cp ${cwd}/MASTERFILES/${intl}    .
  
  #Replace the variable related to the number of base-pairs
  sed -i "s/variable N equal 1000/variable N equal ${nbp}/g" ${lscript}
  #Replace the variable related to the number of turns
  sed -i "s/variable T equal 100/variable T equal ${T}/g" ${lscript}
  #Replace the file to be read as initial configuration
  staticname="variable rname   index initial_configuration_ringdsDNA.*"
  rfile="variable rname index ${intl}"
  sed -i "s/${staticname}/${rfile}/g" ${lscript}

  #Best random seeds 
  r1=$(od -An -N2 -i /dev/urandom)
  r2=$(od -An -N2 -i /dev/urandom)
  echo "variable seedthermo equal ${r1}"   >  parameters.dat
  echo "variable seedthermo1  equal ${r2}" >> parameters.dat
  
  mpirun -np ${ncpu} ${LAMMPSparallel} -in ${lscript}