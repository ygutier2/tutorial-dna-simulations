#!/bin/bash

c++ -std=c++11 lineardsDNA_setup_for_lammps.c++ -o lineardsDNA_setup_for_lammps

#Number of base-pairs
nbp=1000

#Number of helical turns (nbp/10 in the relaxed state)
turns="100"

#Simulation box size (L/2) in x and y directions
#Consider that when completely stretched, the molecule has a size nbp*0.34
Lhalf="20"

./lineardsDNA_setup_for_lammps $nbp $turns $Lhalf

rm lineardsDNA_setup_for_lammps
 
