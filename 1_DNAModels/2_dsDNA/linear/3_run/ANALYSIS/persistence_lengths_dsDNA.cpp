#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision

using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375
#define SMALL 1e-20

//The timestep set in lammps
double dt=0.005;

// function declaration
double sign(double a);
double length(vector<double> a);
double dotProduct (vector<double> a, vector<double> b);
vector<double> crossProduct(vector<double> a, vector<double> b);
void fix_roundoff(double &a);

int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<9)
    {
 
        cout << "Nine arguments are required by the programm:"             << endl;
        cout << "1.- Directory where the data is"                          << endl;
        cout << "2.- Directory where the output will be stored"            << endl;
        cout << "3.- Name of the input file withou timestep"               << endl;
        cout << "4.- Starting timestep"                                    << endl; 
        cout << "5.- The dump frequency"                                   <<endl;
        cout << "6.- Last timestep of the simulation"                      << endl;        
        cout << "7.- number of base-pairs"                                 << endl;
        cout << "8.- Flag linear or ring"                                 << endl;
    }

if(argc==9)
{   
    //cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    /*argv[3] Name of the inputfile*/
    
    /*Start the calculation from this timestep*/
    int equiltime;
    equiltime = atoi(argv[4]);
    
    /*Dump frequency*/
    int dumpfreq;
    dumpfreq = atoi(argv[5]);
    
    /*Last timestep*/
    int totrun;
    totrun = atoi(argv[6]);
    
    /*The number of frames to read*/
    int frames = 1 + (totrun-equiltime)/dumpfreq;

    /*Number of base-pairs*/
    int nbp;
    nbp = atoi(argv[7]);
    
    //Flag for: linear (0) or circular (1) polymers
    int flag = atoi(argv[8]);
    
    int Ntot=4*nbp;
          
    //position of beads and patches in the two strands
    vector< vector< vector<double> > > position  (frames, vector<vector<double> >(3, vector<double>(Ntot)));
    
    int idbb1, idbb2;
    int idhb1, idhb2;
    vector< vector< vector<double> > > hbS1  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > bbS1  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > hbS2  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > bbS2  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    
    vector< vector< vector<double> > > middle (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > S2toS1 (frames, vector<vector<double> >(3, vector<double>(nbp)));
    
    //Number of tangents
    int Ntan;
    if(flag==0){Ntan=nbp-1;}
    if(flag==1){Ntan=nbp;}
    vector< vector< vector<double> > > tangent  (frames, vector<vector<double> >(3, vector<double>(Ntan)));
    vector< vector< vector<double> > > normal  (frames, vector<vector<double> >(3, vector<double>(Ntan)));
    vector< vector< vector<double> > > binormal  (frames, vector<vector<double> >(3, vector<double>(Ntan)));
    
    
    //Tangent-Tangent correlation
    vector<vector <double> > Correlation(frames, vector<double>(Ntan));
    
    vector<vector <double> > Omega(frames, vector<double>(Ntan-1));
    vector<vector <double> > cosOmega(frames, vector<double>(Ntan-1));
    
              
    /*the following variables will be useful to read the data file*/
    ifstream indata;
    char readFile[400] = {'\0'};
    string dummy;
    string line;
    
    int timestep;
    int tbrownian; 
    long int id,type,mol;
    double   num1,num2,num3;
    double   x,y,z;
    double   l1,l2;
    double   Lx,Ly,Lz;    
    
    
    for (int t=0; t<frames; t++)
    {
        timestep = equiltime + t*dumpfreq;
        
        sprintf(readFile, "%s%s%d.data",argv[1],argv[3],timestep);        
        indata.open(readFile);
        
        if (indata.is_open())
        {
            //cout << "reading from timestep = " << timestep << endl;
            
            //Read headers
            for(int i=0;i<10;i++){
                if(i==1) {
                    long long int time;
                    indata >> time;
                    tbrownian = t*dumpfreq*dt;
                }

                if(i==3) {
                    indata >> Ntot;
                }

                if(i==5) {
                    indata >> l1 >> l2;
                    Lx = l2-l1;
                }

                if(i==6) {
                    indata >> l1 >> l2;
                    Ly = l2-l1;
                }

                if(i==7) {
                    indata >> l1 >> l2;
                    Lz = l2-l1;
                }

                else getline(indata,dummy);
            }
            
            
            //READ FILES
            for(int n=0; n<Ntot; n++){
                indata >> id >> mol >> type >> x >> y >> z >> num1 >> num2 >> num3;
                position[t][0][(id-1)] = (x + Lx*num1);
                position[t][1][(id-1)] = (y + Ly*num2);
                position[t][2][(id-1)] = (z + Lz*num3);
                
                //cout << n+1 << " " << x << " " << y << " " << z << endl;
                //cout << n+1 << " " << position[t][0][n] << " " << position[t][1][n] << " " << position[t][2][n] << endl;
            }
            
            //Split positions into backbone and HB sites
            for(int n=0; n<nbp; n++){
                //Ids of beads and patches in the first strand
                idbb1 = 2*(n+1) - 1;   
                idhb1 = 2*(n+1);
                
                //Ids of beads and patches in the second strand
                idbb2 = idbb1 + Ntot/2;
                idhb2 = idhb1 + Ntot/2;
                
                //Beads in first strand
                bbS1[t][0][n] = position[t][0][idbb1-1];
                bbS1[t][1][n] = position[t][1][idbb1-1];
                bbS1[t][2][n] = position[t][2][idbb1-1];
                
                //Patches in the first strand
                hbS1[t][0][n] = position[t][0][idhb1-1];
                hbS1[t][1][n] = position[t][1][idhb1-1];
                hbS1[t][2][n] = position[t][2][idhb1-1];
                
                //Beads in second strand
                bbS2[t][0][n] = position[t][0][idbb2-1];
                bbS2[t][1][n] = position[t][1][idbb2-1];
                bbS2[t][2][n] = position[t][2][idbb2-1];
                
                //Patches in the second strand strand
                hbS2[t][0][n] = position[t][0][idhb2-1];
                hbS2[t][1][n] = position[t][1][idhb2-1];
                hbS2[t][2][n] = position[t][2][idhb2-1];
                
                //DNA axis
                middle[t][0][n] = (bbS1[t][0][n]+bbS2[t][0][n])/2.0;
                middle[t][1][n] = (bbS1[t][1][n]+bbS2[t][1][n])/2.0;
                middle[t][2][n] = (bbS1[t][2][n]+bbS2[t][2][n])/2.0;
                
                S2toS1[t][0][n] = bbS1[t][0][n]-bbS2[t][0][n]; 
                S2toS1[t][1][n] = bbS1[t][1][n]-bbS2[t][1][n];     
                S2toS1[t][2][n] = bbS1[t][2][n]-bbS2[t][2][n];
                
                //cout << n << " " << bbS1[t][0][n] << " " << bbS1[t][1][n] << " " << bbS1[t][2][n] << endl;
                //cout << n << " " << middle[t][0][n] << " " << middle[t][1][n] << " " << middle[t][2][n] << endl;
            }
            
            
            
            /******************/
            /*REFERENCE FRAME**/
            /******************/
            //Unitary tangent vector (Tn)
            double tx,ty,tz,normt;
            
            for(int n=0; n<Ntan; n++){
                if(n<nbp-1){
                    tx = middle[t][0][n+1]-middle[t][0][n];
                    ty = middle[t][1][n+1]-middle[t][1][n];
                    tz = middle[t][2][n+1]-middle[t][2][n];
                }
                
                //Only valid for ring
                if(n==nbp-1){
                    tx = middle[t][0][0]-middle[t][0][n];
                    ty = middle[t][1][0]-middle[t][1][n];
                    tz = middle[t][2][0]-middle[t][2][n];
                }
           
                normt = sqrt( pow(tx,2) + pow(ty,2) + pow(tz,2) );

                tangent[t][0][n] = tx/normt;
                tangent[t][1][n] = ty/normt;
                tangent[t][2][n] = tz/normt;
                
                //cout << n << " " << tangent[t][0][n] << " " << tangent[t][1][n] << " " << tangent[t][2][n] << endl;
            }
            
            /*******************/
            /*Normal vector (F)*/
            /*******************/
            //If we define the vector (F) as the linking vector between the center of the beads in a base pair (with this
            //direction: from strand2 to strand1), the normal vector will not always be perpendicular to the tangent vector defined 
            //above. That's why it is necessary to find the ortogonal proyection of the normal
            //vector, in the plane perpendicular to the tangent vector. So basically we need to solve a linear algebra problem.

            //Step 1: The equation of the plane which is perpendicular to the unitary tangent vector T=(Tx, Ty, Tz) is
            //Ax + By +Cz = 0, where A=Tx, B=Ty and C=Tz. So every vector on this plane has coordinates: V = ((-By-Cz)/A, y, z)

            //Step 2: We define two different vectors in this plane by giving differen values to y and z. For example if y=1
            //and z=0, --> v1 = (-B/A, 1, 0). And if y=0, z=1, --> v2=(-C/A, 0, 1)

            //Step 3: With the two last vectors we can build a new orthonormal basis (in the plane which is perpendicular to the
            //tangent vector). The first vector in this basis is simple the unitary u1 = v1/ ||v1||

            //Step 4: The other vector in the basis should be orthonormal to u1. So first we find the directon of this vector
            //y2 = v2 - (v2 * u1)u1. And then the unitary: u2 = y2 / ||y2||

            //Step 5: now that we have the basis in the plane (u1 and u2) we only find the ortogonal proyection of the linking
            //vector between beads in a base pair, onto the plane spaned for the vectors u1 and u2

            double v1x, v1y, v1z, v2x, v2y, v2z;
            double dv1,dv2;
            double u1x,u1y,u1z;
            double y2x, y2y, y2z, dotproductv2u1, dy2;
            double u2x,u2y,u2z;
            double dotproductS2toS1_u1, dotproductS2toS1_u2;
            double Fx,Fy,Fz,dF;
            
            for(int n=0; n<Ntan; n++)
            {
                double A=tangent[t][0][n];
                double B=tangent[t][1][n];
                double C=tangent[t][2][n];

                //STEP 2
                if(A!=0)
                {
                    //components of the vector embedding in the perpendicular plane to the tangent vector
                    v1x = -B/A; v1y = 1; v1z = 0;                                       
                    v2x = -C/A; v2y = 0; v2z = 1;
                }
 
                if(A==0)
                {
                    //Then V = (x, -(C/B) z, z).

                    if(B!=0)
                    {
                        //We define two different vectors in this plane by giving differen values to x and z. 
                        //For example if x=0 and z=1, --> v1 = (0, -C/B, 1). And if x=1, z=0, --> v2=(1, 0, 0)
                        v1x = 0; v1y = -C/B; v1z = 1;
                        v2x = 1; v2y = 0;    v2z = 0;
                    }

                    if(B==0)
                    {
                        //This means that the tangent points in the z direction (because Tx=0 and Ty=0). 
                        //Therefore the plane perpendicular is the x-y plane
                        v1x = 1; v1y = 0; v1z = 0;
                        v2x = 0; v2y = 1; v2z = 0;
                    }
                }
                
                
                //STEP 3
                //magnitude of the vector v1
                dv1 = sqrt( pow(v1x,2) + pow(v1y,2) + pow(v1z,2) );
                // components of the unitary vector: first vector of the basis in the plane.
                u1x = v1x/dv1; u1y = v1y/dv1; u1z = v1z/dv1;

                //STEP 4
                dotproductv2u1 = v2x*u1x + v2y*u1y + v2z*u1z;
                //components of the orthogonal vector to u1
                y2x = v2x - dotproductv2u1* u1x;
                y2y = v2y - dotproductv2u1* u1y;  
                y2z = v2z - dotproductv2u1* u1z;

                //magnitude of the vector y2
                dy2 = sqrt( pow(y2x,2) + pow(y2y,2) + pow(y2z,2) );  
                // components of the unitary vector: second vector in the basis of the plane
                u2x = y2x/dy2; 
                u2y = y2y/dy2;
                u2z = y2z/dy2;
                
                
                //STEP 5
                //Vector pointing from beads in the second strand to beads in the first strand, per base-pair is S2toS1
                //projection of the previous vector onto the plane (S2toS1*u1) u1 + (S2toS1*u2) u2 
                dotproductS2toS1_u1 = S2toS1[t][0][n]*u1x + S2toS1[t][1][n]*u1y + S2toS1[t][2][n]*u1z;
                dotproductS2toS1_u2 = S2toS1[t][0][n]*u2x + S2toS1[t][1][n]*u2y + S2toS1[t][2][n]*u2z;

                Fx = dotproductS2toS1_u1*u1x + dotproductS2toS1_u2*u2x;
                Fy = dotproductS2toS1_u1*u1y + dotproductS2toS1_u2*u2y;
                Fz = dotproductS2toS1_u1*u1z + dotproductS2toS1_u2*u2z;
                
                //magnitude of normal vectors
                dF = sqrt( Fx*Fx + Fy*Fy + Fz*Fz );
                
                normal[t][0][n] = Fx/dF;
                normal[t][1][n] = Fy/dF;
                normal[t][2][n] = Fz/dF;
                
                //cout << n << " " << normal[t][0][n] << " " << normal[t][1][n] << " " << normal[t][2][n] << endl;
            }
            
            
            /*****************/
            /*Binormal vector*/
            /*****************/
            double bnx,bny,bnz,dB;
            for(int n=0; n<Ntan; n++)
            {
               tx = tangent[t][0][n];
               ty = tangent[t][1][n];
               tz = tangent[t][2][n];
               
               Fx = normal[t][0][n];
               Fy = normal[t][1][n];
               Fz = normal[t][2][n];
               
               bnx =   ty*Fz - tz*Fy;
               bny = -(tx*Fz - tz*Fx);
               bnz =   tx*Fy - ty*Fx;  
               
               dB = sqrt( bnx*bnx + bny*bny + bnz*bnz );
               
               binormal[t][0][n] = bnx/dB;
               binormal[t][1][n] = bny/dB;
               binormal[t][2][n] = bnz/dB;
               
               //cout << n << " " << binormal[t][0][n] << " " << binormal[t][1][n] << " " << binormal[t][2][n] << endl;

            }
            
            
            /*********************************/
            /**The tangen-tangen correlation**/
            /*********************************/
            vector <double> corr(Ntan);

            for(int i=0; i<Ntan; i++)
            {
                for(int j=0; j<Ntan-i; j++)
                {
                    double tix = tangent[t][0][i];
                    double tiy = tangent[t][1][i];
                    double tiz = tangent[t][2][i];
                    
                    double tjx = tangent[t][0][i+j]; 
                    double tjy = tangent[t][1][i+j];
                    double tjz = tangent[t][2][i+j];

                    vector<double> Ti {tix, tiy, tiz};
                    vector<double> Tj {tjx, tjy, tjz};

                    double punto = dotProduct(Ti,Tj);

                    corr[j] = corr[j] + punto;
                }
            }

            //We have already sum over all the tangents that are located at a distance j.
            //So in order to obtain the average we just need to divide by the number of data in each case
            for(int i=0; i<Ntan; i++)
            {
                Correlation[t][i] = corr[i]/(double)(Ntan-i);
            }
            
            
            /*************************/
            /**The angle correlation**/
            /*************************/
            //t,n,b --> u,f,v
            //Twist in the relaxed state (36 degrees)
            double phi0=0.628318;
            for(int n=0;n<Ntan-1;n++){
                //Tangent at n
                double tnx = tangent[t][0][n];
                double tny = tangent[t][1][n];
                double tnz = tangent[t][2][n];
                vector<double> Tn {tnx, tny, tnz};
                
                //Tangent at n+1
                double tn1x = tangent[t][0][n+1];
                double tn1y = tangent[t][1][n+1];
                double tn1z = tangent[t][2][n+1];
                vector<double> Tn1 {tn1x, tn1y, tn1z};
                
                double dotT=dotProduct(Tn,Tn1);
                
                
                //Normal at n
                double fnx = normal[t][0][n];
                double fny = normal[t][1][n];
                double fnz = normal[t][2][n];
                vector<double> Fn {fnx, fny, fnz};
                
                //Normal at n+1
                double fn1x = normal[t][0][n+1];
                double fn1y = normal[t][1][n+1];
                double fn1z = normal[t][2][n+1];
                vector<double> Fn1 {fn1x, fn1y, fn1z};
                
                double dotF=dotProduct(Fn,Fn1);
                
                
                //Biormal at n
                double vnx = binormal[t][0][n];
                double vny = binormal[t][1][n];
                double vnz = binormal[t][2][n];
                vector<double> Vn {vnx, vny, vnz};
                
                //Biormal at n+1
                double vn1x = binormal[t][0][n+1];
                double vn1y = binormal[t][1][n+1];
                double vn1z = binormal[t][2][n+1];
                vector<double> Vn1 {vn1x, vn1y, vn1z};
                
                double dotV=dotProduct(Vn,Vn1);
                
                
                //cout << n << " " << dotT << " " << dotF << " " << dotV <<endl;
                
                //The cosine of alpha + gamma (The Euler angles)
                double cosag = (dotF + dotV)/(1.0+dotT);
                //Round errors
                if (cosag>1.0 && cosag<1.1) {cosag=1.0;}
                if (cosag<-1.0 && cosag>-1.1) {cosag=-1.0;}
                
                //Define the vector obtained ass Fn x Fn1
                vector<double> cross(3);
                cross=crossProduct(Fn,Fn1);
                //Sign of cross*Tn1. This is to know in which direction the angle of rotation is.
                double signo = sign(dotProduct(cross,Tn1));
                
                //Alpha + gamma
                double ag=acos(cosag);
                ag*=signo;
                
                if (n==0){Omega[t][n]=ag-phi0;}
                if (n>0) {Omega[t][n]=Omega[t][n-1]+ag-phi0;} // add up to get Omega_i
            }
            
            // < cos Omega_n >
            for(int n=0;n<Ntan-1;n++){
               	cosOmega[t][n]=cos(Omega[t][n]);
            }
            
        }
        indata.close();
        indata.clear();
        
        //else{cout <<"file "<< readFile << " is not there"<<endl; return 0;}
    }//This closes the frame loop
    
    
    
    //Here we take the average over time of the tangent-tangent correlation
    vector <double> avecorrelation(Ntan);
    for(int n=0; n<Ntan; n++)
    {
        double sum =0.0;
        for (int t=0; t<frames; t++)
        {
            sum += Correlation[t][n];
        }

        avecorrelation[n] = sum/(double)frames;
    }
    
    //Here we take the average over time of the rotations
    vector <double> aveOmegacorr(Ntan-1);
    for(int n=0; n<Ntan-1; n++)
    {
        double sum =0.0;
        for (int t=0; t<frames; t++)
        {
            sum += cosOmega[t][n];
        }

        aveOmegacorr[n] = sum/(double)frames;
    }

    
    /////////////
    // PRINT correlations
    /////////////
    stringstream writeFile1;
    ofstream write1;
    writeFile1 <<"correlations.dat";
    write1.open(writeFile1.str().c_str());
    
    for(int n=0; n<Ntan; n++)
    {   
        write1 << n << " " << avecorrelation[n] << " " << aveOmegacorr[n] << endl;
    }
    
        
}


return 0;
}



//Sign function
double sign(double a) { if (a>0) {return 1.0;} else {return -1.0;} } 

//length
double length(vector<double> a)
{
  double r;
  r = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
  return r;
}

//Dot product
double dotProduct (vector<double> a, vector<double> b)
{
  double r;
  r = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
  return r;
}

//Cross product
vector<double> crossProduct(vector<double> a, vector<double> b)
{
  vector<double> r(3);
  r[0] = a[1]*b[2]-a[2]*b[1];
  r[1] = a[2]*b[0]-a[0]*b[2];
  r[2] = a[0]*b[1]-a[1]*b[0];
  
  return r;
}


// checks for round off in a number in the interval [-1,1] before using asin or acos
void fix_roundoff(double &a)
{
  if (abs(a)>1.001) 
  {
    std::cout<<"Error - number should be in interval [-1,1] but is "<< a << endl;
    exit(0);
  }
  if (a>1){a=1.0;}
  if (a<-1){a=-1.0;}
}





