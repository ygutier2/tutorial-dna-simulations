#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision

using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375
#define SMALL 1e-20

//The timestep set in lammps
double dt=0.005;

// function declaration
double sign(double a);
double length(vector<double> a);
double dotProduct (vector<double> a, vector<double> b);
vector<double> crossProduct(vector<double> a, vector<double> b);
void fix_roundoff(double &a);

int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<9)
    {
 
        cout << "Nine arguments are required by the programm:"             << endl;
        cout << "1.- Directory where the data is"                          << endl;
        cout << "2.- Directory where the output will be stored"            << endl;
        cout << "3.- Name of the input file withou timestep"               << endl;
        cout << "4.- Starting timestep"                                    << endl; 
        cout << "5.- The dump frequency"                                   <<endl;
        cout << "6.- Last timestep of the simulation"                      << endl;        
        cout << "7.- number of base-pairs"                                 << endl;
        cout << "8.- Flag linear or ring"                                  << endl;
    }

if(argc==9)
{   
    //cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    /*argv[3] Name of the inputfile*/
    
    /*Start the calculation from this timestep*/
    int equiltime;
    equiltime = atoi(argv[4]);
    
    /*Dump frequency*/
    int dumpfreq;
    dumpfreq = atoi(argv[5]);
    
    /*Last timestep*/
    int totrun;
    totrun = atoi(argv[6]);
    
    /*The number of frames to read*/
    int frames = 1 + (totrun-equiltime)/dumpfreq;

    /*Number of base-pairs*/
    int nbp;
    nbp = atoi(argv[7]);
    
    //Flag for: linear (0) or circular (1) polymers
    int flag = atoi(argv[8]);
    
    int Ntot=4*nbp;
          
    //position of beads and patches in the two strands
    vector< vector< vector<double> > > position  (frames, vector<vector<double> >(3, vector<double>(Ntot)));
    
    int idbb1, idbb2;
    int idhb1, idhb2;
    vector< vector< vector<double> > > hbS1  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > bbS1  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > hbS2  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > bbS2  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    
    vector< vector< vector<double> > > middle (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > S2toS1 (frames, vector<vector<double> >(3, vector<double>(nbp)));
    
    //Number of tangents
    int Ntan;
    if(flag==0){Ntan=nbp-1;}
    if(flag==1){Ntan=nbp;}
    vector< vector< vector<double> > > tangent  (frames, vector<vector<double> >(3, vector<double>(Ntan)));
    vector< vector< vector<double> > > normal  (frames, vector<vector<double> >(3, vector<double>(Ntan)));
    vector< vector< vector<double> > > binormal  (frames, vector<vector<double> >(3, vector<double>(Ntan)));
    
    
    //Tangent-Tangent correlation
    vector<vector <double> > Correlation(frames, vector<double>(Ntan));
    
              
    /*the following variables will be useful to read the data file*/
    ifstream indata;
    char readFile[400] = {'\0'};
    string dummy;
    string line;
    
    int timestep;
    int tbrownian; 
    long int id,type,mol;
    double   num1,num2,num3;
    double   x,y,z;
    double   l1,l2;
    double   Lx,Ly,Lz;    
    
    
    for (int t=0; t<frames; t++)
    {
        timestep = equiltime + t*dumpfreq;
        
        sprintf(readFile, "%s%s%d.data",argv[1],argv[3],timestep);
        indata.open(readFile);
        
        if (indata.is_open())
        {
            //cout << "reading from timestep = " << timestep << endl;
            
            //Read headers
            for(int i=0;i<10;i++){
                if(i==1) {
                    long long int time;
                    indata >> time;
                    tbrownian = t*dumpfreq*dt;
                }

                if(i==3) {
                    indata >> Ntot;
                }

                if(i==5) {
                    indata >> l1 >> l2;
                    Lx = l2-l1;
                }

                if(i==6) {
                    indata >> l1 >> l2;
                    Ly = l2-l1;
                }

                if(i==7) {
                    indata >> l1 >> l2;
                    Lz = l2-l1;
                }

                else getline(indata,dummy);
            }
            
            
            //READ FILES
            for(int n=0; n<Ntot; n++){
                indata >> id >> mol >> type >> x >> y >> z >> num1 >> num2 >> num3;
                position[t][0][(id-1)] = (x + Lx*num1);
                position[t][1][(id-1)] = (y + Ly*num2);
                position[t][2][(id-1)] = (z + Lz*num3);

            }
            
            //Split positions into backbone and HB sites
            for(int n=0; n<nbp; n++){
                //Ids of beads and patches in the first strand
                idbb1 = 2*(n+1) - 1;   
                idhb1 = 2*(n+1);
                
                //Ids of beads and patches in the second strand
                idbb2 = idbb1 + Ntot/2;
                idhb2 = idhb1 + Ntot/2;
                
                //Beads in first strand
                bbS1[t][0][n] = position[t][0][idbb1-1];
                bbS1[t][1][n] = position[t][1][idbb1-1];
                bbS1[t][2][n] = position[t][2][idbb1-1];
                
                //Patches in the first strand
                hbS1[t][0][n] = position[t][0][idhb1-1];
                hbS1[t][1][n] = position[t][1][idhb1-1];
                hbS1[t][2][n] = position[t][2][idhb1-1];
                
                //Beads in second strand
                bbS2[t][0][n] = position[t][0][idbb2-1];
                bbS2[t][1][n] = position[t][1][idbb2-1];
                bbS2[t][2][n] = position[t][2][idbb2-1];
                
                //Patches in the second strand strand
                hbS2[t][0][n] = position[t][0][idhb2-1];
                hbS2[t][1][n] = position[t][1][idhb2-1];
                hbS2[t][2][n] = position[t][2][idhb2-1];
                
                //DNA axis
                middle[t][0][n] = (bbS1[t][0][n]+bbS2[t][0][n])/2.0;
                middle[t][1][n] = (bbS1[t][1][n]+bbS2[t][1][n])/2.0;
                middle[t][2][n] = (bbS1[t][2][n]+bbS2[t][2][n])/2.0;
            }
            
            
            //////////////////////////////////////////////////
            // Compute Radius of Gyration and end2end distance
            //////////////////////////////////////////////////
            double dx = middle[t][0][nbp-1]-middle[t][0][0];
            double dy = middle[t][1][nbp-1]-middle[t][1][0];
            double dz = middle[t][2][nbp-1]-middle[t][2][0];
        
            double e2e=sqrt(dx*dx + dy*dy + dz*dz);
        
            double rg2=0;
            double com[3]={0.0,0.0,0.0};

            //COM of each polymer
            for(int n=0;n<nbp;n++){
                com[0]+=middle[t][0][n];
                com[1]+=middle[t][1][n];
                com[2]+=middle[t][2][n];
            }

            com[0]/=(double)nbp;
            com[1]/=(double)nbp;
            com[2]/=(double)nbp;
        
            //Radius of gyration
            for(int n=0;n<nbp;n++){
                for(int d=0;d<3;d++){
                    rg2+=pow(middle[t][d][n]-com[d],2.0);
                }
            }
            
            rg2/=(double)nbp;
            
            /////////////
            // PRINT RG
            /////////////
            stringstream writeFile1;
            ofstream write1;
            writeFile1 <<"Rg2_R_vs_t.dat";
            write1.open(writeFile1.str().c_str(), std::ios_base::app);
            write1 << timestep << " " << rg2 << " " << e2e << endl;
            
            
        }
        indata.close();
        indata.clear();
        
        //else{cout <<"file "<< readFile << " is not there"<<endl; return 0;}
    }//This closes the frame loop
    
        
}


return 0;
}



//Sign function
double sign(double a) { if (a>0) {return 1.0;} else {return -1.0;} } 


//length
double length(vector<double> a)
{
  double r;
  r = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
  return r;
}

//Dot product
double dotProduct (vector<double> a, vector<double> b)
{
  double r;
  r = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
  return r;
}

//Cross product
vector<double> crossProduct(vector<double> a, vector<double> b)
{
  vector<double> r(3);
  r[0] = a[1]*b[2]-a[2]*b[1];
  r[1] = a[2]*b[0]-a[0]*b[2];
  r[2] = a[0]*b[1]-a[1]*b[0];
  
  return r;
}


// checks for round off in a number in the interval [-1,1] before using asin or acos
void fix_roundoff(double &a)
{
  if (abs(a)>1.001) 
  {
    std::cout<<"Error - number should be in interval [-1,1] but is "<< a << endl;
    exit(0);
  }
  if (a>1){a=1.0;}
  if (a<-1){a=-1.0;}
}





