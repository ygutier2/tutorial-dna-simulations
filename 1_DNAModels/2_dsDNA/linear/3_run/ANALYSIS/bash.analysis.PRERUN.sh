#!/bin/bash
  #Remove previous results if they exist
  if [ -f "Rg2_R_vs_t.dat" ]; then
    rm Rg2_R_vs_t.dat
  fi
  
  if [ -f "correlations.dat" ]; then
    rm correlations.dat
  fi
  
  if [ -f "fit.log" ]; then
    rm fit.log
  fi
  
  if [ -f "plot_deltaOmega.pdf" ]; then
    rm plot_deltaOmega.pdf
    rm plot_rg2_R_vs_time.pdf
    rm plot_TTC.pdf
  fi

  
  #Compile the programs:
  c++ -std=c++11 rg_R_vstime_dsDNA.cpp         -o rg_R_vstime_dsDNA
  c++ -std=c++11 persistence_lengths_dsDNA.cpp -o persistence_lengths_dsDNA

  #System details (The same as initial configuration)
  nbp=1000
  
  #Start the calculation from this timestep
  ts="0"
  #Dump frequency
  dfreq="100000"
  #Last timestep
  last="60300000"
  
  #Arguments passed to the c++ programs:
  #1.- Path to the input data
  datapath="../REP1/linear_DNA_N${nbp}/"
      
  #2.- Output path (here)
  outputpath="./"
      
  #3.- Name of files to read (without timestep)
  rfile="IN"
  
  #4.-Flag (0) linear; (1) ring
  flag="0"
  
 
  #Compute Rg
  echo "Computing radius of gyration"
  ./rg_R_vstime_dsDNA ${datapath} ${outputpath} ${rfile} ${ts} ${dfreq} ${last} ${nbp} ${flag}
  
  #Compute correlation
  echo "Computing correlations"
  #outttc="TTC.dat"
  ./persistence_lengths_dsDNA ${datapath} ${outputpath} ${rfile} ${outttc} ${ts} ${dfreq} ${last} ${nbp} ${flag}

  rm rg_R_vstime_dsDNA
  rm persistence_lengths_dsDNA
