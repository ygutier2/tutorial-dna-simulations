#!/bin/bash

  #Remove previous results if they exist
  if [ -f "stiffness.dat" ]; then
    rm stiffness.dat
  fi
  
  if [ -f "plot_stiffnessMatrix_oxDNA2.pdf" ]; then
    rm plot_stiffnessMatrix_oxDNA2.pdf
  fi
  

# Main path
mainpath="../REP1/"
          
# Directory where the data is
datapath=${mainpath}"data/"

# Directory where the output will be stored
mkdir -p "."
outputpath="./"

# Total number of molecules:
Mtot=1

# Number of ssDNA oligonucleotides per molecule
nss=2

# Number of nucleotides per ssDNA (in dsDNA this is also equal to the number of base-pairs)
N=50

# Starting timestep
start=0

# The dump frequency
dumpfreq=10000

# Last timestep of the simulation
last=1000000

# oxDNA version (either 1 or 2)
version=2


#####################
#compile the program#
#####################
#Compilation of the programm (Ubuntu)
c++ -std=c++11 stiffness.cpp -o stiffness.out -lm

./stiffness.out ${datapath} ${outputpath} ${start} ${dumpfreq} ${last} ${N} ${version}

rm stiffness.out
