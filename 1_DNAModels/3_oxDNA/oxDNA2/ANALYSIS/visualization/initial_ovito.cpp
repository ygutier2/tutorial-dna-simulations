/*
 * initial_ovito.cpp
 *
 *  Created on: 4 Jun 2022
 *      Author: ygutier2
 */


#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision

using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375

#define SMALL 1e-20

// function declaration
vector<double> quatproduct(vector<double> P, vector<double> Q);
vector<double> q_to_ex(vector<double> Q);
vector<double> q_to_ey(vector<double> Q);
vector<double> q_to_ez(vector<double> Q);
double sign(double a);
double length(vector<double> a);
double dotProduct (vector<double> a, vector<double> b);
vector<double> crossProduct(vector<double> a, vector<double> b);
void fix_roundoff(double &a);
vector<vector <double> > matrixOrthogonal(vector<double> P, vector<double> Q, vector<double> R);
vector<vector <double> > matrixTranspose(vector<vector <double> > M);
vector<vector <double> > matrixProduct(vector<vector <double> > P, vector<vector <double> > Q);
double matrixTrace(vector<vector <double> > M);

int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<9)
    {
        cout << "Eight arguments are required by the programm:"  << endl;
    }


if(argc==9)
{
    cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/

    //Total number of molecules
    int Mtot;
    Mtot = atoi(argv[3]);

    //Number of ssDNA-oligonucleotides per molecule;
    int nss;
    nss = atoi(argv[4]);

    //Total number of ssDNA oligos in the system
    int M = Mtot*nss;

    //Number of nucleotides per single strand
    int N;
    N = atoi(argv[5]);

    //Total number of particles in the system:
    int Ntot = M*N;

    /*Do the calculation at this timestep*/
    int timestep;
    timestep = atoi(argv[6]);

    /*Choose the oxDNA version you are using to compute the hb and bb location*/
    int version;
    version = atoi(argv[7]);

    //Topology of the system: (0) linear, (1) ring
    int flagtopo;
    flagtopo = atoi(argv[8]);

    int Nbonds;
    if(flagtopo==0){Nbonds=((N-1)+N)*M;}
    if(flagtopo==1){Nbonds=(N+N)*M;}

    /*the following variables will be useful to read the data file*/
    int id, mol, type, nx, ny, nz, tt, nn;
    double x, y, z, q0, q1, q2, q3;

    double Lx, Ly, Lz;
    double Lmaxx,Lminx,
           Lmaxy,Lminy,
           Lmaxz,Lminz;

    vector< vector< vector<double> > > quaternion(M, vector<vector<double> >(4, vector<double>(N)));
    vector< vector< vector<double> > > com(M, vector<vector<double> >(3, vector<double>(N)));
    vector< vector< vector<double> > > hb(M, vector<vector<double> >(3, vector<double>(N)));
    vector< vector< vector<double> > > bb(M, vector<vector<double> >(3, vector<double>(N)));
    vector<vector <int> > atomid(M, vector<int>(N));
    vector<vector <int> > atomtype(M, vector<int>(N));

    /*Usefull when reading input files*/
    char name1[10]    = "out";
    char readFile[250] = {'\0'};
    string dummy;
    string line;

    /*********************/
    /**Reads input file**/
    /*********************/
    sprintf(readFile, "%s%s%d.data",argv[1],name1,timestep);
    ifstream read(readFile);
    if(!read){cout << "Error while opening data file!"<<endl;}

    if (read.is_open())
    {
        //Read the first 9 lines
        for(int i=0; i<9; i++)
        {
            if(i==1)
            {
                read >> tt;
                cout << "read timestep: " << tt << endl;
            }

            if(i==3)
            {
                read >> nn;
                if(Ntot!=nn){
                    cout << "ERROR: The expected number of particles does not match the reading ones" << endl;
                    exit(EXIT_SUCCESS);
                }
                if(Ntot==nn) {cout << "read number of base-pairs: " << nn << endl;}
            }

            //Read The box size
            if(i==5)
            {
                read >> Lminx >> Lmaxx;
                //cout << "i=" << i << "  Lx " << Lminx << " " << Lmaxx <<endl;
                Lx = Lmaxx-Lminx;
            }

            if(i==6)
            {
                read >> Lminy >> Lmaxy;
                //cout << "i=" << i << "  Ly " << Lminy << " " << Lmaxy <<endl;
                Ly = Lmaxy-Lminy;
            }

            if(i==7)
            {
                read >> Lminz >> Lmaxz;
                //cout << "i=" << i << "  Lz " << Lminz << " " << Lmaxz <<endl;
                Lz = Lmaxz-Lminz;
                getline(read,dummy);
            }

            else{ getline(read,line);}
        }

        // Continue reading the particles
        for(int i=0; i<Ntot; i++)
        {
            read >> id >> mol >> type >> x >> y >> z >> nx >> ny >> nz >> q0 >> q1 >> q2 >> q3;

            int m = floor((double)(id-1)/(double)(1.0*N));

            if(m==mol-1){
                atomid[m][(id-1)%N]   = id;
                atomtype[m][(id-1)%N] = type;
                com[m][0][(id-1)%N]   = x+(Lx*nx);
                com[m][1][(id-1)%N]   = y+(Ly*ny);
                com[m][2][(id-1)%N]   = z+(Lz*nz);

                quaternion[m][0][(id-1)%N] = q0;
                quaternion[m][1][(id-1)%N] = q1;
                quaternion[m][2][(id-1)%N] = q2;
                quaternion[m][3][(id-1)%N] = q3;

                //cout << m << " " << id << " " << (id-1)%N << endl;

            }
        }
        read.close();
    }

/*//Check
    for (int t=1; t<frames; t++)
    {
        for (int i=0; i<nbp; i++)
        {
            int j = nbp-1-i;
            cout << t << " " << idS1[t][i] << " " << idS2[t][j] << endl;
        }
    }
*/


    /**************************************/
    /**Compute HB and phosphate positions**/
    /**************************************/
    for (int m=0; m<M; m++)
    {
        for(int n=0; n<N; n++ )
        {
            x = com[m][0][n]; y = com[m][1][n]; z = com[m][2][n];

            q0 = quaternion[m][0][n];
            q1 = quaternion[m][1][n];
            q2 = quaternion[m][2][n];
            q3 = quaternion[m][3][n];

            vector<double> Q{q0, q1, q2, q3};

            vector<double> ex(3), ey(3), ez(3);

            ex = q_to_ex(Q); ey = q_to_ey(Q); ez = q_to_ez(Q);

            //oxdna1 backbone
            if(version==1)
            {
                bb[m][0][n] = x - 0.40*ex[0];
                bb[m][1][n] = y - 0.40*ex[1];
                bb[m][2][n] = z - 0.40*ex[2];
            }

            //oxdna2 backbone
            if(version==2)
            {
                bb[m][0][n] = x - 0.34*ex[0] + 0.3408*ey[0];
                bb[m][1][n] = y - 0.34*ex[1] + 0.3408*ey[1];
                bb[m][2][n] = z - 0.34*ex[2] + 0.3408*ey[2];
            }


            //The hydrogen bond sites
            hb[m][0][n] = x + 0.40*ex[0];
            hb[m][1][n] = y + 0.40*ex[1];
            hb[m][2][n] = z + 0.40*ex[2];

            //cout << m << " " << n << endl;
        }
    }


    //PRINT THE VISUALIZATION
    // for ovito we use oblate particles for the bases
    //Shape sphere
    double spx = 0.2; double spy = 0.2; double spz = 0.2;

    //Shape ellipsoid
    double elx = 0.2; double ely = 0.17; double elz = 0.1;

    char writeFile1 [250] = {'\0'};
    char name3[100] ="topology.ovito.txt";
    sprintf(writeFile1, "%sN%d.oxDNA%d.%s", argv[2], N, version, name3);
    ofstream write1(writeFile1);
    cout << "writing on .... " << writeFile1 <<endl;

    //set precision and the number of decimals to be printed always
    write1.precision(5);
    write1.setf(ios::fixed);
    write1.setf(ios::showpoint);

    //Properties=pos:R:3:orientation:R:4:aspherical_shape:R:3
    write1 << "# LAMMPS data file"        << endl;
    write1 << 2*Ntot << " atoms"          << endl;
    write1 << 2*Ntot << " ellipsoids"     << endl;
    write1 << Nbonds << " bonds\n"        << endl;

    write1 << "14 atom types"   << endl;
    write1 << "1  bond types\n" << endl;

    write1 << Lminx << " " << Lmaxx  << " xlo xhi"   << endl;
    write1 << Lminy << " " << Lmaxy  << " ylo yhi"   << endl;
    write1 << Lminz << " " << Lmaxz  << " zlo zhi\n" << endl;

    write1 << "Masses\n"   << endl;
    for(int i=0; i<14;i++){
    	 write1 << i+1 << " " << 3.1575 << endl;
    }

    //write1 << "\n#ITEM: ATOMS id type ellipsoid-flag density x y z" << endl;
    //Ovito reads the quaternions as (w, x, y, z) which means from lammps dump (quat[3], quat[0], quat[1], quat[2] )
    write1 << "\nAtoms   # ellipsoid\n" << endl;

    for(int m=0; m<M; m++ )
    {
        for(int n=0; n<N; n++ )
        {
            //Backbone (change the type of molecule)
            int newtype;
            if(atomtype[m][n] == 1){newtype=11;}
            if(atomtype[m][n] == 2){newtype=12;}
            if(atomtype[m][n] == 3){newtype=13;}
            if(atomtype[m][n] == 4){newtype=14;}
            write1 << 2*atomid[m][n]-1 << " " <<  newtype        << " 1 1 " << bb[m][0][n]  << " " << bb[m][1][n] << " " << bb[m][2][n] << endl;

            //Hydrogen-Bond (is the same type as the original molecule)
            write1 << 2*atomid[m][n]   << " " <<  atomtype[m][n] << " 1 1 " << hb[m][0][n]  << " " << hb[m][1][n] << " " << hb[m][2][n] << endl;

        }

    }

    write1 << "\nBonds\n" << endl;
    int bid=0;
    //backbone
    for(int m=0; m<M; m++ )
    {
    	for(int n=0; n<N-1; n++ )
    	{
    		bid += 1;
    		int b1 = 2*atomid[m][n]-1;
            int b2 = b1+2;
            write1 << bid << " 1 " << b1 << " " << b2 << endl;
    	}

    	//Add backbone connecting ends
    	if(flagtopo==1){
            bid += 1;
            int b1 = 2*atomid[m][N-1]-1;
            int b2 = 2*atomid[m][0]-1;
            write1 << bid << " 1 " << b1 << " " << b2 << endl;
    	}
    }

    //Hydrogen bond
    for(int m=0; m<M; m++ )
    {
    	for(int n=0; n<N; n++ )
       	{
       		bid += 1;
       		int b1 = 2*atomid[m][n]-1;
            int b2 = b1+1;
            write1 << bid << " 1 " << b1 << " " << b2 << endl;
       	}
    }


    write1.close();
    write1.clear();

}








return 0;
}



//Quaternion product
vector<double> quatproduct(vector<double> P, vector<double> Q)
{
    vector<double> R(4);
    R[0] = P[0]*Q[0] - P[1]*Q[1] - P[2]*Q[2] - P[3]*Q[3];
    R[1] = P[0]*Q[1] + Q[0]*P[1] + P[2]*Q[3] - P[3]*Q[2];
    R[2] = P[0]*Q[2] + Q[0]*P[2] - P[1]*Q[3] + P[3]*Q[1];
    R[3] = P[0]*Q[3] + Q[0]*P[3] + P[1]*Q[2] - P[2]*Q[1];

    return R;
}

// converts quaternion DOF into local body reference frame
vector<double> q_to_ex(vector<double> Q)
{
    vector<double> ex(3);

    ex[0]=Q[0]*Q[0]+Q[1]*Q[1]-Q[2]*Q[2]-Q[3]*Q[3];
    ex[1]=2*(Q[1]*Q[2]+Q[0]*Q[3]);
    ex[2]=2*(Q[1]*Q[3]-Q[0]*Q[2]);
    return ex;
}


vector<double> q_to_ey(vector<double> Q)
{
    vector<double> ey(3);

    ey[0]=2*(Q[1]*Q[2]-Q[0]*Q[3]);
    ey[1]=Q[0]*Q[0]-Q[1]*Q[1]+Q[2]*Q[2]-Q[3]*Q[3];
    ey[2]=2*(Q[2]*Q[3]+Q[0]*Q[1]);
    return ey;
}


vector<double> q_to_ez(vector<double> Q)
{
    vector<double> ez(3);

    ez[0]=2*(Q[1]*Q[3]+Q[0]*Q[2]);
    ez[1]=2*(Q[2]*Q[3]-Q[0]*Q[1]);
    ez[2]=Q[0]*Q[0]-Q[1]*Q[1]-Q[2]*Q[2]+Q[3]*Q[3];
    return ez;
}


//Sign function
double sign(double a) { if (a>0) {return 1.0;} else {return -1.0;} }


//length
double length(vector<double> a)
{
  double r;
  r = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
  return r;
}

//Dot product
double dotProduct (vector<double> a, vector<double> b)
{
  double r;
  r = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
  return r;
}

//Cross product
vector<double> crossProduct(vector<double> a, vector<double> b)
{
  vector<double> r(3);
  r[0] = a[1]*b[2]-a[2]*b[1];
  r[1] = a[2]*b[0]-a[0]*b[2];
  r[2] = a[0]*b[1]-a[1]*b[0];

  return r;
}


// checks for round off in a number in the interval [-1,1] before using asin or acos
void fix_roundoff(double &a)
{
  if (abs(a)>1.001)
  {
    std::cout<<"Error - number should be in interval [-1,1] but is "<< a << endl;
    exit(0);
  }
  if (a>1){a=1.0;}
  if (a<-1){a=-1.0;}
}


//The orthogonal matrix. This is the rotation matrix transforming the canonical frame into the frame of the respective triad
vector<vector <double> > matrixOrthogonal(vector<double> a, vector<double> b, vector<double> c)
{
  vector<vector <double> > T(3, vector<double>(3));

  T[0][0] = a[0];    T[0][1] = b[0];    T[0][2] = c[0];
  T[1][0] = a[1];    T[1][1] = b[1];    T[1][2] = c[1];
  T[2][0] = a[2];    T[2][1] = b[2];    T[2][2] = c[2];

  return T;
}


vector<vector <double> > matrixTranspose(vector<vector <double> > a)
{
  vector<vector <double> > M(3, vector<double>(3));

  for(int i=0; i<3; i++)
  {
      for(int j=0; j<3; j++)
      {
          M[i][j] = a[j][i];
      }
  }

  return M;
}


vector<vector <double> > matrixProduct(vector<vector <double> > a, vector<vector <double> > b)
{
  vector<vector <double> > M(3, vector<double>(3));

  for(int i=0; i<3; i++)
  {
      for(int j=0; j<3; j++)
      {
          double s=0;

          for(int k=0; k<3; k++)
          {
              s = s + a[i][k]*b[k][j];

              M[i][j] = s;
          }
      }
  }

  return M;
}


double matrixTrace(vector<vector <double> > a)
{
  double r;
  r = a[0][0] + a[1][1] + a[2][2];
  return r;
}

