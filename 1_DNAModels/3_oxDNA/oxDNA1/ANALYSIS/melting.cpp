#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision

using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375

#define SMALL 1e-20

// function declaration
vector<double> quatproduct(vector<double> P, vector<double> Q);           
vector<double> q_to_ex(vector<double> Q);
vector<double> q_to_ey(vector<double> Q);
vector<double> q_to_ez(vector<double> Q);
double sign(double a);
double length(vector<double> a);
double dotProduct (vector<double> a, vector<double> b);
vector<double> crossProduct(vector<double> a, vector<double> b);
void fix_roundoff(double &a);
vector<vector <double> > matrixOrthogonal(vector<double> P, vector<double> Q, vector<double> R);
vector<vector <double> > matrixTranspose(vector<vector <double> > M);
vector<vector <double> > matrixProduct(vector<vector <double> > P, vector<vector <double> > Q);
double matrixTrace(vector<vector <double> > M);

vector<vector <double> > matrixInverse(vector<vector <double> > M);
vector<vector <double> > matrixTimesConstant(vector<vector <double> > P, double Q);
void matrixPrint(vector<vector <double> > M);
double matrixDet(vector<vector <double> > M);


int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<8)
    {
        cout << "Eleven arguments are required by the programm:"                      << endl;
        cout << "1.- Directory where the data is"                          << endl;
        cout << "2.- Directory where the output will be stored"            << endl;
        cout << "3.- Starting timestep" << endl; 
        cout << "4.- The dump frequency" <<endl;
        cout << "5.- Last timestep of the simulation" << endl;
        cout << "6.- number of base-pairs" << endl;       
        cout << "7.- oxDNA version" << endl;
    }


if(argc==8)
{   
    cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    
    /*Start the calculation from this timestep*/
    int equiltime;
    equiltime = atoi(argv[3]);
    
    /*Dump frequency*/
    int dumpfreq;
    dumpfreq = atoi(argv[4]);
    
    /*Last timestep*/
    int totrun;
    totrun = atoi(argv[5]);
    
    /*number of base-pairs*/  
    int nbp;
    nbp = atoi(argv[6]);

    /*Choose the oxDNA version you are using to compute the hb and bb location*/
    int version;         
    version = atoi(argv[7]);                        
                                                    
    int frames = 1 + (totrun-equiltime)/dumpfreq;
    int num1, num2, num3;

    /*Number of nucleotides (2*(nbp))*/
    int N = 2*(nbp);

    /*the following variables will be useful to read the data file*/
    int id, mol, type, nx, ny, nz, tt;
    double x, y, z, q0, q1, q2, q3;
 
    double Lx, Ly, Lz;
    double Lmaxx,Lminx,
           Lmaxy,Lminy,
           Lmaxz,Lminz;

    vector< vector< vector<double> > > quatS1(frames, vector<vector<double> >(4, vector<double>(nbp)));
    vector< vector< vector<double> > > comS1 (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > hbS1  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > bbS1  (frames, vector<vector<double> >(3, vector<double>(nbp)));

    vector< vector< vector<double> > > quatS2(frames, vector<vector<double> >(4, vector<double>(nbp)));
    vector< vector< vector<double> > > comS2 (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > hbS2  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > bbS2  (frames, vector<vector<double> >(3, vector<double>(nbp)));

    vector<vector <int> > idS1(frames, vector<int>(nbp));
    vector<vector <int> > idS2(frames, vector<int>(nbp));

    vector<vector <int> > typeS1(frames, vector<int>(nbp));
    vector<vector <int> > typeS2(frames, vector<int>(nbp));
    
    vector<vector <int> > melting(frames, vector<double>(int));
    vector<double> fmelt(frames);
    
    
      
    /*Usefull when reading input files*/
    int  timestep;
    char name1[10]    = "out";
    char readFile[250] = {'\0'};
    string dummy; 


    /*Usefull when writing output files*/
    string line;
    
    /*********************/
    /**Reads input files**/
    /*********************/    
    //Inside the file, the number of rows assigned per time-frame is: 9, 2*nbp and 1 (bigbead);
    int nlinespf=9+(2*nbp);

    for (int t=0; t<frames; t++)
    {  
        timestep = equiltime+dumpfreq*t;
        sprintf(readFile, "%s%s%d.data",argv[1], name1,timestep);
        ifstream read(readFile);
        if(!read){cout << "Error while opening data file!"<<endl;}

        if (read.is_open())    
        {
            //Read the first 9 lines
            for(int i=0; i<9; i++)   
            {
                if(i==1)
                {
                    read >> tt;
                    cout << "read timestep: " << tt << endl;
                }
                
                if(i==3)
                {
                    read >> N;
                    cout << "read number of base-pairs: " << N << endl;
                }
                
                //Read The box size
                if(i==5)
                {
                    read >> Lminx >> Lmaxx;
                    //cout << "i=" << i << "  Lx " << Lminx << " " << Lmaxx <<endl;
                    Lx = Lmaxx-Lminx;
                }

                if(i==6)
                {
                    read >> Lminy >> Lmaxy;
                    //cout << "i=" << i << "  Ly " << Lminy << " " << Lmaxy <<endl;
                    Ly = Lmaxy-Lminy;
                }

                if(i==7)
                {
                    read >> Lminz >> Lmaxz;
                    //cout << "i=" << i << "  Lz " << Lminz << " " << Lmaxz <<endl;
                    Lz = Lmaxz-Lminz;
                    getline(read,dummy);
                }
                    
                else{ getline(read,line);}
            }                
                    
            // Continue reading the particles
            for(int i=0; i<N; i++)   
            {
                read >> id >> mol >> type >> x >> y >> z >> nx >> ny >> nz >> q0 >> q1 >> q2 >> q3;

                //First strand
                if(id-1 < nbp)
                {
                    idS1[t][id-1] = id-1;
                    typeS1[t][id-1] = type;
                    comS1[t][0][id-1] = x+(Lx*nx);
                    comS1[t][1][id-1] = y+(Ly*ny);
                    comS1[t][2][id-1] = z+(Lz*nz);

                    quatS1[t][0][id-1] = q0;
                    quatS1[t][1][id-1] = q1;
                    quatS1[t][2][id-1] = q2;
                    quatS1[t][3][id-1] = q3;
                }

                //Second strand
                if((id-1 >= nbp) && (id-1 < 2*nbp))
                {
                    idS2[t][id-1-nbp] = id-1;
                    typeS2[t][id-1-nbp] = type;
                    comS2[t][0][id-1-nbp] = x+(Lx*nx);
                    comS2[t][1][id-1-nbp] = y+(Ly*ny);
                    comS2[t][2][id-1-nbp] = z+(Lz*nz);

                    quatS2[t][0][id-1-nbp] = q0;
                    quatS2[t][1][id-1-nbp] = q1;
                    quatS2[t][2][id-1-nbp] = q2;
                    quatS2[t][3][id-1-nbp] = q3;
                 }
             }  
         }
         read.close();
     }

/*//Check
    for (int t=1; t<frames; t++)
    {
        for (int i=0; i<nbp; i++)
        {
            int j = nbp-1-i;
            cout << t << " " << idS1[t][i] << " " << idS2[t][j] << endl;
        }
    }
*/


    /**************************************/
    /**Compute HB and phosphate positions**/
    /**************************************/
    double x1, y1, z1, q01, q11, q21, q31;
    double x2, y2, z2, q02, q12, q22, q32;

    for (int t=0; t<frames; t++)
    {
        //fraction of melted base-pairs
        int theta=0;
        for(int i=0; i<nbp; i++ )
        {
            int j = nbp-1-i;
            //position of nucleotides in first strand
            x1  =  comS1[t][0][i]; y1  =  comS1[t][1][i]; z1  =  comS1[t][2][i];
 
            //position of complementary nucleotide in second strand (thats why the index is j)
            x2  =  comS2[t][0][j]; y2  =  comS2[t][1][j]; z2  =  comS2[t][2][j];

            q01 = quatS1[t][0][i]; q11 = quatS1[t][1][i]; q21 = quatS1[t][2][i]; q31 = quatS1[t][3][i];
            q02 = quatS2[t][0][j]; q12 = quatS2[t][1][j]; q22 = quatS2[t][2][j]; q32 = quatS2[t][3][j];
            
            vector<double> qs1{q01, q11, q21, q31};
            vector<double> qs2{q02, q12, q22, q32};

            vector<double> ex1(3), ey1(3), ez1(3);
            vector<double> ex2(3), ey2(3), ez2(3);

            ex1 = q_to_ex(qs1); ey1 = q_to_ey(qs1); ez1 = q_to_ez(qs1);
            ex2 = q_to_ex(qs2); ey2 = q_to_ey(qs2); ez2 = q_to_ez(qs2);

            //oxdna1 backbone of a base-pair
            if(version==1)
            {
                bbS1[t][0][i] = x1 - 0.40*ex1[0];
                bbS1[t][1][i] = y1 - 0.40*ex1[1];
                bbS1[t][2][i] = z1 - 0.40*ex1[2];

                bbS2[t][0][i] = x2 - 0.40*ex2[0];
                bbS2[t][1][i] = y2 - 0.40*ex2[1];
                bbS2[t][2][i] = z2 - 0.40*ex2[2];
            }

            //oxdna2 backbone of a base-pair
            if(version==2)
            {
                bbS1[t][0][i] = x1 - 0.34*ex1[0] + 0.3408*ey1[0];
                bbS1[t][1][i] = y1 - 0.34*ex1[1] + 0.3408*ey1[1];
                bbS1[t][2][i] = z1 - 0.34*ex1[2] + 0.3408*ey1[2];

                bbS2[t][0][i] = x2 - 0.34*ex2[0] + 0.3408*ey2[0];
                bbS2[t][1][i] = y2 - 0.34*ex2[1] + 0.3408*ey2[1];
                bbS2[t][2][i] = z2 - 0.34*ex2[2] + 0.3408*ey2[2];
            }


            //The hydrogen bond sites of a base-pair
            hbS1[t][0][i] = x1 + 0.40*ex1[0];
            hbS1[t][1][i] = y1 + 0.40*ex1[1];
            hbS1[t][2][i] = z1 + 0.40*ex1[2];

            hbS2[t][0][i] = x2 + 0.40*ex2[0];
            hbS2[t][1][i] = y2 + 0.40*ex2[1];
            hbS2[t][2][i] = z2 + 0.40*ex2[2];
            
            //Distance between HBsites in abase-pair
            double dx = hbS2[t][0][i]-hbS1[t][0][i];
            double dy = hbS2[t][1][i]-hbS1[t][1][i];
            double dz = hbS2[t][2][i]-hbS1[t][2][i];
            
            double dd = sqrt(dx*dx + dy*dy +dz*dz);
            
            //It is not melted
            if(dd<1.0){melting[t][i]=1;}
            
            //It is melted
            if(dd<1.0){melting[t][i]=0;theta++;}
        }
        
        fmelt[t]=(double)theta/(double)nbp;
    }
    
    /*Fraction of melted bp vs time*/
    char name5[30] ="theta_vs_time.dat";
    char writeFile2 [250] = {'\0'};
    sprintf(writeFile2, "%s%s",argv[2], name5);
    ofstream write2(writeFile2);
    cout << "writing on .... " << writeFile2 <<endl;

    for (int t=0; t<frames; t++) 
    {
        write2 << t << " " << fmelt[t] << endl;
    }
    write2.close();
    write2.clear();


    /*Kymograph*/
    char name6[30] ="kymograph.dat";
    char writeFile3 [250] = {'\0'};
    sprintf(writeFile3, "%s%s",argv[2], name6);
    ofstream write3(writeFile3);
    cout << "writing on .... " << writeFile3 <<endl;

    for (int t=0; t<frames; t++) 
    {
        for(int i=0; i<nbp; i++ )
        {
            if(melting[t][i]==1){
                write3 << t << " " << i+1 << endl;
            }
        }
    }
    write3.close();
    write3.clear();


        
  
 
  
    

  
    
    










}








return 0;
}



//Quaternion product
vector<double> quatproduct(vector<double> P, vector<double> Q)
{
    vector<double> R(4);
    R[0] = P[0]*Q[0] - P[1]*Q[1] - P[2]*Q[2] - P[3]*Q[3];
    R[1] = P[0]*Q[1] + Q[0]*P[1] + P[2]*Q[3] - P[3]*Q[2];
    R[2] = P[0]*Q[2] + Q[0]*P[2] - P[1]*Q[3] + P[3]*Q[1];
    R[3] = P[0]*Q[3] + Q[0]*P[3] + P[1]*Q[2] - P[2]*Q[1];

    return R;
}

// converts quaternion DOF into local body reference frame
vector<double> q_to_ex(vector<double> Q)
{
    vector<double> ex(3);

    ex[0]=Q[0]*Q[0]+Q[1]*Q[1]-Q[2]*Q[2]-Q[3]*Q[3];
    ex[1]=2*(Q[1]*Q[2]+Q[0]*Q[3]);
    ex[2]=2*(Q[1]*Q[3]-Q[0]*Q[2]);
    return ex;
}


vector<double> q_to_ey(vector<double> Q)
{
    vector<double> ey(3);

    ey[0]=2*(Q[1]*Q[2]-Q[0]*Q[3]);
    ey[1]=Q[0]*Q[0]-Q[1]*Q[1]+Q[2]*Q[2]-Q[3]*Q[3];
    ey[2]=2*(Q[2]*Q[3]+Q[0]*Q[1]);
    return ey;
}


vector<double> q_to_ez(vector<double> Q)
{
    vector<double> ez(3);

    ez[0]=2*(Q[1]*Q[3]+Q[0]*Q[2]);
    ez[1]=2*(Q[2]*Q[3]-Q[0]*Q[1]);
    ez[2]=Q[0]*Q[0]-Q[1]*Q[1]-Q[2]*Q[2]+Q[3]*Q[3];
    return ez;
}


//Sign function
double sign(double a) { if (a>0) {return 1.0;} else {return -1.0;} } 


//length
double length(vector<double> a)
{
  double r;
  r = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
  return r;
}

//Dot product
double dotProduct (vector<double> a, vector<double> b)
{
  double r;
  r = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
  return r;
}

//Cross product
vector<double> crossProduct(vector<double> a, vector<double> b)
{
  vector<double> r(3);
  r[0] = a[1]*b[2]-a[2]*b[1];
  r[1] = a[2]*b[0]-a[0]*b[2];
  r[2] = a[0]*b[1]-a[1]*b[0];
  
  return r;
}


// checks for round off in a number in the interval [-1,1] before using asin or acos
void fix_roundoff(double &a)
{
  if (abs(a)>1.001) 
  {
    std::cout<<"Error - number should be in interval [-1,1] but is "<< a << endl;
    exit(0);
  }
  if (a>1){a=1.0;}
  if (a<-1){a=-1.0;}
}


//The orthogonal matrix. This is the rotation matrix transforming the canonical frame into the frame of the respective triad
vector<vector <double> > matrixOrthogonal(vector<double> a, vector<double> b, vector<double> c)
{
  vector<vector <double> > T(3, vector<double>(3));

  T[0][0] = a[0];    T[0][1] = b[0];    T[0][2] = c[0];
  T[1][0] = a[1];    T[1][1] = b[1];    T[1][2] = c[1];
  T[2][0] = a[2];    T[2][1] = b[2];    T[2][2] = c[2];

  return T;
}


vector<vector <double> > matrixTranspose(vector<vector <double> > a)
{
  vector<vector <double> > M(3, vector<double>(3));

  for(int i=0; i<3; i++)
  {
      for(int j=0; j<3; j++)
      {
          M[i][j] = a[j][i];
      }
  }

  return M;
}


vector<vector <double> > matrixProduct(vector<vector <double> > a, vector<vector <double> > b)
{
  vector<vector <double> > M(3, vector<double>(3));

  for(int i=0; i<3; i++)
  {
      for(int j=0; j<3; j++)
      {
          double s=0;
          
          for(int k=0; k<3; k++)
          {
              s = s + a[i][k]*b[k][j];

              M[i][j] = s;
          }
      }
  }

  return M;
}
     

double matrixTrace(vector<vector <double> > a)
{
  double r;
  r = a[0][0] + a[1][1] + a[2][2];
  return r;
}



double matrixDet(vector<vector <double> > a)
{
  double r = a[0][0]*(a[1][1]*a[2][2]-a[2][1]*a[1][2]) - a[0][1]*(a[1][0]*a[2][2]-a[2][0]*a[1][2]) + a[0][2]*(a[1][0]*a[2][1]-a[2][0]*a[1][1]);
  
  return r;
}



vector<vector <double> > matrixTimesConstant(vector<vector <double> > a, double b)
{
  vector<vector <double> > M(3, vector<double>(3));

  for(int i=0; i<3; i++)
  {
      for(int j=0; j<3; j++)
      {
          M[i][j] = b*a[i][j]; 
      }
  }
  
  return M;
}


vector<vector <double> > matrixInverse(vector<vector <double> > a)
{
  vector<vector <double> > M(3, vector<double>(3));

  double det = matrixDet(a);
  double invdet = 1.0/det;

  M[0][0] = (a[1][1] * a[2][2] - a[2][1] * a[1][2]) * invdet;
  M[0][1] = (a[0][2] * a[2][1] - a[0][1] * a[2][2]) * invdet;
  M[0][2] = (a[0][1] * a[1][2] - a[0][2] * a[1][1]) * invdet;
  M[1][0] = (a[1][2] * a[2][0] - a[1][0] * a[2][2]) * invdet;
  M[1][1] = (a[0][0] * a[2][2] - a[0][2] * a[2][0]) * invdet;
  M[1][2] = (a[1][0] * a[0][2] - a[0][0] * a[1][2]) * invdet;
  M[2][0] = (a[1][0] * a[2][1] - a[2][0] * a[1][1]) * invdet;
  M[2][1] = (a[2][0] * a[0][1] - a[0][0] * a[2][1]) * invdet;
  M[2][2] = (a[0][0] * a[1][1] - a[1][0] * a[0][1]) * invdet;
  
  return M;
}



void matrixPrint(vector<vector <double> > a)
{
  for(int i=0; i<3; i++)
  {
      for(int j=0; j<3; j++)
      {
          cout << a[i][j] << " ";
      }
      cout << endl;
  }
  cout << endl;
  cout << endl;
}





