#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision

using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375

#define SMALL 1e-20

// function declaration
vector<double> quatproduct(vector<double> P, vector<double> Q);           
vector<double> q_to_ex(vector<double> Q);
vector<double> q_to_ey(vector<double> Q);
vector<double> q_to_ez(vector<double> Q);
double sign(double a);
double length(vector<double> a);
double dotProduct (vector<double> a, vector<double> b);
vector<double> crossProduct(vector<double> a, vector<double> b);
void fix_roundoff(double &a);
vector<vector <double> > matrixOrthogonal(vector<double> P, vector<double> Q, vector<double> R);
vector<vector <double> > matrixTranspose(vector<vector <double> > M);
vector<vector <double> > matrixProduct(vector<vector <double> > P, vector<vector <double> > Q);
double matrixTrace(vector<vector <double> > M);

vector<vector <double> > matrixInverse(vector<vector <double> > M);
vector<vector <double> > matrixTimesConstant(vector<vector <double> > P, double Q);
void matrixPrint(vector<vector <double> > M);
double matrixDet(vector<vector <double> > M);


int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<8)
    {
        cout << "Eleven arguments are required by the programm:"                      << endl;
        cout << "1.- Directory where the data is"                          << endl;
        cout << "2.- Directory where the output will be stored"            << endl;
        cout << "3.- Starting timestep" << endl; 
        cout << "4.- The dump frequency" <<endl;
        cout << "5.- Last timestep of the simulation" << endl;
        cout << "6.- number of base-pairs" << endl;       
        cout << "7.- oxDNA version" << endl;
    }


if(argc==8)
{   
    cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    
    /*Start the calculation from this timestep*/
    int equiltime;
    equiltime = atoi(argv[3]);
    
    /*Dump frequency*/
    int dumpfreq;
    dumpfreq = atoi(argv[4]);
    
    /*Last timestep*/
    int totrun;
    totrun = atoi(argv[5]);
    
    /*number of base-pairs*/  
    int nbp;
    nbp = atoi(argv[6]);

    /*Choose the oxDNA version you are using to compute the hb and bb location*/
    int version;         
    version = atoi(argv[7]);                        
                                                    
    int frames = 1 + (totrun-equiltime)/dumpfreq;
    int num1, num2, num3;

    /*Number of nucleotides (2*(nbp))*/
    int N = 2*(nbp);

    /*the following variables will be useful to read the data file*/
    int id, mol, type, nx, ny, nz, tt;
    double x, y, z, q0, q1, q2, q3;
 
    double Lx, Ly, Lz;
    double Lmaxx,Lminx,
           Lmaxy,Lminy,
           Lmaxz,Lminz;

    vector< vector< vector<double> > > quatS1(frames, vector<vector<double> >(4, vector<double>(nbp)));
    vector< vector< vector<double> > > comS1 (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > hbS1  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > bbS1  (frames, vector<vector<double> >(3, vector<double>(nbp)));

    vector< vector< vector<double> > > quatS2(frames, vector<vector<double> >(4, vector<double>(nbp)));
    vector< vector< vector<double> > > comS2 (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > hbS2  (frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > bbS2  (frames, vector<vector<double> >(3, vector<double>(nbp)));

    vector<vector <int> > idS1(frames, vector<int>(nbp));
    vector<vector <int> > idS2(frames, vector<int>(nbp));

    vector<vector <int> > typeS1(frames, vector<int>(nbp));
    vector<vector <int> > typeS2(frames, vector<int>(nbp));
    
    
    vector< vector< vector<double> > > centerline(frames, vector<vector<double> >(3, vector<double>(nbp)));
    
    /*End to end distance*/    
    vector <double> e2edist(frames);

    /*The local reference frame per base-pair*/
    vector< vector< vector<double> > > E1(frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > E2(frames, vector<vector<double> >(3, vector<double>(nbp)));
    vector< vector< vector<double> > > E3(frames, vector<vector<double> >(3, vector<double>(nbp)));

    vector< vector<double> > ai(frames, vector<double> (nbp-1));
    vector<double>L(frames);
    
    /*Thetas: local angle deformations */
    double theta, theta1, theta2, theta3;
    vector< vector<double> > localtheta1(frames, vector<double> (nbp-1));
    vector< vector<double> > localtheta2(frames, vector<double> (nbp-1));
    vector< vector<double> > localtheta3(frames, vector<double> (nbp-1));
    
    int navoid=5;
    int Nn=nbp-1-2*navoid;
    
    vector<vector <double> > vec_theta1(frames, vector<double>(Nn));
    vector<vector <double> > vec_theta2(frames, vector<double>(Nn));
    vector<vector <double> > vec_theta3(frames, vector<double>(Nn));

    vector<double> ave1(frames);
    vector<double> ave2(frames);
    vector<double> ave3(frames);

    vector< vector< vector<double> > > omega(frames, vector<vector<double> >(3, vector<double>(Nn)));
    
    //The average over time for a given base-pair
    vector<double> ave1local(Nn);
    vector<double> ave2local(Nn);
    vector<double> ave3local(Nn);
    
    /*Conversion factor from degree/bp to rad/nm*/
    double a=0.345;
    double fconv=(M_PI/180.0)*(1./a);

    vector<double> totalome1(frames);
    vector<double> totalome2(frames);
    vector<double> totalTw(frames);
      
    /*Usefull when reading input files*/
    int  timestep;
    char name1[10]    = "out";
    char readFile[250] = {'\0'};
    string dummy; 


    /*Usefull when writing output files*/
    string line;
    
    /*********************/
    /**Reads input files**/
    /*********************/    
    //Inside the file, the number of rows assigned per time-frame is: 9, 2*nbp and 1 (bigbead);
    int nlinespf=9+(2*nbp);

    for (int t=0; t<frames; t++)
    {  
        timestep = equiltime+dumpfreq*t;
        sprintf(readFile, "%s%s%d.data",argv[1], name1,timestep);
        ifstream read(readFile);
        if(!read){cout << "Error while opening data file!"<<endl;}

        if (read.is_open())    
        {
            //Read the first 9 lines
            for(int i=0; i<9; i++)   
            {
                if(i==1)
                {
                    read >> tt;
                    //cout << "read timestep: " << tt << endl;
                }
                
                if(i==3)
                {
                    read >> N;
                    //cout << "read number of base-pairs: " << N << endl;
                }
                
                //Read The box size
                if(i==5)
                {
                    read >> Lminx >> Lmaxx;
                    //cout << "i=" << i << "  Lx " << Lminx << " " << Lmaxx <<endl;
                    Lx = Lmaxx-Lminx;
                }

                if(i==6)
                {
                    read >> Lminy >> Lmaxy;
                    //cout << "i=" << i << "  Ly " << Lminy << " " << Lmaxy <<endl;
                    Ly = Lmaxy-Lminy;
                }

                if(i==7)
                {
                    read >> Lminz >> Lmaxz;
                    //cout << "i=" << i << "  Lz " << Lminz << " " << Lmaxz <<endl;
                    Lz = Lmaxz-Lminz;
                    getline(read,dummy);
                }
                    
                else{ getline(read,line);}
            }                
                    
            // Continue reading the particles
            for(int i=0; i<N; i++)   
            {
                read >> id >> mol >> type >> x >> y >> z >> nx >> ny >> nz >> q0 >> q1 >> q2 >> q3;

                //First strand
                if(id-1 < nbp)
                {
                    idS1[t][id-1] = id-1;
                    typeS1[t][id-1] = type;
                    comS1[t][0][id-1] = x+(Lx*nx);
                    comS1[t][1][id-1] = y+(Ly*ny);
                    comS1[t][2][id-1] = z+(Lz*nz);

                    quatS1[t][0][id-1] = q0;
                    quatS1[t][1][id-1] = q1;
                    quatS1[t][2][id-1] = q2;
                    quatS1[t][3][id-1] = q3;
                }

                //Second strand
                if((id-1 >= nbp) && (id-1 < 2*nbp))
                {
                    idS2[t][id-1-nbp] = id-1;
                    typeS2[t][id-1-nbp] = type;
                    comS2[t][0][id-1-nbp] = x+(Lx*nx);
                    comS2[t][1][id-1-nbp] = y+(Ly*ny);
                    comS2[t][2][id-1-nbp] = z+(Lz*nz);

                    quatS2[t][0][id-1-nbp] = q0;
                    quatS2[t][1][id-1-nbp] = q1;
                    quatS2[t][2][id-1-nbp] = q2;
                    quatS2[t][3][id-1-nbp] = q3;
                 }
             }  
         }
         read.close();
     }

/*//Check
    for (int t=1; t<frames; t++)
    {
        for (int i=0; i<nbp; i++)
        {
            int j = nbp-1-i;
            cout << t << " " << idS1[t][i] << " " << idS2[t][j] << endl;
        }
    }
*/


    /**************************************/
    /**Compute HB and phosphate positions**/
    /**************************************/
    double x1, y1, z1, q01, q11, q21, q31;
    double x2, y2, z2, q02, q12, q22, q32;

    for (int t=0; t<frames; t++)
    {
        for(int i=0; i<nbp; i++ )
        {
            x1  =  comS1[t][0][i]; y1  =  comS1[t][1][i]; z1  =  comS1[t][2][i];
            x2  =  comS2[t][0][i]; y2  =  comS2[t][1][i]; z2  =  comS2[t][2][i];

            q01 = quatS1[t][0][i]; q11 = quatS1[t][1][i]; q21 = quatS1[t][2][i]; q31 = quatS1[t][3][i];
            q02 = quatS2[t][0][i]; q12 = quatS2[t][1][i]; q22 = quatS2[t][2][i]; q32 = quatS2[t][3][i];

            vector<double> qs1{q01, q11, q21, q31};
            vector<double> qs2{q02, q12, q22, q32};

            vector<double> ex1(3), ey1(3), ez1(3);
            vector<double> ex2(3), ey2(3), ez2(3);

            ex1 = q_to_ex(qs1); ey1 = q_to_ey(qs1); ez1 = q_to_ez(qs1);
            ex2 = q_to_ex(qs2); ey2 = q_to_ey(qs2); ez2 = q_to_ez(qs2);

            //oxdna1 backbone
            if(version==1)
            {
                bbS1[t][0][i] = x1 - 0.40*ex1[0];
                bbS1[t][1][i] = y1 - 0.40*ex1[1];
                bbS1[t][2][i] = z1 - 0.40*ex1[2];

                bbS2[t][0][i] = x2 - 0.40*ex2[0];
                bbS2[t][1][i] = y2 - 0.40*ex2[1];
                bbS2[t][2][i] = z2 - 0.40*ex2[2];
            }

            //oxdna2 backbone
            if(version==2)
            {
                bbS1[t][0][i] = x1 - 0.34*ex1[0] + 0.3408*ey1[0];
                bbS1[t][1][i] = y1 - 0.34*ex1[1] + 0.3408*ey1[1];
                bbS1[t][2][i] = z1 - 0.34*ex1[2] + 0.3408*ey1[2];

                bbS2[t][0][i] = x2 - 0.34*ex2[0] + 0.3408*ey2[0];
                bbS2[t][1][i] = y2 - 0.34*ex2[1] + 0.3408*ey2[1];
                bbS2[t][2][i] = z2 - 0.34*ex2[2] + 0.3408*ey2[2];
            }


            //The hydrogen bond sites
            hbS1[t][0][i] = x1 + 0.40*ex1[0];
            hbS1[t][1][i] = y1 + 0.40*ex1[1];
            hbS1[t][2][i] = z1 + 0.40*ex1[2];

            hbS2[t][0][i] = x2 + 0.40*ex2[0];
            hbS2[t][1][i] = y2 + 0.40*ex2[1];
            hbS2[t][2][i] = z2 + 0.40*ex2[2];
        }
        
        
        
        /*********************************************/
        /**Compute centerline and bp reference frame**/
        /*********************************************/
        //Here we construct the Triad following the SI of the paper "DNA elasticity from CG silations: the effect of groove assymetry"
        for(int i=0; i<nbp; i++ )
        {
            int j = nbp-1-i;
            //position of nucleotides in first strand
            x1  =  comS1[t][0][i]; y1  =  comS1[t][1][i]; z1  =  comS1[t][2][i];
 
            //position of complementary nucleotide in second strand (thats why the index is j)
            x2  =  comS2[t][0][j]; y2  =  comS2[t][1][j]; z2  =  comS2[t][2][j];

            q01 = quatS1[t][0][i]; q11 = quatS1[t][1][i]; q21 = quatS1[t][2][i]; q31 = quatS1[t][3][i];
            q02 = quatS2[t][0][j]; q12 = quatS2[t][1][j]; q22 = quatS2[t][2][j]; q32 = quatS2[t][3][j];

            vector<double> qs1 {q01, q11, q21, q31};
            vector<double> qs2 {q02, q12, q22, q32};

            vector<double> ex1(3), ey1(3), ez1(3);
            vector<double> ex2(3), ey2(3), ez2(3);

            ex1 = q_to_ex(qs1); ey1 = q_to_ey(qs1); ez1 = q_to_ez(qs1);
            ex2 = q_to_ex(qs2); ey2 = q_to_ey(qs2); ez2 = q_to_ez(qs2);


            //The centre line will be usefulf for computing writhe
            centerline[t][0][i] = (x1 + x2)/2.0;
            centerline[t][1][i] = (y1 + y2)/2.0;
            centerline[t][2][i] = (z1 + z2)/2.0;


            //E3 is the average of the unitary e3 vectors in a base-pair. Just remember that the directionality
            //of the chain imposes this two vectors point on apposite directions. Therefore, the average has a -ve sign:
            double dez1, dez2, de3;
            vector<double>  e3(3);

            dez1   = length(ez1);
            ez1[0] = ez1[0]/dez1; 
            ez1[1] = ez1[1]/dez1; 
            ez1[2] = ez1[2]/dez1;

            dez2   = length(ez2);
            ez2[0] = ez2[0]/dez2; 
            ez2[1] = ez2[1]/dez2; 
            ez2[2] = ez2[2]/dez2;

            //here is the -ve sign for the average
            e3[0] = ez1[0] - ez2[0]; 
            e3[1] = ez1[1] - ez2[1]; 
            e3[2] = ez1[2] - ez2[2];
            de3   = length(e3);

            if(de3>SMALL){
                e3[0] = e3[0]/de3; 
                e3[1] = e3[1]/de3; 
                e3[2] = e3[2]/de3;
            }

            if(de3<=SMALL){
                e3[0] = 0.0;
                e3[1] = 0.0;
                e3[2] = 0.0;
            }

            E3[t][0][i] = e3[0];
            E3[t][1][i] = e3[1]; 
            E3[t][2][i] = e3[2]; 
          
            //E2 is related to the vector v2 connecting the two centres of mass of nucleotides in a base-pair.
            vector<double>  v2(3);
            vector<double>  e2(3);
            double dv2, de2;

            v2[0] = x2 - x1; 
            v2[1] = y2 - y1;
            v2[2] = z2 - z1;
            dv2   = length(v2);

            if(dv2>SMALL){
                v2[0] = v2[0]/dv2;
                v2[1] = v2[1]/dv2;
                v2[2] = v2[2]/dv2;
            }

            if(dv2<=SMALL){
                v2[0] = 0.0;
                v2[1] = 0.0;
                v2[2] = 0.0;
            }

            //In general v2 is not perpendicular to E3. Therefore we need to take the projection onto the orthogonal space of e3;
            double dotv2e3 = dotProduct(v2, e3);
            e2[0] = v2[0] - dotv2e3*e3[0];
            e2[1] = v2[1] - dotv2e3*e3[1];
            e2[2] = v2[2] - dotv2e3*e3[2];
            de2   = length(e2);

            if(de2>SMALL){
                e2[0] = e2[0]/de2;
                e2[1] = e2[1]/de2;
                e2[2] = e2[2]/de2;
            }

            if(de2<=SMALL){
                e2[0] = 0.0;
                e2[1] = 0.0;
                e2[2] = 0.0;
            }

            E2[t][0][i] = e2[0];
            E2[t][1][i] = e2[1];
            E2[t][2][i] = e2[2]; 

            //E1 is simply the cross product of the two previous vectors.
            vector<double>  e1(3);
            double de1;

            e1  = crossProduct(e2, e3);
            de1 = length(e1);

            E1[t][0][i] = e1[0];
            E1[t][1][i] = e1[1];
            E1[t][2][i] = e1[2]; 
        }
        
        
        /***********************************/
        /**Compute The local angle (theta)**/
        /***********************************/
        vector<vector <double> > Ti(3, vector<double>(3));
        vector<vector <double> > Tip1(3, vector<double>(3));
        vector<vector <double> > Titranspose(3, vector<double>(3));
        vector<vector <double> > Ri(3, vector<double>(3)); 

        vector<double>  e1i(3); vector<double>  e2i(3); vector<double>  e3i(3);
        vector<double>  e1k(3); vector<double>  e2k(3); vector<double>  e3k(3);
        double trace;
        double totales=0.0;
        

        for(int i=0; i<nbp-1; i++ )
        {
            //construct the orthogonal matrix from the local triad. There are a total of nbp of these matrices.
            e1i[0] = E1[t][0][i]; e1i[1] = E1[t][1][i]; e1i[2] = E1[t][2][i];
            e2i[0] = E2[t][0][i]; e2i[1] = E2[t][1][i]; e2i[2] = E2[t][2][i];
            e3i[0] = E3[t][0][i]; e3i[1] = E3[t][1][i]; e3i[2] = E3[t][2][i];

            e1k[0] = E1[t][0][i+1]; e1k[1] = E1[t][1][i+1]; e1k[2] = E1[t][2][i+1];
            e2k[0] = E2[t][0][i+1]; e2k[1] = E2[t][1][i+1]; e2k[2] = E2[t][2][i+1];
            e3k[0] = E3[t][0][i+1]; e3k[1] = E3[t][1][i+1]; e3k[2] = E3[t][2][i+1];

            Ti          = matrixOrthogonal(e1i, e2i, e3i);
            Titranspose = matrixTranspose(Ti);
            Tip1        = matrixOrthogonal(e1k, e2k, e3k);     

            Ri = matrixProduct(Titranspose, Tip1);
            trace = matrixTrace(Ri);
            theta = acos((trace-1.0)/2.0);

            theta1 = (Ri[2][1]-Ri[1][2])*theta/(2*sin(theta))*180/pi;
            theta2 = (Ri[0][2]-Ri[2][0])*theta/(2*sin(theta))*180/pi;
            theta3 = (Ri[1][0]-Ri[0][1])*theta/(2*sin(theta))*180/pi;
            //cout << "i: " << i+1 << " theta3 = " << theta3 << endl; 
                    
            localtheta1[t][i] = theta1;
            localtheta2[t][i] = theta2;
            localtheta3[t][i] = theta3;
            
            totales = totales+=localtheta3[t][i];
        }
        totalTw[t] = totales/360.0;
        //cout << totalTw[t] << endl;
    }

   
   
   
    /******************************************************************/
    /*The average over position and time of the local angles in rad/nm*/
    /******************************************************************/
    for (int l=0; l<frames; l++)
    {
        //Average over bp at a fixed timesteps
        double prom1=0.0;
        double prom2=0.0;
        double prom3=0.0;
            
        int newindex=0;
        for(int i=0+navoid; i<nbp-1-navoid; i++ )
        {
            theta1=localtheta1[l][i];
            theta2=localtheta2[l][i];
            theta3=localtheta3[l][i];
            
            vec_theta1[l][newindex] = theta1;
            vec_theta2[l][newindex] = theta2;
            vec_theta3[l][newindex] = theta3;
            
            prom1+=theta1;
            prom2+=theta2;
            prom3+=theta3;
                        
            newindex+=1;
        }
        
        ave1[l]=prom1/(double)newindex;
        ave2[l]=prom2/(double)newindex;
        ave3[l]=prom3/(double)newindex;
    }
    
    
           
        
    double avetheta1=0.0;
    double avetheta2=0.0;
    double avetheta3=0.0;
    for (int t=0; t<frames; t++)
    {
        avetheta1 = avetheta1 + ave1[t];
        avetheta2 = avetheta2 + ave2[t];
        avetheta3 = avetheta3 + ave3[t];
    }
        
    avetheta1 = avetheta1/(double)frames;
    avetheta2 = avetheta2/(double)frames;
    avetheta3 = avetheta3/(double)frames;
    

    //Check
    cout << "<theta1> = " << fconv*avetheta1 << " rad/nm" << endl;
    cout << "<theta2> = " << fconv*avetheta2 << " rad/nm" << endl;
    cout << "<theta3> = " << fconv*avetheta3 << " rad/nm" << endl;
    
    
    for (int l=0; l<frames; l++)
    {
        for (int i=0; i<Nn; i++)
        {
                omega[l][0][i] = fconv*(vec_theta1[l][i]-avetheta1);
                omega[l][1][i] = fconv*(vec_theta2[l][i]-avetheta2);
                omega[l][2][i] = fconv*(vec_theta3[l][i]-avetheta3);
        }
    }
    
      
    /***************************************************************************************************************************/
    /**Compute the stiffness matrix according to the paper: DNA elasticity from CG simulations: The effect of groove asymmetry**/
    /***************************************************************************************************************************/ 
    //At a fixed timestep we compute the 3x3 M-matrix. There is one of these matrices per m-bp-step. 
    //The matrix has six independent components xi00,xi01,xi02, xi11, xi12, xi22   
    //vector<vector<vector<double>>> xiTimeMatrix(nfiles, vector<vector<double> >(6, vector<double>(N)));    

    vector<vector<vector<vector<double>>>> ximatrix(frames, vector<vector<vector<double>>>(Nn,vector<vector<double> >(3, vector<double>(3))));   
    vector<double> sum(3);

    for (int l=0; l<frames; l++){
        for(int m=0; m<Nn; m++){
            //For each distance m there are n=Nn-m components. For each component a 3x3 matrix can be created with the 9 elements of the corralation matrix
            vector<vector<vector<double>>> correlation(Nn-m, vector<vector<double> >(3, vector<double>(3)));
            for(int n=0; n<Nn-m; n++){
                sum[0]=0.0;
                sum[1]=0.0;
                sum[2]=0.0;
                
                for(int k=n; k<n+m+1; k++){
                    sum[0] = sum[0] + omega[l][0][k];
                    sum[1] = sum[1] + omega[l][1][k];
                    sum[2] = sum[2] + omega[l][2][k];
                }
                
                for(int mu=0; mu<3; mu++){
                    for(int nu=0; nu<3; nu++){
                        correlation[n][mu][nu]=sum[mu]*sum[nu];
                    }
                }     
                
            }
            
            //Each of the 9 elements of the 3x3 matrix can be average over the N-m data. Therefore there will be an average 3x3 matrix (xi) per value of m.       
            for(int mu=0; mu<3; mu++){
                for(int nu=0; nu<3; nu++){
                    double avemunu=0.0;
                    
                    for(int n=0; n<Nn-m; n++){
                        avemunu = avemunu + correlation[n][mu][nu];
                    }
                    ximatrix[l][m][mu][nu] = avemunu/(double)(Nn-m);
                }
            }
        }
    }
    
    
    /*******************************************************************************************/
    /*If we compute the average over time, at each m we obtain a 3x3 matrix: The lambda matrix.*/ 
    /*Then, we need to compute the inverse of that matrix to get the stiffness matrix **********/
    vector<vector<vector<double>>> stiff(Nn, vector<vector<double> >(3, vector<double>(3))); 
    vector<vector<vector<double>>> lambdaM(Nn, vector<vector<double> >(3, vector<double>(3))); 

    for(int m=0; m<Nn; m++){
        vector<vector <double> > lambdamatrix(3, vector<double>(3));
        
        for(int mu=0; mu<3; mu++){
            for(int nu=0; nu<3; nu++){
                double avelambda=0.0;
                    
                for (int l=0; l<frames; l++){
                    avelambda = avelambda + ximatrix[l][m][mu][nu];
                }
                    
                lambdamatrix[mu][nu] = avelambda/(double)frames;
                lambdaM[m][mu][nu]   = avelambda/(double)frames;
            }
        }
        
        //We need to invert the lambda matrix to obtain the stiffness matrix M       
        vector<vector <double> > M1(3, vector<double>(3));
        vector<vector <double> > M2(3, vector<double>(3));
        
        M1=matrixInverse(lambdamatrix);
        M2=matrixTimesConstant(M1,(double)(1.0+m)/a);
        
        for(int mu=0; mu<3; mu++)
        {
            for(int nu=0; nu<3; nu++)
            {
                stiff[m][mu][nu] = M2[mu][nu];
            }
        }
    }
    
    
    
    
    /*Stiffness Matrix*/
    char name5[30] ="stiffness.dat";
    char writeFile2 [250] = {'\0'};
    
    sprintf(writeFile2, "%s%s",argv[2], name5);
    ofstream write2(writeFile2);
    cout << "writing on .... " << writeFile2 <<endl;

    //set precision and the number of decimals to be printed always
    write2.precision(16);
    write2.setf(ios::fixed);
    write2.setf(ios::showpoint);

    for (int m=0; m<Nn; m++) 
    {
        write2 << m+1 << " " << stiff[m][0][0] << " " << stiff[m][0][1] << " " << stiff[m][0][2] << " " << stiff[m][1][1] << " " << stiff[m][1][2] << " " << stiff[m][2][2] << endl;
    }
    write2.close();
    write2.clear();


        
  
 
  
    

  
    
    










}








return 0;
}



//Quaternion product
vector<double> quatproduct(vector<double> P, vector<double> Q)
{
    vector<double> R(4);
    R[0] = P[0]*Q[0] - P[1]*Q[1] - P[2]*Q[2] - P[3]*Q[3];
    R[1] = P[0]*Q[1] + Q[0]*P[1] + P[2]*Q[3] - P[3]*Q[2];
    R[2] = P[0]*Q[2] + Q[0]*P[2] - P[1]*Q[3] + P[3]*Q[1];
    R[3] = P[0]*Q[3] + Q[0]*P[3] + P[1]*Q[2] - P[2]*Q[1];

    return R;
}

// converts quaternion DOF into local body reference frame
vector<double> q_to_ex(vector<double> Q)
{
    vector<double> ex(3);

    ex[0]=Q[0]*Q[0]+Q[1]*Q[1]-Q[2]*Q[2]-Q[3]*Q[3];
    ex[1]=2*(Q[1]*Q[2]+Q[0]*Q[3]);
    ex[2]=2*(Q[1]*Q[3]-Q[0]*Q[2]);
    return ex;
}


vector<double> q_to_ey(vector<double> Q)
{
    vector<double> ey(3);

    ey[0]=2*(Q[1]*Q[2]-Q[0]*Q[3]);
    ey[1]=Q[0]*Q[0]-Q[1]*Q[1]+Q[2]*Q[2]-Q[3]*Q[3];
    ey[2]=2*(Q[2]*Q[3]+Q[0]*Q[1]);
    return ey;
}


vector<double> q_to_ez(vector<double> Q)
{
    vector<double> ez(3);

    ez[0]=2*(Q[1]*Q[3]+Q[0]*Q[2]);
    ez[1]=2*(Q[2]*Q[3]-Q[0]*Q[1]);
    ez[2]=Q[0]*Q[0]-Q[1]*Q[1]-Q[2]*Q[2]+Q[3]*Q[3];
    return ez;
}


//Sign function
double sign(double a) { if (a>0) {return 1.0;} else {return -1.0;} } 


//length
double length(vector<double> a)
{
  double r;
  r = sqrt(a[0]*a[0] + a[1]*a[1] + a[2]*a[2]);
  return r;
}

//Dot product
double dotProduct (vector<double> a, vector<double> b)
{
  double r;
  r = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
  return r;
}

//Cross product
vector<double> crossProduct(vector<double> a, vector<double> b)
{
  vector<double> r(3);
  r[0] = a[1]*b[2]-a[2]*b[1];
  r[1] = a[2]*b[0]-a[0]*b[2];
  r[2] = a[0]*b[1]-a[1]*b[0];
  
  return r;
}


// checks for round off in a number in the interval [-1,1] before using asin or acos
void fix_roundoff(double &a)
{
  if (abs(a)>1.001) 
  {
    std::cout<<"Error - number should be in interval [-1,1] but is "<< a << endl;
    exit(0);
  }
  if (a>1){a=1.0;}
  if (a<-1){a=-1.0;}
}


//The orthogonal matrix. This is the rotation matrix transforming the canonical frame into the frame of the respective triad
vector<vector <double> > matrixOrthogonal(vector<double> a, vector<double> b, vector<double> c)
{
  vector<vector <double> > T(3, vector<double>(3));

  T[0][0] = a[0];    T[0][1] = b[0];    T[0][2] = c[0];
  T[1][0] = a[1];    T[1][1] = b[1];    T[1][2] = c[1];
  T[2][0] = a[2];    T[2][1] = b[2];    T[2][2] = c[2];

  return T;
}


vector<vector <double> > matrixTranspose(vector<vector <double> > a)
{
  vector<vector <double> > M(3, vector<double>(3));

  for(int i=0; i<3; i++)
  {
      for(int j=0; j<3; j++)
      {
          M[i][j] = a[j][i];
      }
  }

  return M;
}


vector<vector <double> > matrixProduct(vector<vector <double> > a, vector<vector <double> > b)
{
  vector<vector <double> > M(3, vector<double>(3));

  for(int i=0; i<3; i++)
  {
      for(int j=0; j<3; j++)
      {
          double s=0;
          
          for(int k=0; k<3; k++)
          {
              s = s + a[i][k]*b[k][j];

              M[i][j] = s;
          }
      }
  }

  return M;
}
     

double matrixTrace(vector<vector <double> > a)
{
  double r;
  r = a[0][0] + a[1][1] + a[2][2];
  return r;
}



double matrixDet(vector<vector <double> > a)
{
  double r = a[0][0]*(a[1][1]*a[2][2]-a[2][1]*a[1][2]) - a[0][1]*(a[1][0]*a[2][2]-a[2][0]*a[1][2]) + a[0][2]*(a[1][0]*a[2][1]-a[2][0]*a[1][1]);
  
  return r;
}



vector<vector <double> > matrixTimesConstant(vector<vector <double> > a, double b)
{
  vector<vector <double> > M(3, vector<double>(3));

  for(int i=0; i<3; i++)
  {
      for(int j=0; j<3; j++)
      {
          M[i][j] = b*a[i][j]; 
      }
  }
  
  return M;
}


vector<vector <double> > matrixInverse(vector<vector <double> > a)
{
  vector<vector <double> > M(3, vector<double>(3));

  double det = matrixDet(a);
  double invdet = 1.0/det;

  M[0][0] = (a[1][1] * a[2][2] - a[2][1] * a[1][2]) * invdet;
  M[0][1] = (a[0][2] * a[2][1] - a[0][1] * a[2][2]) * invdet;
  M[0][2] = (a[0][1] * a[1][2] - a[0][2] * a[1][1]) * invdet;
  M[1][0] = (a[1][2] * a[2][0] - a[1][0] * a[2][2]) * invdet;
  M[1][1] = (a[0][0] * a[2][2] - a[0][2] * a[2][0]) * invdet;
  M[1][2] = (a[1][0] * a[0][2] - a[0][0] * a[1][2]) * invdet;
  M[2][0] = (a[1][0] * a[2][1] - a[2][0] * a[1][1]) * invdet;
  M[2][1] = (a[2][0] * a[0][1] - a[0][0] * a[2][1]) * invdet;
  M[2][2] = (a[0][0] * a[1][1] - a[1][0] * a[0][1]) * invdet;
  
  return M;
}



void matrixPrint(vector<vector <double> > a)
{
  for(int i=0; i<3; i++)
  {
      for(int j=0; j<3; j++)
      {
          cout << a[i][j] << " ";
      }
      cout << endl;
  }
  cout << endl;
  cout << endl;
}





