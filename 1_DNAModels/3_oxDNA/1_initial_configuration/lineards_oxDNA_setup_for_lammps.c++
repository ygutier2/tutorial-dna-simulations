#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <vector>      
#include <unistd.h>
#include <stdio.h>     
#include <time.h>
#include <iomanip>
#include <ctime>
#include <iostream>     // std::cin, std::cout
#include <fstream>      // std::ifstream

using namespace std;

#define pi 3.14159265358979323846


//Function declaration
vector<double> quatproduct(vector<double> P, vector<double> Q);           
vector<double> q_to_ex(vector<double> Q);
vector<double> q_to_ey(vector<double> Q);
vector<double> q_to_ez(vector<double> Q);

int main()
{

    //The text file containing the nucleotide sequence (in capital letters) of the first strand 
    char fseq[50]="sequence.txt";

    //vector with the particle-type for both strands. 
    //The number of entrance will be created dynamically
    vector<int> typeS1;
    vector<int> typeS2;

    /*DNA rise in simulation units (1sigma = 0.8518 nm)*/
    double rise = 0.4;

    //The number of base-pairs is set after reading the file with the sequence
    int nbp;



/**********************/
/*Reading the sequence*/
/**********************/
    //For the first strand the sequence is given with the directionality from 5' to 3'
    ifstream fread(fseq);

    if(!fread)
    {
        cout << "Error opening input file" << endl;
    }

    if(fread.is_open())
    {
        char s;
        int  type;
        // loop getting single characters
        while (fread.get(s))
        {
              if(s=='A'){typeS1.push_back(1);}
              if(s=='C'){typeS1.push_back(2);}
              if(s=='G'){typeS1.push_back(3);}
              if(s=='T'){typeS1.push_back(4);}
        }
        fread.close();
    }

    //The number of base-pairs in the system
    nbp = typeS1.size();

    //For the complementary strand the vector is also filled following the directionality  5' to 3'
    for(int i=0; i<nbp; i++)
    {
        //The vector typeS2 is filled in with the complementary nucleotide type 
        //found in vector typeS1 (starting from the last entry to the first entry). For example,
        //the first entry of vector typeS2 is complementary to the last entry of vector typeS1.
        int j = (nbp-1)-i;
        int type = typeS1[j];

        if(type==1){typeS2.push_back(4);}
        if(type==2){typeS2.push_back(3);}
        if(type==3){typeS2.push_back(2);}
        if(type==4){typeS2.push_back(1);}
    }

    //check
    /*
    for(int i=0; i<nbp; i++)
    {
        int j = (nbp-1)-i;
        cout << typeS1[i] << " " << typeS2[j] << endl;
    }
    */



/**********************/
/*Centre of mass (COM)*/
/**********************/

    //The length of half of the box
    double L=rise*nbp/2.0;

    cout << "\nThe number of base-pairs in the sequence is " << nbp << endl;

 
    //Set the uniform local-twis (in radians) of DNA: 0.6 is the relaxed state 
    double phi = 0.6;

    //The DNA pitch (number of bp per helical turn) can be inferred from the local twist.
    //In the relaxed state this is ~ 10.471975512;
    double pitch=(2*pi)/phi;
    
    //vector with the particle-position of the COM for both strands. 
    vector<vector <double> > comS1(3, vector<double>(nbp));  
    vector<vector <double> > comS2(3, vector<double>(nbp));  

    //distance in simulation units between DNA axis and the COM.
    //This is the radius of the helix.
    double r =  0.5*(1.0/0.8518);
    
    double start2 = (nbp-1)*phi;


    for(int i=0; i<nbp; i++)
    {
        int j = (nbp-1)-i;
        
        /*First strand COM position*/
        comS1[0][i] = -r*cos((double)i*phi);
        comS1[1][i] = -r*sin((double)i*phi);
        comS1[2][i] = (double)i*rise;
        
        comS2[0][i] = -r*cos((start2 -(double)i*phi) + pi);
        comS2[1][i] = -r*sin((start2 -(double)i*phi) + pi);
        comS2[2][i] = (double)j*rise;

    }


    //check
    /*
    for(int i=0; i<nbp; i++)
    {
        cout << "S1: " << comS1[0][i] << " " << comS1[1][i] << " " << comS1[2][i] << endl;
    }

    for(int i=0; i<nbp; i++)
    {
        cout << "S2: " << comS2[0][i] << " " << comS2[1][i] << " " << comS2[2][i] << endl;
    }
    */

    

/****************/
/*The quaternion*/
/****************/
/*A rotation about an unitary vector (u=(u1, u2, u3)=u1 i + u2 j + u3 k) by an angle (theta), can be represented by the quaternion
q = (q0,q1,q2,q3). Where q0=cos(theta/2), (q1,q2,q3) = (u1,u2,u3)*sin(theta/2).

The Euclidian reference frame is denoted by i, j and k.
The body reference frame is denoted by ex, ey and ez.
In the simulation, the default body reference frame is aligned with the Euclidian reference frame. In terms of quaternions this means no rotation (theta=0)
about the vector u=(0,0,0). This is q=(1,0,0,0).

For each nucleotide its body reference frame points in a different direction. Therefore, the quaternion can be obtained if we answer the folloing question:
Starting from the default orientation of the body reference frame (i,j,k), which is the set of rotations necessary to get the desire orientation of the body frame for 
each nucleotide*/

    //vector with the particle-quaternion for both strands. 
    vector<vector <double> > quatS1(4, vector<double>(nbp));  
    vector<vector <double> > quatS2(4, vector<double>(nbp));  

 
    //quaternion for the first strand. This case is simple because we want that ex always points to the centre of the helix and that ez is aligned to k.
    //This is a rotation about u=(0,0,1) and angle theta=phi*i for the ith-nucleotide.
    for(int i=0; i<nbp; i++)
    {
        quatS1[0][i] = cos((double)i*phi/2.0);
        quatS1[1][i] = 0.0;
        quatS1[2][i] = 0.0;
        quatS1[3][i] = sin((double)i*phi/2.0);
    }


    //The quaternion of any nucleotide in the second strand can be computed from the quaternion of the complementary nucleotide in the first strand.
    //You just have to take that quaternion and first make a rotation of 180° about (0,0,1) followed by a rotation of -180° about (1,0,0).
    for(int i=0; i<nbp; i++)
    {
        //quaternion in the first strand
        double q0 = quatS1[0][i];
        double q1 = quatS1[1][i];        
        double q2 = quatS1[2][i];
        double q3 = quatS1[3][i];

        vector<double> qs1{q0, q1, q2, q3};

        //quaternion associated to the first rotation (cos(pi/2), 0, 0, sin(pi/2))
        vector<double> a{0, 0, 0, 1};

        //quaternion associated to the second rotation (cos(-pi/2), sin(-pi/2), 0, 0)
        vector<double> b{0, -1, 0, 0};

        //Apply the first rotation to qs1. Here the order is very important
        vector<double> r1(4);
        r1 = quatproduct(qs1, a);

        //Apply the second rotation. Here the order is very important
        vector<double> r2(4);
        r2 = quatproduct(r1, b);

        int j = (nbp-1)-i;
        quatS2[0][j] = r2[0];
        quatS2[1][j] = r2[1];
        quatS2[2][j] = r2[2];
        quatS2[3][j] = r2[3];
    }

    //check
    /*
    for(int i=0; i<nbp; i++)
    {
        int j = (nbp-1)-i;
        cout << "S1: " << quatS1[0][i] << " " << quatS1[1][i] << " " << quatS1[2][i] << " " << quatS1[3][i] << endl;
        cout << "S2: " << quatS2[0][j] << " " << quatS2[1][j] << " " << quatS2[2][j] << " " << quatS2[3][j] << endl;

        cout << "\n \n";
    }
    */



/***************/
/*Bonds section*/
/***************/

//Number of FENE bonds per strand. (Type 1)
int nfene=nbp-1;
vector< vector <int> > feneS1(2, vector<int>(nfene));
vector< vector <int> > feneS2(2, vector<int>(nfene));


for(int i=0; i<nbp-1; i++)
{
    //FENE bonds
    feneS1[0][i] = i+1;
    feneS1[1][i] = i+2;
    //cout << feneS1[0][i] << " " << feneS1[1][i] << endl;

    feneS2[0][i] = i+1+nbp;
    feneS2[1][i] = i+2+nbp;
    //cout << feneS2[0][i] << " " << feneS2[1][i] << endl;
} 





/*********************************/
/*Write the initial configuration*/
/*********************************/
string name1("initial_configuration_N");
string name2("_phi");

stringstream writeFile;
writeFile << name1 << nbp << name2 << phi;

ofstream write(writeFile.str().c_str());
cout << "writing on .... " << writeFile.str().c_str() <<endl;

//set precision and the number of decimals to be printed always
write.precision(16);
write.setf(ios::fixed);
write.setf(ios::showpoint);


  write << "LAMMPS data file initial configuration linear dsDNA molecule for oxDNA; timestep = 0" << endl;

  write << 2*nbp  << " atoms"     << endl;
  write << 2*nbp    << " ellipsoids"     << endl;
  write << 2*nfene  << " bonds"     << endl;
  write << "\n";

  write << 4 << " atom types"     << endl;
  write << 1 << " bond types"     << endl;
  write << "\n";

  write << -L << " " << L << " xlo xhi" << endl;
  write << -L << " " << L << " ylo yhi" << endl;
  write << -3 << " " << nbp*0.4 +10 << " zlo zhi" << endl;
  write << "\n";



  write << "Masses\n" << endl;
  for(int i=0; i<4;i++)
  {
      write << i+1 << " 3.1575" << endl;
  }
  write << "\n";


  
  //Please note that the molecule-id refers to the strand (either 1 or 2) at which the nucleotide is part of.
  write << "# Atom-ID, type, position, molecule-ID, ellipsoid flag, density" << endl;
  write << "Atoms\n" << endl;
  //First strand: 
  for(int i=0; i<nbp; i++ )
  {
      if(i<nbp-1)
      {
          write << i+1     << " " << typeS1[i]  << " " << comS1[0][i]  << " " << comS1[1][i] << " " << comS1[2][i] << " 1 1 1" << endl;
      }
      
      if(i==nbp-1)
      {
          write << i+1     << " " << typeS1[i]  << " " << comS1[0][i]  << " " << comS1[1][i] << " " << comS1[2][i] << " 1 1 1" << endl;
      }
  } 

  //Second strand:
  for(int i=0; i<nbp; i++ )
  {
      if(i==0)
      {
          write << i+1+nbp << " " << typeS2[i]  << " " << comS2[0][i]  << " " << comS2[1][i] << " " << comS2[2][i] << " 2 1 1" << endl;
      }
      
      if(i>0)
      {
          write << i+1+nbp << " " << typeS2[i]  << " " << comS2[0][i]  << " " << comS2[1][i] << " " << comS2[2][i] << " 2 1 1" << endl;
      }
  } 
  write << "\n";




  write << "# Atom-ID, translational, rotational velocity" << endl;
  write << "Velocities\n" << endl;
  for(int i=0; i<2*nbp; i++ )
  {
      write << i+1 << " 0 0 0 0 0 0" << endl;
  } 
  write << "\n";





  write << "# Atom-ID, shape (sx, sy, sz), quaternion (q0,q1,q2,q3)" << endl;
  write << "Ellipsoids\n" << endl;
  //First strand: 
  for(int i=0; i<nbp; i++ )
  {
      write << i+1     << " 1.1739845031423408 1.1739845031423408 1.1739845031423408 " << quatS1[0][i] << " " << quatS1[1][i] << " " << quatS1[2][i] << " " << quatS1[3][i] << endl;
  } 

  //Second strand:
  for(int i=0; i<nbp; i++ )
  {
      write << i+1+nbp << " 1.1739845031423408 1.1739845031423408 1.1739845031423408 " << quatS2[0][i] << " " << quatS2[1][i] << " " << quatS2[2][i] << " " << quatS2[3][i] << endl;
  }
  write << "\n";



  write << "# bondid, bondtype, b1, b2" << endl;
  write << "Bonds\n" << endl;

  int bondid =0;
  // FENE bonds S1 (type 1)
  for(int i=0; i<nfene; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 1 << " " << feneS1[0][i] << " " << feneS1[1][i] << endl;
  }
  // FENE bonds S2 (type 1)
  for(int i=0; i<nfene; i++ )
  {
      bondid = bondid+1;
      write << bondid << " " << 1 << " " << feneS2[0][i] << " " << feneS2[1][i] << endl;
  }  

return 0; 
}





//Quaternion product
vector<double> quatproduct(vector<double> P, vector<double> Q)
{
    vector<double> R(4);
    R[0] = P[0]*Q[0] - P[1]*Q[1] - P[2]*Q[2] - P[3]*Q[3];
    R[1] = P[0]*Q[1] + Q[0]*P[1] + P[2]*Q[3] - P[3]*Q[2];
    R[2] = P[0]*Q[2] + Q[0]*P[2] - P[1]*Q[3] + P[3]*Q[1];
    R[3] = P[0]*Q[3] + Q[0]*P[3] + P[1]*Q[2] - P[2]*Q[1];

    return R;
}

// converts quaternion DOF into local body reference frame
vector<double> q_to_ex(vector<double> Q)
{
    vector<double> ex(3);

    ex[0]=Q[0]*Q[0]+Q[1]*Q[1]-Q[2]*Q[2]-Q[3]*Q[3];
    ex[1]=2*(Q[1]*Q[2]+Q[0]*Q[3]);
    ex[2]=2*(Q[1]*Q[3]-Q[0]*Q[2]);
    return ex;
}


vector<double> q_to_ey(vector<double> Q)
{
    vector<double> ey(3);

    ey[0]=2*(Q[1]*Q[2]-Q[0]*Q[3]);
    ey[1]=Q[0]*Q[0]-Q[1]*Q[1]+Q[2]*Q[2]-Q[3]*Q[3];
    ey[2]=2*(Q[2]*Q[3]+Q[0]*Q[1]);
    return ey;
}


vector<double> q_to_ez(vector<double> Q)
{
    vector<double> ez(3);

    ez[0]=2*(Q[1]*Q[3]+Q[0]*Q[2]);
    ez[1]=2*(Q[2]*Q[3]-Q[0]*Q[1]);
    ez[2]=Q[0]*Q[0]-Q[1]*Q[1]-Q[2]*Q[2]+Q[3]*Q[3];
    return ez;
}
