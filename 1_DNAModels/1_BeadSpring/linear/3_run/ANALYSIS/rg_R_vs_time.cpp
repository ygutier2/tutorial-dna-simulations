#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision
#include <tuple>     //Functions can return more than one value
#include <set>
#include <algorithm>

using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375
#define SMALL 1e-20

//The timestep set in lammps
double dt=0.01;

int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<7)
    {
        cout << "Six arguments are required by the programm:"               << endl;
        cout << "1.- Path to the input data"                                << endl;
        cout << "2.- Output path"                                           << endl;
        cout << "3.- Name of the input file"                                << endl;
        cout << "4.- Timestep"                                              << endl;
        cout << "5.- Number of molecules"                                   << endl;
        cout << "6.- Nbeads per molecule"                                   << endl;
    }

if(argc==7)
{   
    //cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    /*argv[3] Input file*/
    
    /*timestep*/
    int timestep;
    timestep = atoi(argv[4]);

    /*Number of polymers in the simulation*/
    int M;
    M = atoi(argv[5]);
    
    /*Number of beads per polymer*/
    int Nbeads;
    Nbeads = atoi(argv[6]);
      
    vector< vector< vector<double> > > position(M, vector<vector<double> >(Nbeads, vector<double>(3)));
    vector<double> rg2vec(M);
    vector<double> e2evec(M);
    
    
    /*the following variables will be useful to read the data file*/
    int tbrownian;
    int time;
    int Ntot;        //The total number of beads in the system. It should be equal to M*Nbeads.
    int id,type,mol;
    int ix,iy,iz;
    double   x,y,z;
    double lmin,lmax,Lx,Ly,Lz;    
    int nm;
    
    /*Read the file*/
    ifstream indata1;
    char readFile1[400] = {'\0'};
    string dummy;
    string line;
    sprintf(readFile1, "%s%s%d",argv[1],argv[3],timestep);
    indata1.open(readFile1);
    
    if (indata1.is_open())
    {       
        //Read headers
        for(int i=0;i<10;i++){
            if(i==1) {
                indata1 >> time;                   
                tbrownian = time*dt;
            }

            if(i==3) {
                indata1 >> Ntot;
            }

            if(i==5) {
                indata1 >> lmin >> lmax;
                Lx = lmax-lmin;
            }

            if(i==6) {
                indata1 >> lmin >> lmax;
                Ly = lmax-lmin;
            }

            if(i==7) {
                indata1 >> lmin >> lmax;
                Lz = lmax-lmin;
            }

            else getline(indata1,dummy);
        }
                
        //READ ATOMS
        for(int n=0; n<Ntot; n++){
            indata1 >> id >> type >> x >> y >> z >> ix >> iy >> iz;
            
            int m = floor((double)(id-1)/(1.0*Nbeads));
            
            position[m][(id-1)%Nbeads][0] = (x + Lx*ix);
            position[m][(id-1)%Nbeads][1] = (y + Ly*iy);
            position[m][(id-1)%Nbeads][2] = (z + Lz*iz);
        }
        
        indata1.close();        
        indata1.clear();  
    }
   
    else {cout << "Error opening file " << timestep << endl;}
   
   
   
    /////////////////////////////////////////////////////////////////////////////////
    // Compute Radius of Gyration and end2end per molecule and average over molecules
    /////////////////////////////////////////////////////////////////////////////////
    double averg2=0;
    double avee2e=0;
    for(int m=0;m<M;m++){
        double dx = position[m][Nbeads-1][0]-position[m][0][0];
        double dy = position[m][Nbeads-1][1]-position[m][0][1];
        double dz = position[m][Nbeads-1][2]-position[m][0][2];
        
        e2evec[m]=sqrt(dx*dx + dy*dy + dz*dz);
        avee2e+=e2evec[m];
        
        double rg2=0;
        double com[3]={0.0,0.0,0.0};

        //COM of each polymer
        for(int n=0;n<Nbeads;n++){
            com[0]+=position[m][n][0];
            com[1]+=position[m][n][1];
            com[2]+=position[m][n][2];
        }

        com[0]/=(double)Nbeads;
        com[1]/=(double)Nbeads;
        com[2]/=(double)Nbeads;
        
        //Radius of gyration
        for(int n=0;n<Nbeads;n++){
            for(int d=0;d<3;d++){
                rg2+=pow(position[m][n][d]-com[d],2.0);
            }
        }
            
        rg2/=(double)Nbeads;
        rg2vec[m]=rg2;
       
        averg2+=rg2;
    }

    averg2/=(double)M;
    avee2e/=(double)M;
    
    
    /////////////////////////////
    // Compute STDEV
    ////////////////////////////
    double stdev1=0.0;
    double sum1=0.0;

    double stdev2=0.0;
    double sum2=0.0;
    if(M>1){
        for(int m=0;m<M;m++){
            sum1 += (averg2-rg2vec[m])*(averg2-rg2vec[m]);
            sum2 += (avee2e-e2evec[m])*(avee2e-e2evec[m]);
        }    
        sum1/=(double)(M-1);
        stdev1=sqrt(sum1);
        
        sum2/=(double)(M-1);
        stdev2=sqrt(sum2);
    }
    
    if(M==1){
        stdev1=0.0;
        stdev2=0.0;
    }
    
        
    /////////////
    // PRINT RG
    /////////////
    stringstream writeFile1;
    ofstream write1;
    writeFile1 <<"Rg2_R_vs_t.dat";
    write1.open(writeFile1.str().c_str(), std::ios_base::app);
    write1 << timestep << " " << averg2 << " " << stdev1 << " " << avee2e << " " << stdev2 << endl;
}


return 0;
}
