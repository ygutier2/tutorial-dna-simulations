#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision

using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375
#define SMALL 1e-20

//The timestep set in lammps
double dt=0.01;

//Declare functions
vector< vector<double> > computeTTCpm(int N, int Nbeads, vector< vector< vector<double> > > position);
vector<double> computeaveTTC(int N, int Nbeads, vector< vector<double> > corrpm);

int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<10)
    {
 
        cout << "Nine arguments are required by the programm:"             << endl;
        cout << "1.- Directory where the data is"                          << endl;
        cout << "2.- Directory where the output will be stored"            << endl;
        cout << "3.- Name of the input file withou timestep"               << endl;
        cout << "4.- Name of the output file for TTC"                      << endl;
        cout << "5.- Starting timestep"                                    << endl; 
        cout << "6.- The dump frequency"                                   <<endl;
        cout << "7.- Last timestep of the simulation"                      << endl;        
        cout << "8.- number of polymers in the simulation"                 << endl;
        cout << "9.- number of beads per polymer"                          << endl;
    }

if(argc==10)
{   
    //cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    /*argv[3] Name of the inputfile*/
    /*argv[4] Name of the outputfile for TTC*/
    
    /*Start the calculation from this timestep*/
    int equiltime;
    equiltime = atoi(argv[5]);
    
    /*Dump frequency*/
    int dumpfreq;
    dumpfreq = atoi(argv[6]);
    
    /*Last timestep*/
    int totrun;
    totrun = atoi(argv[7]);
    
    /*The number of frames to read*/
    int frames = 1 + (totrun-equiltime)/dumpfreq;

    /*Number of ring-polymers in the simulation*/
    int N;
    N = atoi(argv[8]);

    /*Number of beads per polymer*/
    int Nbeads;
    Nbeads = atoi(argv[9]);
      
    vector< vector< vector<double> > > position(N, vector<vector<double> >(Nbeads, vector<double>(3)));
              
    /*the following variables will be useful to read the data file*/
    ifstream indata;
    char readFile[400] = {'\0'};
    string dummy;
    string line;
    
    int timestep;
    int tbrownian; 
    //The total number of beads in the system. It should be equal to N*Nbeads.
    long int Ntot;
    long int id,type,mol;
    double   num1,num2,num3;
    double   x,y,z;
    double   l1,l2;
    double   Lx,Ly,Lz;    
    
    
    /*Output file for Tangent-Tangent-Correlation*/
    stringstream writeFileTTC;
    writeFileTTC << argv[2] << argv[4];
    ofstream writeTTC(writeFileTTC.str().c_str());
    vector< vector<double> > TTCatt(frames, vector<double>(Nbeads-1));
    vector< vector<double> > stdevTTCatt(frames, vector<double>(Nbeads-1));
    
    for (int t=0; t<frames; t++)
    {
        timestep = equiltime + t*dumpfreq;
        
        sprintf(readFile, "%s%s%d",argv[1],argv[3],timestep);        
        indata.open(readFile);
        
        if (indata.is_open())
        {
            //cout << "reading from timestep = " << timestep << endl;
            
            //Read headers
            for(int i=0;i<10;i++){
                if(i==1) {
                    long long int time;
                    indata >> time;
                    tbrownian = t*dumpfreq*dt;
                }

                if(i==3) {
                    indata >> Ntot;
                }

                if(i==5) {
                    indata >> l1 >> l2;
                    Lx = l2-l1;
                }

                if(i==6) {
                    indata >> l1 >> l2;
                    Ly = l2-l1;
                }

                if(i==7) {
                    indata >> l1 >> l2;
                    Lz = l2-l1;
                }

                else getline(indata,dummy);
            }
            
            
            //READ FILES
            for(int n=0; n<Ntot; n++){
                indata >> id >> type >> x >> y >> z >> num1 >> num2 >> num3;

                int M = floor((double)(id-1)/(1.0*Nbeads));
                position[M][(id-1)%Nbeads][0] = (x + Lx*num1);
                position[M][(id-1)%Nbeads][1] = (y + Ly*num2);
                position[M][(id-1)%Nbeads][2] = (z + Lz*num3);
            }
           
            
            
            // Compute Tangent-Tangent-Correlation per molecule
            ///////////////////////////////////////////////////
            vector< vector<double> > ttcpm(N, vector<double>(Nbeads-1));
            ttcpm=computeTTCpm(N, Nbeads, position);
            
            //Now compute the average over molecules of the TTC
            vector<double> ttc(Nbeads-1);
            ttc=computeaveTTC(N, Nbeads, ttcpm);
            
            //Store it in a time dependent vector
            for(int n=0; n<Nbeads-1; n++){
                TTCatt[t][n] = ttc[n];
            }
            
        }
        indata.close();
        indata.clear();
        
        //else{cout <<"file "<< readFile << " is not there"<<endl; return 0;}
    }//This closes the frame loop
    
    
    //Do the average over time of the tangent-tangent-correlation and the errors
    for(int n=0; n<Nbeads-1; n++){
        double corr=0.0;
        for (int t=0; t<frames; t++){
            corr+=TTCatt[t][n];
        }
        corr/=(double)(frames);
        writeTTC << n << " " << corr << endl;
    }
        
}


return 0;
}



/////////////////////////////////////////////////////
// COMPUTE TANGENT-TANGENT CORRELATION PER MOLECULE//
/////////////////////////////////////////////////////
vector< vector<double> > computeTTCpm(int N, int Nbeads, vector< vector< vector<double> > > position){
    vector<vector <double> > ttcorrelation(N, vector<double>(Nbeads-1));
    //Loop over molecules
    for(int m=0;m<N;m++){
        //Computes the tangents
        vector<vector <double> > T(Nbeads-1, vector<double>(3));
        for(int n=0;n<Nbeads-1;n++){
            double Tnorm=0.0;
            for(int d=0;d<3;d++){
                T[n][d] = position[m][n+1][d] - position[m][n][d];
                Tnorm  += T[n][d]*T[n][d];
            }
            Tnorm=sqrt(Tnorm);
            
            //Unitary tangent vector
            T[n][0]/=(double)Tnorm;
            T[n][1]/=(double)Tnorm;
            T[n][2]/=(double)Tnorm;
        }
        
        //Computes the TTC for a fixed molecule
        vector <double> corr(Nbeads-1);
        double tix, tiy, tiz;
        double tjx, tjy, tjz;
        for(int i=0; i<Nbeads-1; i++)
        {
            for(int j=0; j<Nbeads-1-i; j++)
            {
                tix = T[i][0];   tiy = T[i][1];   tiz = T[i][2];
                tjx = T[i+j][0]; tjy = T[i+j][1]; tjz = T[i+j][2];

                double punto = tix*tjx + tiy*tjy + tiz*tjz;
                corr[j] += punto;
            }
        }
        
        //We have already sum over all the tangents that are located at a distance j.
        //So in order to obtain the average we just need to divide by the number of data in each case
        for(int i=0; i<Nbeads-1; i++)
        {
            ttcorrelation[m][i] = corr[i]/(double)(Nbeads-1-i);
        }
    }
    
    return ttcorrelation;
}

//////////////////////////////////////////////
// COMPUTE AVERAGE OVER MOLECULES OF THE TTC//
//////////////////////////////////////////////
vector<double> computeaveTTC(int N, int Nbeads, vector< vector<double> > corrpm){
    vector <double> avecorrelation(Nbeads-1);
    for(int i=0; i<Nbeads-1; i++)
    {
        double sum =0.0;
        for (int m=0; m<N; m++)
        {
            sum += corrpm[m][i];
        }
        avecorrelation[i] = sum/(double)N;
    }
    
    return avecorrelation;
}





