#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<fstream>
#include<sstream>
#include <unistd.h>
#include<ctime>
#include <iomanip>
#include <random>
#include <algorithm>    // std::find
using namespace std;

int main(int argc, char* argv[]){

//Number of molecules (chains)
int Nm = atoi(argv[2]);

//Number of beads per molecule
int N = atoi(argv[3]);

//Total number of beads in the system
int Ntot = Nm*N;

//Flag for: linear (0) or circular (1) polymers
int flag = atoi(argv[4]);

//Monomer density:
double rho = atof(argv[5]);

//Create polymers starting from the centre of the box (0 no), (1 yes)
int flagc = atoi(argv[6]);

//Diameter of each bead
double sigma=1.0;

//Cube
double sigma3 = sigma*sigma*sigma;

//Box size
double L = cbrt( M_PI*sigma3*Ntot/(6*rho) ); 

//File to write in
ofstream write;
stringstream filename;
filename.str("");
filename.clear();
if(flag==0){filename << argv[1] << ".linear";}
if(flag==1){filename << argv[1] << ".ring";}
write.open(filename.str().c_str());

//Bond types:
int btypes=1;

//Number of bonds per molecule:
int Nb;
if(flag==0){Nb=(N-1);}
if(flag==1){Nb=N;}

//Number of bonds in the system
int Nbonds;
if(flag==0){Nbonds=(N-1)*Nm;}
if(flag==1){Nbonds=N*Nm;}


//Angle types
int atypes=1;

//Number of angles por molecule
int Na;
if(flag==0){Na=(N-2);}
if(flag==1){Na=N;}

//Number of angles in the system
int Nangles;
if(flag==0){Nangles=Na*Nm;}
if(flag==1){Nangles=Na*Nm;}

//Position of beads in the system
vector< vector< vector<double> > > pos(Nm, vector<vector<double> >(N, vector<double>(3)));

//Random position of the first bead of each chain inside the box
std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<float> dist(-L/2.0 + 5.0, L/2.0 - 5.0);


write <<"LAMMPS data file: timestep = 0, procs = 1"<<endl;
write <<endl;

write << Ntot    << " atoms"  <<endl;
write << Nbonds  << " bonds"  <<endl;
write << Nangles << " angles" <<endl;
write << endl;

write << 1      << " atom types"  <<endl;
write << btypes << " bond types"  <<endl;
write << atypes << " angle types" <<endl;
write <<endl;

write << -L/2.0 << " " << L/2.0  << " xlo xhi "<<endl;
write << -L/2.0 << " " << L/2.0  << " ylo yhi "<<endl;
write << -L/2.0 << " " << L/2.0  << " zlo zhi "<<endl;
write <<endl;

write << "Masses "<<endl;
write <<endl;
write << " 1 1 " <<endl;
write <<endl;

write << "Atoms "<<endl;
write <<endl;

int index=0;
for(int m=0;m<Nm;m++){
    for(int n=0;n<N;n++){
        double r=1.1;

    	   if(n==0){
    	       if(flagc==0){
    	           pos[m][n][0]=dist(gen) + rand()*1.0/RAND_MAX;
	               pos[m][n][1]=dist(gen) + rand()*1.0/RAND_MAX;
    	           pos[m][n][2]=dist(gen) + rand()*1.0/RAND_MAX;
    	       }
    	       
    	       if(flagc==1){
    	           pos[m][n][0]=rand()*1.0/RAND_MAX;
	               pos[m][n][1]=rand()*1.0/RAND_MAX;
    	           pos[m][n][2]=rand()*1.0/RAND_MAX;
    	       }
        }
    
        if(n>0){
            double phi=rand()*1.0/RAND_MAX*2.0*M_PI;
            double theta=rand()*1.0/RAND_MAX*M_PI;
            pos[m][n][0]=pos[m][n-1][0]+r*sin(theta)*cos(phi);
            pos[m][n][1]=pos[m][n-1][1]+r*sin(theta)*sin(phi);
            pos[m][n][2]=pos[m][n-1][2]+r*cos(theta);
        }
    
        index+=1;
        //NEED TO WRITE:
        //INDEX MOLECULE TYPE X Y Z IX IY IZ
        write << index << " " << m+1 << " 1 " << pos[m][n][0] << " " <<  pos[m][n][1] << " "  << pos[m][n][2] << " 0 0 0 " << endl;
    }
}

write <<endl;
write <<endl;

//CREATE BONDS BETWEEN BEADS
write << "Bonds"<<endl;
write <<endl;
index=0;
for(int m=0;m<Nm;m++){
    for(int n=0;n<Nb;n++){
        index += 1;
        //For linear polymer we have N-1 bonds
        if(n<N-1){write << index << " " << 1 << " " << (n+1)+N*m << " " << n+2+N*m << endl;}

        //For ring polymer we have N bonds        
        if(n==N-1){write << index << " " << 1 << " " << (n+1)+N*m << " " << 1+N*m << endl;}
    }
}

write <<endl;

//CREATE ANGLES BETWEEN BEADS
//We set at the beginning all angle types to 1
write << "Angles"<<endl;
write <<endl;
index=0;
for(int m=0;m<Nm;m++){
    for(int n=0;n<Na;n++){
        index += 1;         
        //For linear polymer we have N-2 angles
        if(n<N-2){write  << index << " " << 1 << " " << n+1+N*m << " " << n+2+N*m << " " << n+3+N*m << endl;}

        //For ring polymer we have N angles 
        if(n==N-2){write << index << " " << 1 << " " << n+1+N*m << " " << n+2+N*m << " " << 1  +N*m << endl;}

        if(n==N-1){write << index << " " << 1 << " " << n+1+N*m << " " << 1  +N*m << " " << 2  +N*m << endl;}
    }
}

return 0;
}


