#!/bin/bash

c++ -std=c++11 generate_polymer.c++ -o generate_polymer

#Number of molecules
Nm=1

#Number of beads per molecule
N=100

#Monomer density
rho="0.001"

#Name of the file
name="poly.m${Nm}.n${N}.rho${rho}"

#Flag for: linear(0) or circular (1) polymers
flag=1

#The first bead of all chains start at the centre of the box?
# No  (0):
# Yes (1):
flagc=0
 
./generate_polymer $name $Nm $N $flag $rho $flagc

rm generate_polymer
 
