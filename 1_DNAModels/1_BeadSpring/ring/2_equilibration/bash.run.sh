#!/bin/bash
#Current working directory variable 
cwd=`pwd`

#Location of the LAMMPS executables
LAMMPS="${cwd}/../../../../lmp_serial_29Sept2021"
LAMMPSparallel="${cwd}/../../../../lmp_mpi_29Sept2021"

#Name of the lammps script to run
lscript="eq.lammps"

#Initial configurations
intr="poly.m1.n100.rho0.001.ring"

#Number of cpu's to use in the simulation
ncpu=1
  
  mkdir -p REP1
  cd    REP1

  cp ${cwd}/MASTERFILES/${lscript} .
  cp ${cwd}/MASTERFILES/${intr}    .

  #Best random seeds 
  r1=$(od -An -N2 -i /dev/urandom)
  r2=$(od -An -N2 -i /dev/urandom)
  echo "variable seedthermo equal ${r1}"   >  parameters.dat
  echo "variable seedthermo1  equal ${r2}" >> parameters.dat
  
  mpirun -np ${ncpu} ${LAMMPSparallel} -in ${lscript}