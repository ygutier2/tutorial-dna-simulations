#!/bin/bash
  #Remove previous results if they exist
  if [ -f "Rg2_R_vs_t.dat" ]; then
    rm Rg2_R_vs_t.dat
  fi
  
  if [ -f "MSD_vs_t.dat" ]; then
    rm MSD_vs_t.dat
  fi
  
  if [ -f "TTC.dat" ]; then
    rm TTC.dat
  fi
  
  if [ -f "fit.log" ]; then
    rm fit.log
  fi
  
  if [ -f "plot_rg2_MSD_vs_time.pdf" ]; then
    rm plot_rg2_MSD_vs_time.pdf
  fi
  
  if [ -f "plot_TTC.pdf" ]; then
    rm plot_TTC.pdf
  fi

  
  
  #Compile the programs:
  c++ -std=c++11 msd_vs_time.cpp   -o msd_vs_time
  c++ -std=c++11 rg_R_vs_time.cpp  -o rg_R_vs_time
  c++ -std=c++11 ttcorrelation.cpp -o ttcorrelation
  
  #System details (The same set in the LAMMPS script)
  M=1
  Nbeads=100
  rho=0.001
  
  #Start the calculation from this timestep
  ts="0"
  #Dump frequency
  dfreq="10000"
  #Last timestep
  last="10000000"
  
  #Arguments passed to the c++ programs:
  #1.- Path to the input data
  datapath="../REP1/data/"
      
  #2.- Output path (here)
  outputpath="./"
      
  #3.- Name of files to read (without timestep)
  rfile="poly.m${M}.n${Nbeads}.rho${rho}."
  
 
  #Compute Rg
  echo "Computing radius of gyration"
  for timestep in $( seq ${ts} ${dfreq} ${last} )
  do
      ./rg_R_vs_time ${datapath} ${outputpath} ${rfile} ${timestep} ${M} ${Nbeads}
  done
  
  #Compute MSD
  echo "Computing Mean Squared Displacement"
  ./msd_vs_time ${datapath} ${outputpath} ${rfile} ${ts} ${dfreq} ${last} ${M} ${Nbeads}
  
  #Compute Tangent-Tangent Correlation
  echo "Computing Tangent-Tangent Correlation"
  outttc="TTC.dat"
  ./ttcorrelation ${datapath} ${outputpath} ${rfile} ${outttc} ${ts} ${dfreq} ${last} ${M} ${Nbeads}

  rm msd_vs_time
  rm rg_R_vs_time
  rm ttcorrelation
