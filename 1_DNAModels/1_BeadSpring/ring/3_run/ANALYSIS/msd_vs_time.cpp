#include<vector>     /*Library for using vector insted of arrays*/
#include<iostream>   /*Basic functions*/   
#include <cmath>     /*Define some fundamental mathematical operations*/
#include<stdlib.h>   /*Useful for random numbers*/
#include <sstream>
#include <fstream>   /*To read and write into files*/
#include <limits>
#include <stdio.h>   /*Basic functions in particular "sprintf"*/
#include <iomanip>      // std::setprecision
#include <tuple>     //Functions can return more than one value
#include <set>
#include <algorithm>
#include <climits>




using namespace std;

#define pi 3.141592653589793238462643383279502884197169399375
#define SMALL 1e-20

//The timestep set in lammps
double dt=0.01;

int main(int argc, char *argv[])
{
    /**********************************************/
    /*Pass the following arguments to the programm*/
    /**********************************************/
    /*To get help screen of the program: ./trajectories help*/
    if(argc<9)
    {
        cout << "Eight arguments are required by the programm:"             << endl;
        cout << "1.- Path to the input data"                                << endl;
        cout << "2.- Output path"                                           << endl;
        cout << "3.- Input file (without timestep)"                         << endl;
        cout << "4.- tstart"                                                << endl;
        cout << "5.- dumpfreq"                                              << endl;
        cout << "6.- tend"                                                  << endl;
        cout << "7.- Number of molecules"                                   << endl;
        cout << "8.- Nbeads per molecule"                                   << endl;
    }

if(argc==9)
{   
    //cout << "ok" << endl;
    /*argv[1] Directory where the data is*/
    /*argv[2] Directory where the output will be stored*/
    /*argv[3] Input file*/
    
    /*Start the calculation from this timestep*/
    int equiltime;
    equiltime = atoi(argv[4]);;
    
    /*Dump frequency*/
    int dumpfreq;
    dumpfreq = atoi(argv[5]);
    
    /*Last timestep*/
    int totrun;
    totrun = atoi(argv[6]);
           
    /*The number of frames to read*/
    int frames = 1 + (totrun-equiltime)/dumpfreq;
        
    /*Number of polymers in the simulation*/
    int M;
    M = atoi(argv[7]);
    
    /*Number of beads per polymer*/
    int Nbeads;
    Nbeads = atoi(argv[8]);
      
    vector< vector< vector<double> > > position(M, vector<vector<double> >(Nbeads, vector<double>(3)));
    vector< vector< vector<double> > > COM(frames, vector<vector<double> >(M, vector<double>(3)));
    
    
    /*the following variables will be useful to read the data file*/
    int time;
    int tbrownian;
    int timestep;
    int Ntot;        //The total number of beads in the system. It should be equal to M*Nbeads.
    int id,type,mol;
    double   x,y,z;
    int ix,iy,iz;
    double lmin,lmax,Lx,Ly,Lz; 
    int Nbonds;   
    
    /*Read the file with ids of terminal beads*/
    ifstream indata1;
    char readFile1[400] = {'\0'};
    string dummy;
    string line;
        
    
    for (int t=0; t<frames; t++)
    {
        timestep = equiltime + t*dumpfreq;
        //cout << "Reading timestep:" << timestep << endl;
        
        sprintf(readFile1, "%s%s%d",argv[1],argv[3],timestep);        
        indata1.open(readFile1);
    
        if (indata1.is_open())
        {       
            //Read headers
            for(int i=0;i<10;i++){
                if(i==1) {
                    indata1 >> time;                   
                    tbrownian = time*dt;
                }

                if(i==3) {
                    indata1 >> Ntot;
                }

                if(i==5) {
                    indata1 >> lmin >> lmax;
                    Lx = lmax-lmin;
                }

                if(i==6) {
                    indata1 >> lmin >> lmax;
                    Ly = lmax-lmin;
                }

                if(i==7) {
                    indata1 >> lmin >> lmax;
                    Lz = lmax-lmin;
                }

                else getline(indata1,dummy);
            }
                
            //READ ATOMS
            for(int n=0; n<Ntot; n++){
                indata1 >> id >> type >> x >> y >> z >> ix >> iy >> iz;
                int m = floor((double)(id-1)/(1.0*Nbeads));
                position[m][(id-1)%Nbeads][0] = (x + Lx*ix);
                position[m][(id-1)%Nbeads][1] = (y + Ly*iy);
                position[m][(id-1)%Nbeads][2] = (z + Lz*iz);
            }
                        
            for(int m=0;m<M;m++){
                //COMPUTE COM for all the molecules at different times
                double comx=0.0;
                double comy=0.0;
                double comz=0.0;

                for(int n=0;n<Nbeads;n++){
                    comx+=position[m][n][0];
                    comy+=position[m][n][1];
                    comz+=position[m][n][2];
                }

                comx/=(double)Nbeads;
                comy/=(double)Nbeads;
                comz/=(double)Nbeads;
                
                COM[t][m][0]=comx;
                COM[t][m][1]=comy;
                COM[t][m][2]=comz;
            }
            
            indata1.close();
            indata1.clear();
        }
        else {cout << "Error opening file";}
    }
   
   
   
    /////////////////////////////
    // COMPUTE MSD
    ////////////////////////////
    //Some auxiliar quantities
    double dx,dy,dz, dx2, dy2, dz2;

    //This is the MSD one per com
    vector< vector<double> > MSDcom(frames, vector<double>(M));  
    vector<double> msd(frames);
                
    for(int tau=0; tau<frames; tau++)
    {
        for(int m=0;m<M;m++){
            dx2 = dy2 = dz2 = 0.0;
            
            for(int l=0; l<frames-tau; l++)
            {
                dx = COM[l+tau][m][0]-COM[l][m][0];
                dy = COM[l+tau][m][1]-COM[l][m][1];
                dz = COM[l+tau][m][2]-COM[l][m][2];
                
                dx2 += dx*dx;
                dy2 += dy*dy;
                dz2 += dz*dz;
            }
            
            dx2 /= (double)(frames-tau);
            dy2 /= (double)(frames-tau);
            dz2 /= (double)(frames-tau);
            
            //This is the MSD per molecule
            MSDcom[tau][m] = dx2 + dy2 + dz2;
            
            //This is the average over molecules
            msd[tau]+=MSDcom[tau][m];
        }
        msd[tau]*=1.0/(double)M;
    }
    
    /////////////////////////////
    // COMPUTE STDEV
    ////////////////////////////
    vector<double> stdev(frames);
    if(M>1){
        for(int tau=0; tau<frames; tau++)
        {
            double sum=0.0;
            for(int m=0;m<M;m++){
                sum += (msd[tau]-MSDcom[tau][m])*(msd[tau]-MSDcom[tau][m]);
            }

            sum/=(double)(M-1);
            stdev[tau]=sqrt(sum);
        }
     }
     
     if(M==1){
         for(int tau=0; tau<frames; tau++)
         {
             stdev[tau]=0.0;
         }
     }
    /////////////
    // PRINT MSD
    /////////////
    stringstream writeFile1;
    ofstream write1;
    writeFile1 <<"MSD_vs_t.dat";
    write1.open(writeFile1.str().c_str());

    for(int t=0; t<frames; t++)
    {
        write1 << t*dumpfreq << " " << msd[t] << " " << stdev[t];
        for(int m=0;m<M;m++){
            write1 << " " << MSDcom[t][m];
        }
        write1 << endl;
    }
	
}


return 0;
}
