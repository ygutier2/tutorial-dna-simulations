#################################
####    PARAMETERS       ########
#################################
#temperature of the system
variable Temp equal 1.0

#damping parameter, it determines how rapidly the temperature is relaxed.
#We can think about it as inversely proportional to viscosity.
variable tdamp equal 1.0

#persistence length (K of angle coeff)
variable lp equal 3.0

#Length of the run with LJ steric repulsion and FENE bonds
variable run1 equal 10000000

#Restart every these timesteps
variable trestart equal 500000

#Dump data every these timesteps
variable dumpfreq equal 10000

#Dump trajectory every these timesteps
variable dumptraj equal 10000



################################
####	DEFINTIONS 	    ########
################################
variable M equal 1
variable N equal 100
variable rho equal 0.001

#Name of the output file
variable simname index poly.m${M}.n${N}.rho${rho}

#Creates folder where output files will be dumped
variable folder index data
shell    mkdir ${folder}



############################
####   SIM PARAMETERS    ###
############################
#file with the random seed variables
include parameters.dat

units lj
atom_style angle
boundary   p p p
neighbor 1.2 bin
neigh_modify every 1 delay 1 check yes

read_restart ../../2_equilibration/REP1/data/Restart.poly.m1.n100.rho0.001.1000000
reset_timestep 0



######################################
####	PAIRS -- REPULSIVE 	#######
######################################
pair_style   lj/cut 1.12246
pair_modify  shift yes
pair_coeff   * * 1.0 1.0 1.12246


####################################
####    BONDS                #######
####################################
bond_style fene
#It turns off the LJ part of the FENE-bond potential between 1-2 atoms. However, the pairwise LJ interaction is still on
special_bonds fene#<=== I M P O R T A N T (new command)
bond_coeff   * 30.0   1.6  1.0  1.0


####################################
####	ANGLE	             #######
####################################
angle_style   cosine
angle_coeff  * ${lp}


####################################
####    DUMP POSITIONS OF ATOMS ####
####################################
##DAT (best for postprocessing)
dump 1 all custom ${dumpfreq} ${folder}/${simname}.* id type x y z ix iy iz
dump_modify 1 sort id

##CUSTOM (best for visualisation of several molecules)
dump 2 all custom ${dumptraj} ${folder}/trj_${simname}.lammpstrj id mol type x y z ix iy iz
dump_modify 2 sort id
dump_modify 2 format line "%d %d %d %.6le %.6le %.6le %d %d %d"


######################
#### CUSTOM INFO #####
######################
thermo 1000
thermo_style   custom   step  temp  epair vol cpu
timestep 0.01


####################################
####	FIXES	             #######
####################################
timestep 0.01
fix 1 all nve  
fix 2 all langevin   ${Temp} ${Temp} ${tdamp}  ${seedthermo}

#Useful commands when running in parallel
#comm_style tiled
#fix bal all balance 10000 1.1 rcb
comm_modify cutoff 3.5

run ${run1}
