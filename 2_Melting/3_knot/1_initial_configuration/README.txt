To generate the centre line of the knot we use the script provide by Joseph Sleiman:

    https://colab.research.google.com/drive/1KgxJdSAYNbTKQqF22anB2vPDUOKHsr9J
    
Make sure to clean any previous session by:
    Go to "runtime at the top of the page"
    click "disconnect and delete runtime"
    
You follow instruction to generate the knots:

   3_1: p=3, q=2, N=100 beads
   5_1: p=5, q=2, N=100 beads

The configurations generated can be found by clicking in the folder icon in the left. The names assigned are:
  3_1: "(3-2) Torus.dat"
  5_1: "(5-1) Torus.dat"
