#!/bin/bash

# Main path
mainpath="../REP_preRun/"
          
# Directory where the data is
datapath=${mainpath}"data/"

# Directory where the output will be stored
mkdir -p "."
outputpath="./"

# Total number of molecules:
Mtot=1

# Number of ssDNA oligonucleotides per molecule
nss=2

# Number of nucleotides per ssDNA (in dsDNA this is also equal to the number of base-pairs)
N=100

# Starting timestep
start=0

# The dump frequency
dumpfreq=1000000

# Last timestep of the simulation
last=400000000

# oxDNA version (either 1 or 2)
version=2


#####################
#compile the program#
#####################
#Compilation of the programm (Ubuntu)
c++ -std=c++11 melting.cpp -o melting.out -lm

./melting.out ${datapath} ${outputpath} ${start} ${dumpfreq} ${last} ${N} ${version}

rm melting.out
