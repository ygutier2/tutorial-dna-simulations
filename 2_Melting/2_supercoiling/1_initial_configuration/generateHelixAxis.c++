#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<fstream>
#include<sstream>
#include <unistd.h>
#include<ctime>
#include <random>
#include <algorithm>    // std::find
using namespace std;

int main(int argc, char* argv[]){

//Name of the file: argv[1]

//Number of beads per molecule
int N = atoi(argv[2]);

//File to write in
ofstream write;
stringstream filename;
filename.str("");
filename.clear();
filename << argv[1];
write.open(filename.str().c_str());


//In simulation units: 1sigma=0.8518nm. Meaning that 0.34nm=0.399 sigma.
double l=0.4;

//The contour length of DNA should match a circle: L=Nbp*l = 2*pi*R.
double PolymerRadius = (double)(N)*l/(2.0*M_PI); 
double theta =(1.0/(double)N)*2.0*M_PI;

for(int n=0;n<N;n++){
    double x=PolymerRadius*cos(theta*(double)n);
    double y=PolymerRadius*sin(theta*(double)n);
    double z=0.0;
    
    write <<  x << " " <<  y << " "  << z << endl;
}


return 0;
}


