# EUTOPIA Tutorial: DNA models and OXDNA (Yair Fosado)

Here, you will find information about how to perform Molecular Dynamics simulations of DNA using models with different level of resolution and interactions: **Bead-spring, dsDNA and oxDNA**. You will learn the advantages of each model and under which situations they can be employed.

Furtheremore, we will discuss how to:

1. Set initial configurations of DNA molecules with different topology. 
2. Perform Molecular Dynamics simulations with the different models.
3. Visualise the trajectory from simulations.
4. Analyse the output from simulations.

Please if you use any of the codes here, do not forget to cite this repository and the  corresponding papers in the following links:

[bead-spring polymer with Topo2](https://academic.oup.com/nar/article/50/8/4659/6574672)

[dsDNA implementation](https://pubs.rsc.org/en/content/articlelanding/2016/sm/c6sm01859a)

[dsDNA melting](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.119.118002)

[dsDNA supercoling dynamics](https://www.pnas.org/doi/10.1073/pnas.1905215118)

[oxDNA twist, roll, tilt deformations and dynamics](https://pubs.rsc.org/en/content/articlelanding/2021/sm/d0sm01812k)

[oxDNA implementation into LAMMPS](https://link.springer.com/article/10.1140/epje/i2018-11669-8)

[DNA nanostar with oxDNA](https://arxiv.org/abs/2202.06331)


## Requirements
In this section you will find the software neccessary for this tutorial and instructions on how to install it. The tutorial assumes that a LAMMPS executable  called **lmp\_mpi\_29Sep2021** exist into this folder (see below for installation).

Also, we recomend to create an alias for ovito. To this end, open a terminal into your home directory and type *vim bash.rc*. Then add the line:

alias ovito='/home/student/Eutopia-School/Software/ovito-basic-3.7.5-x86_64/bin/./ovito'

Do not forget to replace the path to where you installed ovito.

### Git
Git is a distributed version control software. This is, a way to save changes over time without overwriting previous versions. The instructions to install it in *Windows, Linux and Mac* can be found in the following [Link](https://github.com/git-guides/install-git).

### Xcode (only for mac users)
You can install Xcode from the App Store.

### LAMMPS
In this tutorial we will implement simulations of DNA using [oxDNA](https://dna.physics.ox.ac.uk/index.php/Main_Page), a coarse- grained model that is based on force fields tuned to account for several geometrical and thermodynamic features of single and double stranded DNA (in its B form). Details on this model can be found in the extensive literature available online, but for a quick introduction we reccomend this [paper](https://www.frontiersin.org/articles/10.3389/fmolb.2021.693710/full).

One important feature of the oxDNA model is that [recently](https://link.springer.com/article/10.1140/epje/i2018-11669-8) it was implemented as a package that can be build into LAMMPS. Here, I mentioned the general steps on how to build LAMMPS with this package (assuming that you are using a computer with either macOS or linux-OS such as Ubuntu). For different operating systems please follow instructions [here](https://docs.lammps.org/Install.html).

We will work with the current stable release of LAMMPS (**29-Sept-2021**) that can be installed in the following way: Assuming that you have a terminal located in the **home** directory, you have to type:

> git clone -b stable https://github.com/lammps/lammps.git mylammps
> 
> cd mylammps/src/STUBS/
> 
> make
> 
> cd ../
> 
> make yes-molecule yes-asphere yes-CG-DNA yes-extra-molecule yes-rigid yes-misc
> 
> make mpi
> 
> mv lmp\_mpi ~/lmp\_mpi_29Sep2021

With this, an executable with the name **lmp\_mpi\_29Sep2021** shoul have been created in your home directory (*~/*). Copy the executable into this folder, where you downloaded the tutorial.

### Ovito
In the tutorial we will learn how to visualize the output generated from simulations. To this end, we will use [Ovito](https://www.ovito.org) a visualization tool that can deal with aspherical particles. This is necessary to capture the planarity of bases in a nucleotide.

To install ovito you just have to follow the instructions in this [link](https://www.ovito.org/docs/current/installation.html). Ovito is very friendly and instructions on installation should be easy to follow. However, here I summarize the steps for installation:

> 1. Open this [Link](https://www.ovito.org)
> 
> 2. Choose your Operating System
> 
> 3. Download the basic version of ovito (free of charge)
> 
> 4. Depending on your system:
> 	* Linux: Extract the downloaded .tar.xz archive file using the tar command: tar xJfv ovito-3.7.0-x86_64.tar.xz. This will create a new sub-directory containing the program files. Change into that directory and start OVITO by running the executable ./bin/ovito.
> 
>	* Windows: Run the installer program ovito-x.y.z-win64.exe to install OVITO in a directory of your choice. Note that Windows might ask whether you really want to launch the installer, because it was downloaded from the web and it is not digitally signed.
>
>	* macOS:Double-click the downloaded .dmg disk image file to open it, agree to the program license, and drag the Ovito application bundle into your Applications folder. Then start OVITO by double-clicking the application bundle.

### numpy
In ubuntu you can install it by **pip3 install numpy**
### TacoxDNA 
More information can be found on this [Link](http://tacoxdna.sissa.it/)


### oxView
More information can be found on this [Link](https://sulcgroup.github.io/oxdna-viewer/)


